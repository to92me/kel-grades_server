# Subject - add staff to subject

**URL** : `/api/subject`

**Method** : `POST`

**Format** : `CSV`

**Auth required** : YES

**Data constraints**

```csv
subject_id,user_id,association
___,___,___,
...
```

**Data example**

```csv
subject_id,user_id,association
1,91, assistant,
5,99,
1,90,assistant,
3,91,none,
```

## Success Response

**Code** : `200 OK`

**Content example**

```CSV
subject_id,user_id,association,
1,91,professor,updated,
5,99,assistant,Subject by this ID does not exists,
1,90,-,Association must be professor, assistant or none,
3,91,none,ok,
```

## Error Response

**Condition** : sent data is incorrect

**Code** : `400 BAD REQUEST`

**Content** :

```html

  <h3> HTTP_UNAUTHORIZED </h3>

```
