# Subject - get information about subject, scores and grades from all students

**URL** : `/api/subject/:subject_name`

**URL Parameters** : subject_name=[string] where subject_name is the subject name of the Subject in the database.

**Method** : `GET`

**Format** : `JSON`

**Auth required** : YES

**Permissions required** : `Professor` or `admin`

**Data constraints** :


## Success Response

**Code** : `200 OK`

**Content example**


```json
[
    {
        "ects points": 8,
        "pass condition": "t1+t2>50; r1+r2>50",
        "professor": [],
        "semester": "letnji",
        "student": [
            {
                "email": "petar@petrovic",
                "first name": "Petar",
                "grade": 5,
                "part": [
                    {
                        "exam": [
                            {
                                "date": "2018-1-23",
                                "points": 23
                            },
                            {
                                "date": "2018-1-28",
                                "points": 50
                            }
                        ],
                        "name": "mikrokontroleri",
                        "points_max": 50,
                        "type": "teorija"
                    },
                    {
                        "exam": [],
                        "name": "projekat",
                        "points_max": 50,
                        "type": "projekat"
                    }
                ],
                "passed": 0,
                "points": 0,
                "second name": "Petrovic",
                "telephone": ""
            },
            {
                "email": "marko@gmail.com",
                "first name": "Janko",
                "grade": 5,
                "part": [
                    {
                        "exam": [],
                        "name": "mikrokontroleri",
                        "points_max": 50,
                        "type": "teorija"
                    },
                    {
                        "exam": [],
                        "name": "projekat",
                        "points_max": 50,
                        "type": "projekat"
                    }
                ],
                "passed": 0,
                "points": 0,
                "second name": "Slavko",
                "telephone": ""
            }
        ],
        "subject id": 2,
        "subject name": "DMK"
    }
]
```

## Error Response

**Condition** : sent data is incorrect

**Code** : `400 BAD REQUEST`

**Content** :

```html

  <h3> HTTP_REQUEST </h3>

```
