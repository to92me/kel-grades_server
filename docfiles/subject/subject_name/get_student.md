# Subject - get information about subject and logged student scores and grades

**URL** : `/api/subject/:subject_name`

**URL Parameters** : subject_name=[string] where subject_name is the subject name of the Subject in the database.

**Method** : `GET`

**Format** : `JSON`

**Auth required** : YES

**Permissions required** : `Student`

**Data constraints** : student will get only data for him ( not from other students)


## Success Response

**Code** : `200 OK`

**Content example**


```json
[
    {
        "ects points": 8,
        "pass condition": "t1+t2>50; r1+r2>50",
        "professor": [],
        "semester": "letnji",
        "student": [
            {
                "email": "marko@gmail.com",
                "first name": "Janko",
                "grade": 5,
                "part": [
                    {
                        "exam": [],
                        "name": "mikrokontroleri",
                        "points_max": 50,
                        "type": "teorija"
                    },
                    {
                        "exam": [],
                        "name": "projekat",
                        "points_max": 50,
                        "type": "projekat"
                    }
                ],
                "passed": 0,
                "points": 0,
                "second name": "Slavko",
                "telephone": "",
                "user_id": "ee204-2012"
            }
        ],
        "subject id": 2,
        "subject name": "DMK"
    }
]
```

## Error Response

**Condition** : sent data is incorrect

**Code** : `400 BAD REQUEST`

**Content** :

```html

  <h3> HTTP_REQUEST </h3>

```
