# Student - delete subject and all parts and scores of it

**URL** : `/api/subject/:subject_name`

**URL Parameters** : subject_name=[string] where subject_name is the name of the Subject in the database.

**Method** : `DELETE`

**Auth required** : YES

**Permissions required** : `Professor` or `admin`

**Data constraints**


## Success Response

**Code** : `200 OK`


## Error Response

**Condition** : sent data is incorrect

**Code** : `400 BAD REQUEST`

**Content** :

```html

  <h3> HTTP_BAD_REQUEST </h3>

```
