# Subject - add students to subject and create parts of subject

**URL** : `/api/subject/:subject_name`

**URL Parameters** : subject_name=[string] where subject_name is the subject name of the Subject in the database.

**Method** : `POST`

**Format** : `CSV`

**Auth required** : YES

**Permissions required** : `Professor` or `admin`

**Note** : Only professor or assistant on subject can create subject parts and add students

**Data example**

```CSV
DMK,teorija:mikrokontroleri:50,projekat:projekat:50,grade,passed
e5,
ee204-2012,
```

## Success Response

**Code** : `200 OK`

**Content example**

```CSV
DMK,teorija:mikrokontroleri:50,projekat:projekat:50,grade,passed
e5,
ee204-2012,
```

## Error Response

**Condition** : sent data is incorrect

**Code** : `400 BAD REQUEST`

**Content** :

```html

  <h3> HTTP_BAD_REQUEST </h3>

```
