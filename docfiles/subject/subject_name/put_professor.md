# Subject - updates students points and grades on subject

**URL** : `/api/subject/:subject_name`

**URL Parameters** : subject_name=[string] where subject_name is the subject name of the Subject in the database.

**Method** : `PUT`

**Format** : `CSV`

**Auth required** : YES

**Permissions required** : `Professor` or `admin`

**Note** : Only professor or assistant on subject can update grades and scores

**Data example**

```CSV
DMK,ime,prezime,teorija:mikrokontroleri:50,projekat:projekat:50,points,grade,passed
e5,neko,nesto, 33, 32, 65,7,1
ee204-2012,neko,nesto,12,42,12,5,0
```

## Success Response

**Code** : `200 OK`

**Content example**

```CSV
DMK,ime,prezime,teorija:mikrokontroleri:50,projekat:projekat:50,points,grade,passed
e5,neko,nesto, 33, 32, 65,7,1
ee204-2012,neko,nesto,12,42,12,5,0
```

## Error Response

**Condition** : sent data is incorrect

**Code** : `400 BAD REQUEST`

**Content** :

```html

  <h3> HTTP_BAD_REQUEST </h3>

```
