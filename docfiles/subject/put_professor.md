# Subject - update subject

**URL** : `/api/subject`

**Method** : `POST`

**Format** : `CSV`

**Auth required** : YES

**Permissions required** : `Professor` or `admin`

**Data example**

```CSV
subject_name,passed_string,ects_points,semester
lprs,k1+k2>50,5,fall
```

## Success Response

**Code** : `200 OK`

**Content example**

```CSV
subject_id,subject_name,passed_string,ects_points,semester
5,lprs,k1+k2>50,5,fall, updated
```

## Error Response

**Condition** : sent data is incorrect

**Code** : `400 BAD REQUEST`

**Content** :

```html

  <h3> HTTP_BAD_REQUEST </h3>

```
