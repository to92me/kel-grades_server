# Student - get information about all subjects

**URL** : `/api/subject`

**Method** : `GET`

**Format** : `JSON`

**Auth required** : YES

**Data constraints** :


## Success Response

**Code** : `200 OK`

**Content example**


```json
[
    {
        "ects_points": 8,
        "id": 1,
        "name": "PSDS",
        "pass_condition": "fall",
        "semester": "p1+p2>50"
    },
    {
        "ects_points": 8,
        "id": 2,
        "name": "DMK",
        "pass_condition": "letnji",
        "semester": "t1+t2>50; r1+r2>50"
    },
    {
        "ects_points": 8,
        "id": 3,
        "name": "LPRS",
        "pass_condition": "fall",
        "semester": "p1+p2>50"
    }
]
```

## Error Response

**Condition** : if student by this id does not exists.

**Code** : `400 BAD REQUEST`

**Content** :

```html

  <h3> HTTP_REQUEST </h3>

```
