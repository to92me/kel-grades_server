# KELGS REST documentation

Kel grade managment system Application Programming Interface

Where full URLs are provided in responses they will be rendered as if service
is running on 'http://testserver:port/'.

## Open Endpoints

Open endpoints require no Authentication.

* [Login](docfiles/login/login.md) : `POST /login`

## Endpoints that require Authentication

Closed endpoints require a valid Token to be included in the header of the
request. A Token can be acquired from the Login view above.

### Students related

Each endpoint manipulates or displays information related to the Student whose
Token is provided with the request:

* [Show student](docfiles/student/user_id/get_student.md) : `GET /student/:user_id -> JSON`
* [show subject](docfiles/subject/subject_name/get_student.md) : `GET /subject/:subject_name -> JSON`
* [show subject](docfiles/subject/get_student.md) : `GET /subject/ -> JSON`

### Professors and Admins related

Endpoints for viewing and manipulating the Accounts that the Authenticated User
has permissions to access.

 Student

* [show student](docfiles/student/user_id/get_professor.md) : `GET /student/:user_id  -> JSON`
* [create student](docfiles/student/post_professor.md) : `POST /student <- CSV`
* [update student](docfiles/student/put_professor.md) : `PUT /student <- CSV`
* [delete student](docfiles/student/user_id/delete_professor.md) : : `DELETE /student/:user_id`

 Professor

* [show professor](docfiles/professor/user_id/get_professor.md) : `GET /professor/:user_id -> JSON`
* [create professor](docfiles/professor/post_professor.md) : `POST /professor <- CSV`
* [update professor](docfiles/professor/put_professor.md) : `PUT /professor <- CSV`
* [delete professor](docfiles/professor/user_id/delete_professor.md) : `DELETE /professor/:user_id`
* [create professor admin](docfiles/professor/admin/post_professor.md) : `POST /professor/admin <- CSV`
* [update professor admin](docfiles/professor/admin/put_professor.md) : `PUT /professor/admin <- CSV`

 Subject

* [show subject](docfiles/subject/subject_name/get_professor.md) : `GET /subject/:subject_name -> JSON`
* [create subject](docfiles/subject/post_professor.md) : `POST /subject <- CSV`
* [update subject](docfiles/subject/put_professor.md) : `PUT /subject <- CSV`
* [delete subject](docfiles/subject/subject_name/delete_professor.md) : `DELETE /subject/:subject_name`

 Subject parts scores and grades

* [add stuff to subject](docfiles/subject/patch_professor.md) : `PATCH /subject <- CSV`
* [add student to subject](docfiles/subject/subject_name/post_professor.md) : `POST /subject/:subject_name <-CSV`
* [add update student-subject data](docfiles/subject/subject_name/put_professor.md) : `PUT /subject/:subject_name <-CSV`

 Admin

* [show admins](docfiles/admin/get_professor.md) : `GET /student/:user_id  -> JSON`
* [create admin](docfiles/admin/post_professor.md) : `POST /student <- CSV`
* [update admin](docfiles/admin/put_professor.md) : `PUT /student <- CSV`
* [delete admin](docfiles/admin/user_id/delete_professor.md) : : `DELETE /student/:user_id`

 Development

* [dev - logs](docfiles/dev/get_professor.md) : `GET /dev -> HTML or JSON`
