# Login

Used to collect a Token for a registered User.

**URL** : `/api/login/`

**Method** : `POST`

**Auth required** : NO

**Data constraints**

 Basic Auth

```
    "username": "[index]",
    "password": "[password]"
```

**Data example**

```
    "username": "ee200-2000",
    "password": "abcd1234"
```

## Success Response

**Code** : `200 OK`

**Content example**

Cockie

```
    "sessionToken": "93144b288eb1fdccbe46d6fc0f241a51766ecd3d"
```

## Error Response

**Condition** : sent data is incorrect

**Code** : `400 BAD REQUEST`

**Content** :

```html

  <h3> HTTP_BAD_REQUEST </h3>

```
