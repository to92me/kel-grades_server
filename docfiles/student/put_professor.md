# Professor - update student

**URL** : `/api/student`

**Method** : `POST`

**Format** : `CSV`

**Auth required** : YES

**Permissions required** : `Professor` or `admin`

**Data example**

```CSV
index,ime,prezime,email,telephone,password
5,Marko,Zarko,mz@gmail.com,006655,passwd_asdf
```

**or** without telephone

```CSV
index,ime,prezime,email,telephone,password
5,Marko,Zarko,mz@gmail.com,,passwd_asdf
```

## Success Response

**Code** : `200 OK`

**Content example**

```CSV
index,ime,prezime,email,telephone,password,craeted
5,Marko,Zarko,mz@gmail.com,006655,passwd_asdf,updated
```

## Error Response

**Condition** : sent data is incorrect

**Code** : `400 BAD REQUEST`

**Content** :

```html

  <h3> HTTP_BAD_REQUEST </h3>

```
