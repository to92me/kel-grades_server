# Student - get information about student

**URL** : `/api/student/:user_id`

**URL Parameters** : user_id=[string] where user_id is the ID of the User in the database.

**Method** : `GET`

**Format** : `JSON`

**Auth required** : YES

**Permissions required** : `Professor` or `admin`

**Data constraints** professor will get data from any student


## Success Response

**Code** : `200 OK`

**Content example**


```json
{
  "admin": true,
  "email": "marko@gmail.com",
  "name_first": "Janko",
  "name_second": "Slavko",
  "subject": [
      {
          "ects points": 8,
          "pass condition": "p1+p2>50",
          "professor": [
              {
                  "email": "vukvra@gmail.com",
                  "first name": "Vuk",
                  "id": "90",
                  "second name": "Vra",
                  "telephone": "",
                  "type": "assistant"
              },
              {
                  "email": "drkanovic@uns.ac.rs",
                  "first name": "Damjan",
                  "id": "91",
                  "second name": "Rkanovic",
                  "telephone": "",
                  "type": "assistant"
              },
              {
                  "email": "rasti@uns.ac.rs",
                  "first name": "Rastislav",
                  "id": "92",
                  "second name": "Struharik",
                  "telephone": "",
                  "type": "professor"
              }
          ],
          "semester": "fall",
          "student": [
              {
                  "grade": 5,
                  "part": [
                      {
                          "exam": [
                              {
                                  "date": "2016-12-16",
                                  "points": 12
                              },
                              {
                                  "date": "2016-12-20",
                                  "points": 15
                              },
                              {
                                  "date": "2016-12-25",
                                  "points": 25
                              }
                          ],
                          "name": "teorija 1",
                          "points_max": 25,
                          "type": "teorija"
                      },
                      {
                          "exam": [
                              {
                                  "date": "2016-11-16",
                                  "points": 15
                              }
                          ],
                          "name": "teorija 2",
                          "points_max": 25,
                          "type": "teorija"
                      },
                      {
                          "exam": [],
                          "name": "zadaci1",
                          "points_max": 12,
                          "type": "zadaci"
                      },
                      {
                          "exam": [],
                          "name": "zadaci2",
                          "points_max": 12,
                          "type": "zadaci"
                      },
                      {
                          "exam": [],
                          "name": "projekat",
                          "points_max": 26,
                          "type": "projekat"
                      },
                      {
                          "exam": [],
                          "name": "matrice",
                          "points_max": 25,
                          "type": "teorija"
                      }
                  ],
                  "passed": 0,
                  "points": 0
              },
              {
                  "grade": 5,
                  "part": [
                      {
                          "exam": [],
                          "name": "mikrokontroleri",
                          "points_max": 50,
                          "type": "teorija"
                      },
                      {
                          "exam": [],
                          "name": "projekat",
                          "points_max": 50,
                          "type": "projekat"
                      }
                  ],
                  "passed": 0,
                  "points": 0
              }
          ],
          "subject id": 1,
          "subject name": "PSDS"
      },
      {
          "ects points": 8,
          "pass condition": "t1+t2>50; r1+r2>50",
          "professor": [
              {
                  "email": "vukvra@gmail.com",
                  "first name": "Vuk",
                  "id": "90",
                  "second name": "Vra",
                  "telephone": "",
                  "type": "assistant"
              },
              {
                  "email": "drkanovic@uns.ac.rs",
                  "first name": "Damjan",
                  "id": "91",
                  "second name": "Rkanovic",
                  "telephone": "",
                  "type": "assistant"
              },
              {
                  "email": "rasti@uns.ac.rs",
                  "first name": "Rastislav",
                  "id": "92",
                  "second name": "Struharik",
                  "telephone": "",
                  "type": "professor"
              }
          ],
          "semester": "letnji",
          "student": [
              {
                  "grade": 5,
                  "part": [
                      {
                          "exam": [
                              {
                                  "date": "2016-12-16",
                                  "points": 12
                              },
                              {
                                  "date": "2016-12-20",
                                  "points": 15
                              },
                              {
                                  "date": "2016-12-25",
                                  "points": 25
                              }
                          ],
                          "name": "teorija 1",
                          "points_max": 25,
                          "type": "teorija"
                      },
                      {
                          "exam": [
                              {
                                  "date": "2016-11-16",
                                  "points": 15
                              }
                          ],
                          "name": "teorija 2",
                          "points_max": 25,
                          "type": "teorija"
                      },
                      {
                          "exam": [],
                          "name": "zadaci1",
                          "points_max": 12,
                          "type": "zadaci"
                      },
                      {
                          "exam": [],
                          "name": "zadaci2",
                          "points_max": 12,
                          "type": "zadaci"
                      },
                      {
                          "exam": [],
                          "name": "projekat",
                          "points_max": 26,
                          "type": "projekat"
                      },
                      {
                          "exam": [],
                          "name": "matrice",
                          "points_max": 25,
                          "type": "teorija"
                      }
                  ],
                  "passed": 0,
                  "points": 0
              },
              {
                  "grade": 5,
                  "part": [
                      {
                          "exam": [],
                          "name": "mikrokontroleri",
                          "points_max": 50,
                          "type": "teorija"
                      },
                      {
                          "exam": [],
                          "name": "projekat",
                          "points_max": 50,
                          "type": "projekat"
                      }
                  ],
                  "passed": 0,
                  "points": 0
              }
          ],
          "subject id": 2,
          "subject name": "DMK"
      }
  ],
  "telephone": "",
  "type": "student"
}
```

## Error Response

**Condition** : sent data is incorrect

**Code** : `400 BAD REQUEST`

**Content** :

```html

  <h3> HTTP_BAD_REQUEST </h3>

```
