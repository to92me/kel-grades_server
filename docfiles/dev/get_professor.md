# Show logs for development

**URL** : `/api/dev`

**Format** : `JSON` or `Html`

**Method** : `GET`

**Auth required** : YES

**Permissions required** : `Professor` or `admin`

## Success Response

**Code** : `200 OK`

**Content examples**

Html web page with logs
