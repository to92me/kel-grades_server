# Professor - get information about professor and all his subjects students on them, scores and grades

**URL** : `/api/professor/:user_id`

**URL Parameters** : user_id=[string] where user_id is the ID of the User in the database.

**Method** : `GET`

**Format** : `JSON`

**Auth required** : YES

**Data constraints**

**NOTE NOT IMPLEMENTED YET

## Success Response

**Code** : `200 OK`

**Content example**


```json

```

## Error Response

**Condition** : sent data is incorrect

**Code** : `400 BAD REQUEST`

**Content** :

```html

  <h3> HTTP_BAD_REQUEST </h3>

```
