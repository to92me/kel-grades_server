# Professor - delete professor

**URL** : `/api/professor/:user_id`

**URL Parameters** : user_id=[string] where user_id is the ID of the User in the database.

**Method** : `DELETE`

**Auth required** : YES

**Permissions required** : `Professor` or `admin`

**Data constraints**


## Success Response

**Code** : `200 OK`


## Error Response

**Condition** :

**Code** : `400 BAD REQUEST`

**Content** : sent data is incorrect

```html

  <h3> HTTP_BAD_REQUEST </h3>

```
