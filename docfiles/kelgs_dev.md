# KELGS DEV
This document is for KEL students who wish to continue development of KEL grades server project.
Application source is split into eight groups:
1. Adapter,
2. Authorization,
3. Controller,
4. Logger,
5. MySqlHandler,
6. Server,
7. Utils and
8. Views.

## Server group
Creates HTTP/HTTPS stack, listens to requests and creates handlers to respond to requests.
The group contains three classes:
1. RequestHandler class - gets raw request, checks authorization and creates a response to it.
2. RequestHandlerFactory class - is builder factory class that creates for each request new thread with RequestHandler. Therefore, each request is handled in its own thread.
3. HttpServer class - creates HTTP/HTTPS stack and has an instance of RequestHandlerFactory. For each request gets from the factory handler that will handle a request.

## Controller group
For each API group, there is one controller that will check what HTTP method is used and execute it. The controller will also check if logged user has permission to execute a request. 
The Controller doesn't work directly on a database, it uses an Adapter for raw database commands.
The controller usually parses a request data  (JSON, CSV ... )  checks if it the user is student or professor which Adapter should call etc...

## Adapter group
Adapters are classes that execute raw database commands.

## Logger group
Contains Logger and Logger interface. Logger interface is class made to be inherited in classes which use logger.

## MySqlHandler group
This group is now just for storing database configuration. Idea was to create MySql connection object pool. This is one good future task.

## Authorization group
Checks if a request has:
- user id and password in base64 ( HTTP basic auth) or
- cookie with the session token.
Gets level of authorization of the user and generates config object of a user.

## Views group
Generates HTTP web page for debugging purposes.

## Utils group
Contains utils like URI parser, CSV parser etc...
