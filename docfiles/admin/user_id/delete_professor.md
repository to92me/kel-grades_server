# Admin - delete admin

**URL** : `/api/admin/:user_id`

**URL Parameters** : user_id=[string] where user_id is the ID of the User in the database.

**Method** : `DELETE`

**Auth required** : YES

**Permissions required** : `Professor` or `admin`

**Data constraints**


## Success Response

**Code** : `200 OK`


## Error Response

**Condition** : sent data is incorrect

**Code** : `400 BAD REQUEST`

**Content** :

```html

  <h3> HTTP_BAD_REQUEST </h3>

```
