# Admin - get information about all admins

**URL** : `/api/admin`

**Method** : `GET`

**Format** : `JSON`

**Auth required** : YES

**Permissions required** : `Professor` or `admin` or `student`

**Data constraints**


## Success Response

**Code** : `200 OK`

**Content example**


```json
[
    {
        "admin": true,
        "email": "Admin@uns.ac.rs",
        "first name": "Admin",
        "id": "a1",
        "second name": "Admin",
        "telephone": "",
        "type": "admin"
    },
    {
        "admin": true,
        "email": "vukvra@gmail.com",
        "first name": "Vuk",
        "id": "90",
        "second name": "Vra",
        "telephone": "",
        "type": "professor"
    },
    {
        "admin": true,
        "email": "rasti@uns.ac.rs",
        "first name": "Rastislav",
        "id": "92",
        "second name": "Struharik",
        "telephone": "",
        "type": "professor"
    }
]
```

## Error Response

**Condition** : sent data is incorrect

**Code** : `400 BAD REQUEST`

**Content** :

```html

  <h3> HTTP_BAD_REQUEST </h3>

```
