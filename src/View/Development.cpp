#include "Development.h"

namespace View{

Path Development::path_footer = "footer.html";
Path Development::path_header = "header.html";


void Development::setTopHeaderFile(const std::string &_path){
    Development::path_header.clear();
    Development::path_header.assign(_path);
}

void Development::setBottomHeaderFile(const std::string &_path){
    Development::path_footer.clear();
    Development::path_footer.assign(_path);
}

void Development::generateHtml(std::ostream &_out_stream){

    std::ifstream istream_header(path_header.toString());
    std::ifstream istream_footer(path_footer.toString());

    if(istream_header.good()){
        _out_stream << istream_header.rdbuf();
    }

    std::stringstream log_buff;
    logger->streamLog(log_buff);

    std::string line;
    while(std::getline(log_buff, line)){
        line = line + "<br/>";
        _out_stream << line;
    }

    if(istream_footer.good()){
        _out_stream << istream_footer.rdbuf();
    }else{
        logger->streamLog(_out_stream);

    }
    _out_stream.flush();
}

}
