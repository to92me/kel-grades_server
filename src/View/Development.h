#ifndef VIEWDEBUG_H
#define VIEWDEBUG_H

#include <iostream>
#include "Logger/Logger.h"
#include "Poco/Path.h"
#include <fstream>

using Poco::Path;
using std::istream;

namespace View{


/**
 *  @brief View Debug class is html generator for debuging purposes.
 *
 * @ingroup View
 *
 * @author Tomislav Tumbas
 *
 * @version 0.1
 *

 * @note NOT DONE, still in progress
 *
 *
 * contact tumbas.tomislav@gmail.com tumbas@uns.ac.rs
 */
class Development{
public:
    /**
     * @brief Constructor
     */
    Development(){
        this->logger = Log::Logger::getInstance();
    }

    /**
      * @brief Destructor
      */
    ~Development(){}

    /**
     * @brief this method should get as parameter path to html,css,js text file that
     * will be places before log text create complete html web page
     * @note not implemented
     * @todo implement this method
     */
    static void setTopHeaderFile(const std::string &_path);

    /**
     * @brief this method should get as parameter path to html,css,js text file that
     * will be places after log text to create complete html web page
     * @note not implemented
     * @todo implement this method
     */
    static void setBottomHeaderFile(const std::string &_path);

    /**
     * @brief creates html wep page from top header bottom header and log from logger
     * @note top and bottom header are not implemented
     * @todo complete top and bottom header file
     *
     */
    void generateHtml(std::ostream& _out_stream);

private:
    Log::Logger* logger;
    static Path path_footer;
    static Path path_header;

};

}

#endif
