/**
 * @class Logger
 *
 * @ingroup Log
 * (Note, this needs exactly one \defgroup somewhere)
 *
 * @brief Provide an example
 *
 * Logger class is used to log all information to database or to command line
 * If application is run as deamon logger will store logs to databse.
 *
 * @note Not done.
 *
 * @author (last to touch it) $Author: bv $
 *
 * @version $Revision: 1.1 $
 *
 * @date $Date: 26.11.2015 $
 *
 * Contact: tumbas.tomislav@gmail.com, tumbas@uns.ac.rs
 *
 * Created on: Wed Apr 13 18:39:37 2005
 *
 * $Id: doxygen-howto.html,v 1.5 2005/04/14 14:16:20 bv Exp $
 *
 */

#ifndef LOGSAVER_H
#define LOGSAVER_H

#include "Poco/Mutex.h"
#include "Poco/DateTime.h"
#include <map>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <MySqlHandler/MySqlHandler.h>
#include "Poco/Data/Session.h"
#include "Poco/Data/SessionPool.h"
#include "Poco/Data/Statement.h"
#include "Poco/JSON/Object.h"

#define LOG_LEVEL_TO_DB 1

using Poco::FastMutex;
using Poco::ScopedLock;
using Poco::DateTime;
using MySql::MySqlHandler;

namespace Log{

/**
 * @brief The Logger class
 * is the main logger class. It can log data to terminal and to DB. It contains
 * MySQL connector.
 *
 * @ingroup Log
 *
 * @author Tomislav Tumbas
 *
 * @todo test all fetures
 * @todo test if connection is not aveable
 * @todo if there is no connectino to db set logger to dev mode ( print data to terminal )
 * @todo add feature to set blacklist or whitelist ( Logger reads it and creates ... )
 *
 * @version 1.1
 *
 * contact tumbas.tomislav@gmail.com tumbas@uns.ac.rs
 */
class Logger {
public:

    /// Type of logging
    enum LogType{
        INFO,
        WARNING,
        ERROR,
        DEBUG
    };

    /// level of logging
    enum LogLevel{
        ALL = 1, /**< priint or store all logs */
        WARNING_AND_ERROR = 2, /**<  print or store warnings and errors */
        ERROR_ONLY = 3   /**<  print or store only errors */
    };

    ///Singleton logger instance getter
    static Logger* getInstance();
    ~Logger();

    /**
     * @brief logs information ( info )
     * @param _module is name of module of logging
     * @param _info is text to log
     */
    void logInfo(std::string _module, std::string _info);

    /**
     * @brief logs warning
     * @param _module is name of module of logging
     * @param _warning is text to log
     */
    void logWarning(std::string _module, std::string _warning);

    /**
     * @brief logs error
     * @param _module is name of module of logging
     * @param _error is text to log
     */
    void logError(std::string _module, std::string _error);

    /**
     * @brief logs debug
     * @param _module is name of module of logging
     * @param _debug is text to log
     */
    void logDebug(std::string _module, std::string _debug);

    // this static is redudanci but it provides nicer style of setting dev mode

    /**
     * @brief Sets Development mode of loggin
     * @param true printing to terminal, false storeing data to Database
     */
    static void setDevMode(bool _dev_mode);

    /**
     * @brief getter for dev mode
     * @return dev mode
     */
    static bool getDevMode();

    /**
     * @brief if this is enabled then logs will be stored to buffer fixed size ( @see setLogBufSize )
     * @param _enable is bool true = enable, false = disable
     */
    void setWriteToHtmlStream(const bool _enable);

    /**
     * @brief this mehod will create store paramtere _modul_name to blacklist and logs from this module will not be
     * loged. Except error messages, they are always loged.
     * @param _module_name
     */
    void blacklistModule(const std::string _module_name);

    /**
     * @brief set size of log buffer for printing data to development view
     * @param _size is size of buffer
     */
    inline void setLogBufSize(const unsigned int _size);

    /**
     * @brief streamLog creates stream of logs
     * @param _out_stream is out stream with logs
     */
    void streamLog(std::ostream& _out_stream);

    /**
     * @brief jsonLog creates Poco::JSON::Object with logs
     * @return Poco::JSON::Object with logs
     */
    Poco::JSON::Object::Ptr jsonLog();

private:


    //singleton
    Logger();
    static Logger* instance;

    FastMutex mutex;
    DateTime date_time;
    std::string module;
    std::string db_connection_chain;
    std::set<std::string> blacklist;

    static bool dev_mode;

    typedef std::map<LogType,std::string> LogTypeMap;
    static LogTypeMap log_type_map;

    MySqlHandler* handler;
    Poco::Data::SessionPool* session_pool_mysql;

    bool createSessionPool();

    void printString(LogType _type, std::__cxx11::string _module, std::string _str);
    void logToDB(LogType _type, std::__cxx11::string _module, std::string _str);

    // note this is separate method because it maybe needs some different format from printing to terminal
    void logToLogBuf(LogType _type, std::__cxx11::string _module, std::string _str);

    bool log_buf_enabled = false;
    std::deque<std::string> log_buf;
    unsigned int log_buf_size = 400;


};

} // endnamespace logger

#endif
