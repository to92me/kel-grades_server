#include "Interface.h"

namespace Log {

void LoggerInterface::debug(std::string _debug){
    logger->logDebug(this->module, _debug);
}

void LoggerInterface::info(std::string _info){
    logger->logInfo(this->module, _info);
}

void LoggerInterface::warning(std::string _warning){
    logger->logWarning(this->module, _warning);
}

void LoggerInterface::error(std::string _error){
    logger->logError(this->module, _error);
}

LoggerInterface::LoggerInterface(std::string _module){
    this->module = _module;
    module_set = true;
    logger = Logger::getInstance();
}

LoggerInterface::LoggerInterface(){
    this->module_set = false;
    logger = Logger::getInstance();
}

}
