#include "Logger.h"

namespace Log{

Logger* Logger::instance = NULL;
bool Logger::dev_mode = true;

Logger::LogTypeMap Logger::log_type_map = {
    {Logger::LogType::INFO,"Info"},
    {Logger::LogType::WARNING,"Warning"},
    {Logger::LogType::ERROR,"ERROR"},
    {Logger::LogType::DEBUG,"Debug"}
};

void Logger::logWarning(std::string _module, std::string _warning){
    FastMutex::ScopedLock lock(mutex);

    if(dev_mode == true){
        this->printString(LogType::WARNING, _module, _warning);
    }else{
        this->logToDB(LogType::WARNING, _module, _warning);
    }

    if(log_buf_enabled == true){
        this->logToLogBuf(LogType::WARNING, _module, _warning);
    }
}

void Logger::logInfo(std::string _module, std::string _info){
    FastMutex::ScopedLock lock(mutex);

    if(dev_mode == true){
        this->printString(LogType::INFO, _module, _info);
    }else{
        this->logToDB(LogType::INFO, _module, _info);
    }

    if(log_buf_enabled == true){
        this->logToLogBuf(LogType::INFO, _module, _info);
    }
}

void Logger::logError(std::string _module, std::string _error){
    FastMutex::ScopedLock lock(mutex);

    if(dev_mode == true){
        this->printString(LogType::ERROR, _module, _error);
    }else{
        this->logToDB(LogType::ERROR, _module, _error);
    }

    if(log_buf_enabled == true){
        this->logToLogBuf(LogType::ERROR, _module, _error);
    }
}

void Logger::logDebug(std::string _module, std::string _debug){
    FastMutex::ScopedLock lock(mutex);

    if(dev_mode == true){
        this->printString(LogType::DEBUG, _module, _debug);
    }else{
        this->logToDB(LogType::DEBUG, _module, _debug);
    }

    if(log_buf_enabled == true){
        this->logToLogBuf(LogType::DEBUG, _module, _debug);
    }
}

Logger* Logger::getInstance(){

    if (Logger::instance == NULL){
        instance = new Logger();
    }

    return instance;
}

void Logger::setDevMode(bool _dev_mode){

    dev_mode = _dev_mode;
}

bool Logger::getDevMode(){

    return dev_mode;
}

void Logger::printString(LogType _type,std::string _module, std::string _str){
    std::stringstream ss;

    if(_type != LogType::ERROR && blacklist.find(_module) != blacklist.end()){
        return;
    }

    ss << _module
       << " ["
       << Logger::log_type_map[_type]
          << "] "
          << _str
          << std::endl;

    std::cout << ss.str();

}

void Logger::logToDB(LogType _type, std::__cxx11::string _module, std::__cxx11::string _str){

    if(_type != LogType::ERROR && blacklist.find(_module) != blacklist.end()){
        return;
    }

    Poco::Data::Session session(session_pool_mysql->get());

    session << "INSERT INTO logs (type, module, text) VALUES(?, ?, ?)", use(log_type_map[_type]), use(_module), use(_str), now;

    session.close();
}

Logger::Logger(){
    this->handler = MySqlHandler::getInstance();
    createSessionPool();
}

bool Logger::createSessionPool(){
    bool success = true;

    Poco::Data::MySQL::Connector::registerConnector();

    this->db_connection_chain = handler->getConnectorString();

    try{

        session_pool_mysql = new Poco::Data::SessionPool(Poco::Data::MySQL::Connector::KEY,db_connection_chain, 0, 3, 10);

    }catch(Poco::Data::UnknownDataBaseException & ex){
        success = false;

        std::cout << ex.message() << std::endl;
    }

    return success;
}

Logger::~Logger(){
    this->session_pool_mysql->shutdown();
    delete this->session_pool_mysql;
//    std::cout << "Logger ending";
}

void Logger::blacklistModule(const std::__cxx11::string _module_name){
    FastMutex::ScopedLock lock(mutex);

    if(blacklist.find(_module_name) == blacklist.end()){
        blacklist.insert(_module_name);
    }
}

void Logger::logToLogBuf(LogType _type, std::__cxx11::string _module, std::__cxx11::string _str){
    std::stringstream ss;

    if(_type != LogType::ERROR && blacklist.find(_module) != blacklist.end()){
        return;
    }

    ss << _module
       << " ["
       << Logger::log_type_map[_type]
          << "] "
          << _str
          << std::endl;

    log_buf.push_back(ss.str());

    while(log_buf.size() > log_buf_size){
        log_buf.pop_front();
    }
}

void Logger::setWriteToHtmlStream(const bool _enable){
    FastMutex::ScopedLock lock(mutex);

    this->log_buf_enabled = _enable;
}

void Logger::setLogBufSize(const unsigned int _size){
    FastMutex::ScopedLock lock(mutex);

    this->log_buf_size = _size;
}

void Logger::streamLog(std::ostream &_out_stream){
    FastMutex::ScopedLock lock(mutex);

    for(std::deque<std::string>::iterator it = log_buf.begin(); it != log_buf.end(); ++it){
        _out_stream << *it;
    }

}

Poco::JSON::Object::Ptr Logger::jsonLog(){
    Poco::JSON::Object::Ptr json_obj = new Poco::JSON::Object();
    int i = 0;
    for(std::deque<std::string>::iterator it = log_buf.begin(); it != log_buf.end(); ++it){
        json_obj->set(("log"+std::to_string(i)),*it);
        i++;
    }
    return json_obj;
}

}
