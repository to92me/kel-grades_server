
#ifndef LOGGERFACTORY_H
#define LOGGERFACTORY_H

#include <string>

#include "Logger.h"

namespace Log {

/**
 * @brief is Logger interace. It has implemeted methos log debug error warning and info.
 *
 * @ingroup Log
 *
 * @author Tomislav Tumbas
 *
 * @version 1.1
 *
 * contact tumbas.tomislav@gmail.com tumbas@uns.ac.rs
 */
class LoggerInterface {
public:

    /**
     * @brief LoggerInterface constructor. Note after this You have to set Your module name
     */
    LoggerInterface();

    /**
     * @brief LoggerInterface contructor
     * @param _module is Your module name from witch are You logging
     */
    LoggerInterface(std::string _module);

    /**
     * @brief information logging
     * @param _info text
     */
    void info(std::string _info);

    /**
     * @brief debug logging
     * @param _debug text
     */
    void debug(std::string _debug);

    /**
     * @brief error logging
     * @param _error text
     */
    void error(std::string _error);

    /**
     * @brief warning logging
     * @param _warning text
     */
    void warning(std::string _warning);

    /**
     * @brief set module name
     * @param _module is module name
     */
    inline void setModule(std::string _module){
        this->module = _module;
        module_set = true;
    }

    /**
     * @brief check if module is set
     * @return set/not set
     */
    inline bool isModuleSet(){
        return this->module_set;
    }

    /**
     * @brief getLoggerInstance singleton
     * @return instance of logger
     */
    inline Logger* getLoggerInstance(){
       return this->logger;
    }


private:
    std::string module = "NOT SET";
    bool module_set = false;

    Logger* logger;
};

}

#endif
