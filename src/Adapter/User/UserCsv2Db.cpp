#include "UserCsv2Db.h"

namespace Adapter{

void UserCsv2Db::createUser(Poco::Net::HTTPRequest &req, Poco::Net::HTTPServerResponse &resp, Utils::CsvHandler &_csv_handler, Authorization::Level &_level, Authorization::Level &_level_new_user){
    Session session(Poco::Data::MySQL::Connector::KEY,my_sql_handler->getConnectorString());

    if(session.isConnected() == true){

        for(int i = 1; i < _csv_handler.size(); i++){

            if(_csv_handler.get(i).size() > 5){
                //check if id is duplicate
                Poco::Nullable<std::string> db_name_first;
                Poco::Nullable<std::string> db_name_second;
                std::string db_id = _csv_handler.get(i,0);


                // try to get user by this id
                session << "SELECT \
                           user.name_first, \
                        user.name_second \
                        FROM \
                        user \
                        WHERE \
                        user.user_id = ?;",
                into(db_name_first), into(db_name_second),
                        use(db_id),
                        now;

                // if there is user by this id first and second name will not be null
                // @todo check if first and second name are same
                if(db_name_first.isNull() == false &&
                        db_name_second.isNull() == false){
                    _csv_handler.pushElementBack(i,"User by this ID already exists");

                    // else create new user
                }else{
                    Poco::Nullable<std::string> db_telephone;
                    std::string db_email;
                    std::string db_type;
                    Poco::Nullable<bool> db_admin;
                    std::string db_credentials_passwd;

                    db_name_first = _csv_handler.get(i,1);
                    db_name_second = _csv_handler.get(i,2);
                    db_email = _csv_handler.get(i,3);
                    if(_csv_handler.get(i,4).compare(_csv_handler.getEmptyElement()) != 0) db_telephone = _csv_handler.get(i,4);
                    db_type = Authorization::getAuthorizationLevelString(_level_new_user);
                    db_admin = Authorization::getAuthorizationAdminBool(_level_new_user);
                    db_credentials_passwd = _csv_handler.get(i,5);

                    info("Creating new user. id: " + db_id + " , " + db_name_first.value() + " , " + db_name_second.value());

                    session << "INSERT INTO \
                               user (user_id, name_first, name_second, email, telephone, type, admin)\
                               VALUES (?, ?, ?, ?, ?, ?, ?)",
                               use(db_id),
                            use(db_name_first),
                            use(db_name_second),
                            use(db_email),
                            use(db_telephone),
                            use(db_type),
                            use(db_admin),
                            now;

                    session << "INSERT INTO \
                               credentials(user_id,passwd) \
                               VALUES(?, ?);",
                    use(db_id),
                            use(db_credentials_passwd),
                            now;

                    _csv_handler.pushElementBack(i,"User created");
                }
            }else{
                _csv_handler.pushElementBack(i,"Error in elements");
            }
        }
        session.close();
    }else{
        Utils::ErrorHandler err_h;
        err_h.genErrMssgDbUnreachable(resp);
    }
}


void UserCsv2Db::updateUser(Poco::Net::HTTPRequest &req, HTTPServerResponse &resp, Utils::CsvHandler &_csv_handler, Authorization::Level &_level, Authorization::Level &_level_new_user){
    Session session(Poco::Data::MySQL::Connector::KEY,my_sql_handler->getConnectorString());

    if(session.isConnected() == true){

        for(int i = 1; i < _csv_handler.size(); i++){

            if(_csv_handler.get(i).size() > 5){
                //check if id is duplicate
                Poco::Nullable<std::string> db_name_first;
                Poco::Nullable<std::string> db_name_second;
                std::string db_id = _csv_handler.get(i,0);


                    // try to get user by this id
                    session << "SELECT \
                               user.name_first, \
                            user.name_second \
                            FROM \
                            user \
                            WHERE \
                            user.user_id = ?;",
                    into(db_name_first), into(db_name_second),
                            use(db_id),
                            now;

                    // if there is no user by this id first and second name will be null
                    if(db_name_first.isNull() != false &&
                            db_name_second.isNull() != false){
                        _csv_handler.pushElementBack(i,"User by this ID does not exists");

                        // else update user
                    }else{
                        Poco::Nullable<std::string> db_telephone;
                        std::string db_email;
                        std::string db_type;
                        Poco::Nullable<bool> db_admin;
                        std::string db_credentials_passwd;

                        db_name_first = _csv_handler.get(i,1);
                        db_name_second = _csv_handler.get(i,2);
                        db_email = _csv_handler.get(i,3);
                        if(_csv_handler.get(i,4).compare(_csv_handler.getEmptyElement()) != 0) db_telephone = _csv_handler.get(i,4);
                        db_type = Authorization::getAuthorizationLevelString(_level_new_user);
                        db_admin = Authorization::getAuthorizationAdminBool(_level_new_user);
                        db_credentials_passwd = _csv_handler.get(i,5);

                        info("updateing user. id: " + db_id + " , " + db_name_first.value() + " , " + db_name_second.value());

                        session << "UPDATE user \
                                   SET \
                                   user.name_first = ?, \
                                user.name_second = ?, \
                                user.email = ?, \
                                user.telephone = ?, \
                                user.type = ?, \
                                user.admin = ? \
                                    WHERE \
                                    user.user_id = ?; ",
                        use(db_name_first),
                                use(db_name_second),
                                use(db_email),
                                use(db_telephone),
                                use(db_type),
                                use(db_admin),
                                use(db_id),
                                now;

                        session << "UPDATE credentials \
                                   SET \
                                credentials.passwd = ? \
                                    WHERE \
                                    credentials.user_id = ?;",
                                use(db_credentials_passwd),
                                use(db_id),
                                now;

                        _csv_handler.pushElementBack(i,"User updated");
                    }
            }else{
                _csv_handler.pushElementBack(i,"Error in elements");
            }
        }
        session.close();
    }else{
        Utils::ErrorHandler err_h;
        err_h.genErrMssgDbUnreachable(resp);
    }
}

}
