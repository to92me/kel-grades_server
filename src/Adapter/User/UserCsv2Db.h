#ifndef KELGS_ADAPTER_USER_USERCSV2DB_H
#define KELGS_ADAPTER_USER_USERCSV2DB_H


#include "Logger/Interface.h"
#include "MySqlHandler/MySqlHandler.h"
#include "Authorization/Authorization.h"

#include "Utils/UriParser.h"
#include "Utils/ErrorHandler.h"
#include "Utils/StdString2HttpMethod.h"
#include "Utils/CsvHandler.h"

#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/JSON/Object.h"

using Poco::Net::HTTPServerRequest;
using Poco::Net::HTTPServerResponse;
using Poco::Data::Session;

using Utils::UriParser;
using Log::LoggerInterface;
using Credentials::Authorization;

namespace Adapter {

/**
 * @brief creates Users from Csv file 
 * @ingroup Adapter
 * @author Tomislav Tubas
 *
 *
 * @version 0.1
 *
 * @note not done
 * @date 17.2.2017
 *
 * contact tumbas.tomislav@gmail.com tumbas@uns.ac.rs
 */
class UserCsv2Db : public LoggerInterface{
public:

    /**
     * @brief Constructor
     */
    UserCsv2Db():LoggerInterface("UserCsv2Db"){
        my_sql_handler = MySqlHandler::getInstance();
    }


    /**
   * @brief updateUser updates user from CSV file. For more information about CSV file template for this method look at @see API documentation.
   * @param req is HTTP Server Request. 
   * @param resp is HTTP Server Response.
   * @param _csv_handler is CsvHandler with CSV file.
   * @param _level is @see Authorization::Level of currently logged in user. This information is used to chose if logged in user has rights to update users to new Authorization level.
   * @param _level_new_user is requested @see Authorization::Level to update Users. 
   */
  void updateUser(Poco::Net::HTTPRequest &req, HTTPServerResponse &resp, Utils::CsvHandler &_csv_handler, Authorization::Level &_level, Authorization::Level &_level_new_user);

  /**
   * @brief createUser creates user from CSV file. For more information about CSV file template for this method look at @see API documentation.
   * @param req is HTTP Server Request. 
   * @param resp is HTTP Server Response.
   * @param _csv_handler is CsvHandler with CSV file.
   * @param _level is @see Authorization::Level of currently logged in user. This information is used to chose if logged in user has rights to create users with requested level.
   * @param _level_new_user is requested @see Authorization::Level for new Users. 
   */
  void createUser(Poco::Net::HTTPRequest &req, HTTPServerResponse &resp, Utils::CsvHandler &_csv_handler, Authorization::Level &_level, Authorization::Level &_level_new_user);



  /**
   * @brief empty_telephone. If for telephone number stays this string, telephone in Database will be NULL. 
   */
  const std::string empty_telephone = "-";

private:
  
   MySqlHandler* my_sql_handler; ///< mysql handler with configuration string of Session

};

}

#endif

