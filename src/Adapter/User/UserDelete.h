#ifndef KELGS_ADAPTER_USER_USERDELETE_H
#define KELGS_ADAPTER_USER_USERDELETE_H

#include "Logger/Interface.h"
#include "MySqlHandler/MySqlHandler.h"
#include "Authorization/Authorization.h"

#include "Utils/UriParser.h"
#include "Utils/ErrorHandler.h"

#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/JSON/Object.h"

using Poco::Net::HTTPServerRequest;
using Poco::Net::HTTPServerResponse;
using Poco::Data::Session;

using Utils::UriParser;
using Log::LoggerInterface;
using Credentials::Authorization;

namespace Adapter{

/**
 * @brief delete User from Database.
 * @ingroup Adapter
 * @author Tomislav Tubas
 *
 * If Status.user_id is not found in Databse adapter will return false.
 *
 * @version 0.1
 *
 * @note not done
 * @date 1.12.2016
 *
 * contact tumbas.tomislav@gmail.com tumbas@uns.ac.rs
 */
class UserDelete: public LoggerInterface{
public:

    /**
     * @brief Constructor
     */
    UserDelete(): LoggerInterface("UserDelete"){
        my_sql_handler = MySqlHandler::getInstance();
    }

    /**
     * @brief deleteUser deletes from database user with given id 
     * @param _user_id is user id
     * @return true if access to database is successful 
     */
    bool deleteUser(std::string _user_id);

private:
    MySqlHandler* my_sql_handler; ///< mysql handler with configuration string of Session

};
}

#endif
