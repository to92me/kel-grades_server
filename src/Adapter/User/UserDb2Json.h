#ifndef KELGS_ADAPTER_USER_USERDB2JSON_H
#define KELGS_ADAPTER_USER_USERDB2JSON_H

#include "Logger/Interface.h"
#include "MySqlHandler/MySqlHandler.h"
#include "Authorization/Authorization.h"

#include "Utils/UriParser.h"
#include "Utils/ErrorHandler.h"
#include "Utils/StdString2AuthorizationLevel.h"

#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/JSON/Object.h"

using Poco::Net::HTTPServerRequest;
using Poco::Net::HTTPServerResponse;
using Poco::Data::Session;

using Utils::UriParser;
using Log::LoggerInterface;
using Credentials::Authorization;

namespace Adapter{

/**
 * @brief creates User Json Object from Database.
 * @ingroup Adapter
 * @author Tomislav Tubas
 *
 * If Status.user_id is not found in Databse UserDb2Json will return NULL
 *
 * @version 0.1
 *
 * @note not done
 * @date 1.12.2016
 *
 * contact tumbas.tomislav@gmail.com tumbas@uns.ac.rs
 */
class UserDb2Json : public LoggerInterface{
public:

    /**
     * @brief Constructor
     */
    UserDb2Json(): LoggerInterface("UserDb2Json"){
        my_sql_handler = MySqlHandler::getInstance();
    }

    /**
     * @brief getUser gets from database User infmation with given user id.
     * @param _level is Authorization::Level of logged in user.
     * @param _id is user id.
     * @return pointer JSON array with user information.
     */
    Poco::JSON::Array::Ptr getUser(Authorization::Level &_level, std::__cxx11::string _id);

    /**
     * @brief getUsersByType gets from database all Users with given type. 
     * @param _type is type of user (student, professor or admin)
     * @return pointer JSON array with users information.
     */
    Poco::JSON::Array::Ptr getUsersByType(std::string _type);

private:
    Utils::StdString2AuthorizationLevel type_converter; ///< converts string (student, professor, admin) and adnmin bool to Authorization::Level. 
    MySqlHandler* my_sql_handler; ///< mysql handler with configuration string of Session.

};
}

#endif
