#include "UserDb2Json.h"

namespace Adapter{

Poco::JSON::Array::Ptr UserDb2Json::getUser(Authorization::Level &_level, std::__cxx11::string _id){
    Session session(Poco::Data::MySQL::Connector::KEY,my_sql_handler->getConnectorString());

    if(session.isConnected() == true){

        Poco::Nullable<std::string> db_name_first;
        Poco::Nullable<std::string> db_name_second;
        Poco::Nullable<std::string> db_email;
        Poco::Nullable<std::string> db_tel;
        Poco::Nullable<std::string> db_type;
        Poco::Nullable<bool> db_admin;

        Poco::JSON::Array::Ptr json_array = new Poco::JSON::Array;

        session << "SELECT \
                    user.name_first, \
                user.name_second, \
                user.email, \
                user.telephone, \
                user.type, \
                user.admin \
                FROM \
                user \
                WHERE \
                user.user_id = ?;",
        into(db_name_first), into(db_name_second), into(db_email), into(db_tel), into(db_type), into(db_admin),
                use(_id),
                now;
        // note telephone can be null, because filed in databse telephone is not set as not_null.
        if(db_name_first.isNull() == false &&
                db_name_second.isNull() == false &&
                db_email.isNull() == false &&
                db_type.isNull() == false &&
                db_admin.isNull() == false){

            Poco::JSON::Object::Ptr user_item = new Poco::JSON::Object();
            user_item->set("name_first", db_name_first.value());
            user_item->set("name_second", db_name_second.value());
            user_item->set("email", db_email.value());

            if(db_tel.isNull()){
                user_item->set("telephone","");
            }else{
                user_item->set("telephone",db_tel.value());
            }

            user_item->set("type", db_type.value());
            user_item->set("admin", db_admin.value());

            json_array->add(user_item);

            _level = type_converter.convert(db_type.value(), db_admin.value());


        }
        session.close();
        return json_array;
    }else{
        return NULL;
    }
}

Poco::JSON::Array::Ptr UserDb2Json::getUsersByType(std::string _type){
    typedef Poco::Tuple<std::string, std::string, std::string, std::string, Poco::Nullable<std::string>, std::string, bool> User;
    Session session(Poco::Data::MySQL::Connector::KEY,my_sql_handler->getConnectorString());
    Poco::JSON::Array::Ptr json_array = new Poco::JSON::Array;

    if(session.isConnected() == true){
        std::vector<User> users;

        session << "SELECT \
                   user.user_id, \
                   user.name_first, \
                   user.name_second, \
                   user.email, \
                   user.telephone, \
                   user.type, \
                   user.admin \
               FROM \
                   user \
               WHERE \
                   user.type = ?;",
        into(users),use(_type),now;

        for(std::vector<User>::iterator it = users.begin(); it != users.end(); ++it){
            Poco::JSON::Object::Ptr json_user = new Poco::JSON::Object;

            json_user->set("id",it->get<0>());
            json_user->set("first name",it->get<1>());
            json_user->set("second name",it->get<2>());
            json_user->set("email",it->get<3>());
            if((it->get<4>()).isNull()){
                json_user->set("telephone","");
            }else{
                json_user->set("telephone",(it->get<4>()).value());
            }
            json_user->set("type",it->get<5>());
            json_user->set("admin",it->get<6>());

            json_array->add(json_user);
        }
    }else{
        error("Dabase unreachable");
    }
    return json_array;

}

}
