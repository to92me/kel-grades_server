#include "UserDelete.h"

namespace Adapter{

bool UserDelete::deleteUser(std::string _user_id){
    Session session(Poco::Data::MySQL::Connector::KEY,my_sql_handler->getConnectorString());

    if(session.isConnected()){

        // delete exams of user
        session <<"DELETE FROM exam \
                  WHERE \
                  exam.user_id = ?;",
        use(_user_id), now;

        // delete credentials of user
        session <<"DELETE FROM credentials \
                  WHERE \
                  credentials.user_id = ?;",
        use(_user_id), now;

        // delete user from subject table
        session <<"DELETE FROM student_subject \
                  WHERE \
                  student_subject.user_id = ?;",
        use(_user_id), now;

        // delete user from professor table
        session <<"DELETE FROM professor_subject \
                  WHERE \
                  professor_subject.professor_id = ?;",
        use(_user_id), now;

        // delete user
        session << "DELETE FROM user \
                   WHERE \
                   user.user_id = ?;",
        use(_user_id), now;
        return true;
    }else{
        return false;
    }
}

}
