#include "LogsDb2Json.h"

namespace Adapter{

Poco::JSON::Array::Ptr LogsDb2Json::execute(){
    typedef Poco::Tuple<std::string, Poco::DateTime, std::string, std::string> Log;

     Session session(Poco::Data::MySQL::Connector::KEY,my_sql_handler->getConnectorString());

    if(session.isConnected() == true){
    std::vector<Log> logs;

    Poco::JSON::Array::Ptr poco_json_array_ptr = new Poco::JSON::Array();

    session << "SELECT \
                logs.log_id, \
                logs.date, \
                logs.type, \
                logs.text \
            FROM \
                logs;", into(logs), now;


    for(std::vector<Log>::iterator it = logs.begin(); it != logs.end(); ++it){
        Poco::JSON::Object::Ptr poco_json_obj_logs = new Poco::JSON::Object();

        poco_json_obj_logs->set("log_id", it->get<0>());
        poco_json_obj_logs->set("date",date_2_string.dateTime2String(it->get<1>()));
        poco_json_obj_logs->set("type", it->get<2>());
        poco_json_obj_logs->set("text", it->get<3>());

        poco_json_array_ptr->add(poco_json_obj_logs);
    }
    session.close();
    return poco_json_array_ptr;
    }else{
        return NULL;
    }
}


}
