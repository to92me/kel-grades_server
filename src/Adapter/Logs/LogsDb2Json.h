#ifndef KELGS_ADAPTER_LOGS_LOGSDB2JSON_h
#define KELGS_ADAPTER_LOGS_LOGSDB2JSON_h

#include "Logger/Interface.h"
#include "MySqlHandler/MySqlHandler.h"
#include "Authorization/Authorization.h"

#include "Utils/UriParser.h"
#include "Utils/ErrorHandler.h"
#include "Utils/PocoDate2StdString.h"

#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/JSON/Object.h"

using Poco::Net::HTTPServerRequest;
using Poco::Net::HTTPServerResponse;
using Poco::Data::Session;

using Utils::UriParser;
using Log::LoggerInterface;
using Credentials::Authorization;

namespace Adapter{

/**
 * @brief creates Logs Json Object from Database.
 * @ingroup Adapter
 * @author Tomislav Tubas
 *
 * @version 0.1
 *
 * @note not done
 * @date 1.12.2015
 *
 * contact tumbas.tomislav@gmail.com tumbas@uns.ac.rs
 */
class LogsDb2Json : public LoggerInterface{
public:

    /**
     * @brief Constructor
     */
    LogsDb2Json():LoggerInterface("LogsDb2Json"){
        my_sql_handler = MySqlHandler::getInstance();
    }
protected:
    /**
     * @brief execute gets all logs from database
     * @return Poco::JSON:Array with all logs from database
     */
    Poco::JSON::Array::Ptr execute();

private:
    Utils::PocoDate2StdString date_2_string; ///< from Poco::Date to date in string format
    MySqlHandler* my_sql_handler; ///< mysql handler with configuration string of Session


};
}

#endif
