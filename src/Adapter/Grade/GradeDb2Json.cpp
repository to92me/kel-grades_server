#include "GradeDb2Json.h"

namespace Adapter{

void GradeDb2Json::getGrade(std::string _student_id, int _subject_id, Poco::JSON::Object::Ptr _json_obj){
    typedef Poco::Tuple <int, int, int> Grade;
    Session session(Poco::Data::MySQL::Connector::KEY,my_sql_handler->getConnectorString());

    if(session.isConnected()){
        Grade grade;

        session << "SELECT \
                   student_subject.grade, \
                   student_subject.passed, \
                   student_subject.points \
               FROM \
                   student_subject \
               WHERE \
                   student_subject.subject_id = ? \
                       AND student_subject.user_id = ?;",
        into(grade),use(_subject_id),use(_student_id),now;

        session.close();

        _json_obj->set("grade",grade.get<0>());
        _json_obj->set("passed",grade.get<1>());
        _json_obj->set("points",grade.get<2>());

    }else{
        error("Database Unreachable");
    }
    return;
}
}
