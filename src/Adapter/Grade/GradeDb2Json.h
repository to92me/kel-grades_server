#ifndef KELGS_ADAPTER_PART_GRADEDB2JSON_H
#define KELGS_ADAPTER_PART_GRADEDB2JSON_H

#include "Logger/Interface.h"
#include "MySqlHandler/MySqlHandler.h"
#include "Authorization/Authorization.h"

#include "Utils/UriParser.h"
#include "Utils/ErrorHandler.h"
#include "Utils/PocoDate2StdString.h"

#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/JSON/Object.h"

using Poco::Net::HTTPServerRequest;
using Poco::Net::HTTPServerResponse;
using Poco::Data::Session;

using Utils::UriParser;
using Log::LoggerInterface;
using Credentials::Authorization;

namespace Adapter{

/**
 * @brief gets grades from database and returns it in JSON array; 
 * @ingroup Adapter
 * @author Tomislav Tubas
 * *
 * @version 0.1
 *
 * @note not done
 * @date september 2017
 *
 * contact tumbas.tomislav@gmail.com tumbas@uns.ac.rs
 */
class GradeDb2Json : public LoggerInterface{
public:

    /**
     * @brief Constructor
     */
    GradeDb2Json(): LoggerInterface("GradeDb2Json"){
        my_sql_handler = MySqlHandler::getInstance();
    }

    /**
     * @brief getGrade gets grades from database with given user(student) id and subject id
     * @param _student_id is id of user (student)
     * @param _subject_id is id of subject
     * @param _json_obj data got from database is in this JSON object stored
     */
    void getGrade(std::string _student_id, int _subject_id, Poco::JSON::Object::Ptr _json_obj = NULL);

 private:
    MySqlHandler* my_sql_handler; ///< mysql handler with configuration string of Session


};
}

#endif
