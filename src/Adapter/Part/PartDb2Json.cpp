#include "PartDb2Json.h"

namespace Adapter{

Poco::JSON::Array::Ptr PartDb2Json::getParts(int _subject_id){
    typedef Poco::Tuple <std::string, int, std::string> Part;
    Poco::JSON::Array::Ptr json_array = new Poco::JSON::Array();
    Session session(Poco::Data::MySQL::Connector::KEY,my_sql_handler->getConnectorString());

    if(session.isConnected()){
        std::vector<Part> parts;

        session << "SELECT \
                   subjectpart.name, \
                subjectpart.points_max, \
                subjectpart.type \
                FROM subjectpart \
                WHERE \
                subjectpart.subject_id = ? ;",
        into(parts),use(_subject_id),now;

        session.close();

        for(std::vector<Part>::iterator it = parts.begin(); it != parts.end(); ++it){
            Poco::JSON::Object::Ptr part = new Poco::JSON::Object();

            part->set("name",it->get<0>());
            part->set("points_max",it->get<1>());
            part->set("type",it->get<2>());

            json_array->add(part);
        }
    }else{
        error("Database Unreachable");
    }
    return json_array;
}

Poco::JSON::Array::Ptr PartDb2Json::getPartsWithExams(int _subject_id, std::string _student_id){
    typedef Poco::Tuple <int, std::string, int, std::string> Part;
    Poco::JSON::Array::Ptr json_array = new Poco::JSON::Array();
    Session session(Poco::Data::MySQL::Connector::KEY,my_sql_handler->getConnectorString());

    if(session.isConnected()){
        std::vector<Part> parts;
        ExamDb2Json exam_db_2_json;

        session << "SELECT \
                   subjectpart.subjectpart_id, \
                   subjectpart.name, \
                subjectpart.points_max, \
                subjectpart.type \
                FROM subjectpart \
                WHERE \
                subjectpart.subject_id = ? ;",
        into(parts),use(_subject_id),now;

        session.close();

        for(std::vector<Part>::iterator it = parts.begin(); it != parts.end(); ++it){
            Poco::JSON::Object::Ptr part = new Poco::JSON::Object();

            part->set("name",it->get<1>());
            part->set("points_max",it->get<2>());
            part->set("type",it->get<3>());

            Poco::JSON::Array::Ptr exam_array  = exam_db_2_json.getExam(it->get<0>(),_student_id);

            part->set("exam", exam_array);
            json_array->add(part);
        }
    }else{
        error("Database Unreachable");
    }
    return json_array;
}

Poco::JSON::Array::Ptr PartDb2Json::getParts(std::string _subject_name){
    typedef Poco::Tuple <std::string, int, std::string> Part;
    Session session(Poco::Data::MySQL::Connector::KEY,my_sql_handler->getConnectorString());
    Poco::JSON::Array::Ptr json_array = new Poco::JSON::Array();

    if(session.isConnected()){
        std::vector<Part> parts;

        session << "SELECT \
                   subjectpart.subjectpart_id, \
                subjectpart.name, \
                subjectpart.points_max, \
                subjectpart.type \
                FROM subjectpart \
                JOIN subject ON \
                subjectpart.subject_id = subject.subject_id \
                WHERE \
                subject.name = ? ;",
        into(parts),use(_subject_name),now;

        session.close();

        for(std::vector<Part>::iterator it = parts.begin(); it != parts.end(); ++it){
            Poco::JSON::Object::Ptr part = new Poco::JSON::Object();

            part->set("name",it->get<0>());
            part->set("points_max",it->get<1>());
            part->set("type",it->get<2>());

            json_array->add(part);
        }
    }else{
        error("Database Unreachable");
    }
    return json_array;

}

Poco::JSON::Array::Ptr PartDb2Json::getPartsWithExams(std::string _subject_name, std::string _student_id){
    typedef Poco::Tuple <int, std::string, int, std::string> Part;
    Session session(Poco::Data::MySQL::Connector::KEY,my_sql_handler->getConnectorString());
    Poco::JSON::Array::Ptr json_array = new Poco::JSON::Array();

    if(session.isConnected()){
        std::vector<Part> parts;
        ExamDb2Json exam_db_2_json;

        session << "SELECT \
                   subjectpart.subjectpart_id, \
                subjectpart.name, \
                subjectpart.points_max, \
                subjectpart.type \
                FROM subjectpart \
                JOIN subject ON \
                subjectpart.subject_id = subject.subject_id \
                WHERE \
                subject.name = ? ;",
        into(parts),use(_subject_name),now;

        for(std::vector<Part>::iterator it = parts.begin(); it != parts.end(); ++it){
            Poco::JSON::Object::Ptr part = new Poco::JSON::Object();

            part->set("name",it->get<1>());
            part->set("points_max",it->get<2>());
            part->set("type",it->get<3>());

            Poco::JSON::Array::Ptr exam_array  = exam_db_2_json.getExam(it->get<0>(),_student_id);

            part->set("exam", exam_array);
            json_array->add(part);
        }
    }else{
        error("Database Unreachable");
    }
    return json_array;
}
}
