#ifndef KELGS_ADAPTER_PART_PARTDB2JSON_H
#define KELGS_ADAPTER_PART_PARTDB2JSON_H

#include "Logger/Interface.h"
#include "MySqlHandler/MySqlHandler.h"
#include "Authorization/Authorization.h"

#include "Adapter/Exam/ExamDb2Json.h"

#include "Utils/UriParser.h"
#include "Utils/ErrorHandler.h"
#include "Utils/PocoDate2StdString.h"
#include "Utils/CsvHandler.h"

#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/JSON/Object.h"

using Poco::Net::HTTPServerRequest;
using Poco::Net::HTTPServerResponse;
using Poco::Data::Session;

using Utils::UriParser;
using Log::LoggerInterface;
using Credentials::Authorization;
using Utils::CsvHandler;

namespace Adapter{

/**
 * @brief gets subject parts from database
 * @ingroup Adapter
 * @author Tomislav Tubas
 *
 * @version 0.1

 * @note 

 * @date march 2017
 *
 * contact tumbas.tomislav@gmail.com tumbas@uns.ac.rs
 */
class PartDb2Json : public LoggerInterface{
public:

    /**
     * @brief Constructor
     */
    PartDb2Json(): LoggerInterface("PartDb2Json"){
        my_sql_handler = MySqlHandler::getInstance();
    }

    /**
     * @brief getParts gets subject parts from database with given subject id and returns JSON with subject parts. 
     * @param _subject_id is subject id.
     * @return pointer to JSON array with subject parts information.
     */
    Poco::JSON::Array::Ptr getParts(int _subject_id);

    /**
     * @brief getParts gets subject parts from database with given subject name and returns JSON with subject parts. 
     * @param _subject_name is subject name. 
     * @return pointer to JSON array with subject parts information.
     */
    Poco::JSON::Array::Ptr getParts(std::string _subject_name);

    /**
     * @brief getPartsWithExams gets subject parts from database with given subject id and student (user) id and returns JSON with subject parts.  
     * @param _subject_id is subject id.
     * @param _student_id is user id of student.
     * @return  pointer to JSON array with subject parts information.
     */
    Poco::JSON::Array::Ptr getPartsWithExams(int _subject_id, std::string _student_id);

    /**
     * @brief getPartsWithExams gets subject parts and exams from database with given subject name and student (user) id and returns JSON with subject parts.
     * @param _subject_name is subject name.
     * @param _student_id is user id of student.
     * @return  pointer to JSON array with subject parts and exams.
     */
    Poco::JSON::Array::Ptr getPartsWithExams(std::string _subject_name, std::string _student_id);

    /**
     * @brief createPart creates parts given in CSV file. For more information about CSV file template for this method look at @see API documentation
     * @param _csv_handler CsvHandler instance with sent CSV file
     * @return true if access to database is successful 
     * @todo move this to new file called PartCsv2Db
     */
    bool createPart(CsvHandler &_csv_handler);

 private:
    MySqlHandler* my_sql_handler; ///< mysql handler with configuration string of Session


};
}

#endif
