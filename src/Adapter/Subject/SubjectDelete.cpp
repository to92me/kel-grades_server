#include "SubjectDelete.h"

namespace Adapter{

bool SubjectDelete::deleteSubject(int _subject_id){
    Session session(Poco::Data::MySQL::Connector::KEY,my_sql_handler->getConnectorString());

    if(session.isConnected()){

        // delete exams of subject
        session <<"DELETE exam FROM exam \
                  JOIN \
              subjectpart ON subjectpart.subjectpart_id = exam.subjectpart_id \
          WHERE \
              subjectpart.subject_id = ?;",
        use(_subject_id), now;

        // delete all parts of subject
        session <<"DELETE FROM subjectpart \
                  WHERE \
                      subjectpart.subject_id = ?;",
        use(_subject_id), now;

        // delete subject from students
        session <<"DELETE FROM student_subject \
                  WHERE \
                      student_subject.subject_id = ?;",
        use(_subject_id), now;

        // delete subject from professors
        session <<"DELETE FROM professor_subject \
                  WHERE \
                      professor_subject.subject_id = ?;",
        use(_subject_id), now;

        // delete subject
        session << "DELETE FROM subject \
                   where \
                   subject.subject_id = ?;",
        use(_subject_id), now;

        session.close();
        return true;
    }else{
        error("Database Unreachable");
        return false;
    }
}

bool SubjectDelete::deleteSubject(std::string _subject_name){
    Session session(Poco::Data::MySQL::Connector::KEY,my_sql_handler->getConnectorString());

    if(session.isConnected()){

        //database safe mode does not alow multiple joins on update and delete, so get subject id first and delete by id
        //

        int subject_id;

        session << "SELECT \
                   subject.subject_id  \
               FROM  \
                   subject  \
               WHERE  \
                   subject.name = ?;",
        into(subject_id), use(_subject_name), now;

        // delete exams of subject
        session <<"DELETE exam FROM exam \
                  JOIN \
              subjectpart ON subjectpart.subjectpart_id = exam.subjectpart_id \
          WHERE \
              subjectpart.subject_id = ?;",
        use(subject_id), now;

        // delete all parts of subject
        session <<"DELETE FROM subjectpart \
                  WHERE \
                      subjectpart.subject_id = ?;",
        use(subject_id), now;

        // delete subject from students
        session <<"DELETE FROM student_subject \
                  WHERE \
                      student_subject.subject_id = ?;",
        use(subject_id), now;

        // delete subject from professors
        session <<"DELETE FROM professor_subject \
                  WHERE \
                      professor_subject.subject_id = ?;",
        use(subject_id), now;

        // delete subject
        session << "DELETE FROM subject \
                   where \
                   subject.subject_id = ?;",
        use(subject_id), now;
        return true;
    }else{
        error("Database Unreachable");
        return false;
    }
}

}
