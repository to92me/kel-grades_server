#ifndef KELGS_ADAPTER_SUBJECT_SUBJECTDB2JSON_H
#define KELGS_ADAPTER_SUBJECT_SUBJECTDB2JSON_H

#include "Logger/Interface.h"
#include "MySqlHandler/MySqlHandler.h"
#include "Authorization/Authorization.h"

#include "Utils/UriParser.h"
#include "Utils/CsvHandler.h"
#include "Utils/ErrorHandler.h"

#include "Adapter/Part/PartDb2Json.h"

#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/JSON/Object.h"

using Poco::Net::HTTPServerRequest;
using Poco::Net::HTTPServerResponse;

using Log::LoggerInterface;
using Credentials::Authorization;
using Poco::Data::Session;
using Utils::UriParser;

namespace Adapter{

/**
 * @brief creates Subject Json Object from Data base.
 * @ingroup Adapter
 * @author Tomislav Tubas
 *
 * If Status.user_id is not found in Databse UserDb2Json will return NULL
 *
 * @version 0.1
 *
 * @note not done
 * @date 1.12.2015
 *
 * contact tumbas.tomislav@gmail.com tumbas@uns.ac.rs
 */
class SubjectDb2Json : public LoggerInterface{
public:

    /** Tuple containing Subject information: subject_id, subject name, pass string, semester and ECTS points. */
    typedef Poco::Tuple<int, std::string, std::string, std::string, int> Subject;

    /** Tuple containing Professor(User) information : user_id, first name, second name, email, telephone and type.*/
    typedef Poco::Tuple<std::string, std::string, std::string, std::string, Poco::Nullable<std::string>,std::string> Professor;

    /**
     * @brief Constructor
     */
    SubjectDb2Json(): LoggerInterface("SubjectDb2Json"){
        my_sql_handler = MySqlHandler::getInstance();
    }

    ~SubjectDb2Json(){}

    /**
     * @brief getSubjectById gets subject with given id from database and returns it in JSON.
     * @param _subject_id is id of subject.
     * @return  JSON array pointer with subject information.
     * @see API documentation for more information about JSON array template.
     */
    Poco::JSON::Array::Ptr getSubjectById(int _subject_id);

    /**
     * @brief getSubjectByName gets subject with given name from database and returns it in JSON.
     * @param _subject_name is name of subject.
     * @return JSON array pointer with subject information.
     * @see API documentation for more information about JSON array template.
     */
    Poco::JSON::Array::Ptr getSubjectByName(std::string _subject_name);

    /**
     * @brief getSubjectsByUser gets Subjects from database in witch user with given id is involved and returns them in JSON array.
     * @param _level is Authorization level from witch method gets if user is professor or student. 
     * @param _user_id is user id.
     * @return JSON array pointer with subject information
     * @todo split to two methods.
     */
    Poco::JSON::Array::Ptr getSubjectsByUser(Authorization::Level &_level, std::string _user_id);

    /**
     * @brief getSubjectWithPartsAndExams gets from database subject with all students on subject with given id, parts of subject and exams of parts.
     * @param _subject_id is subject id.
     * @return JSON array pointer with all students on subject, parts of subject and exams of parts.
     * @see API documentation for more information about JSON array template.
     */
    Poco::JSON::Array::Ptr getSubjectWithPartsAndExams(int _subject_id);

    /**
     * @brief getSubjectWithPartsAndExams gets from database subject with all students on subject with given name, parts of subject and exams of parts.
     * @param _subject_name is subject name
     * @return JSON array pointer with all students on subject, parts of subject and exams of parts.
     * @see API documentation for more information about JSON array template.
     */
    Poco::JSON::Array::Ptr getSubjectWithPartsAndExams(std::string _subject_name);

    /**
     * @brief getSubjectWithPartsAndExams gets from database subject information with given id and all parts of subject and exams of parts of student with given user id.
     * @param _subject_id is subject id.
     * @param _student_id is student id.
     * @return JSON array pointer with subject, student parts of subject and exams of parts.
     * @see API documentation for more information about JSON array template.
     */
    Poco::JSON::Array::Ptr getSubjectWithPartsAndExams(int _subject_id, std::string _student_id);

    /**
     * @brief getSubjectWithPartsAndExams gets from database subject information with given name and all parts of subject and exams of parts of student with given user id.
     * @param _subject_id is subject id.
     * @param _student_id is student id.
     * @return JSON array pointer with subject, student, parts of subject and exams of parts.
     * @see API documentation for more information about JSON array template.
     */
    Poco::JSON::Array::Ptr getSubjectWithPartsAndExams(std::string _subject_name, std::string _student_id);

    /**
     * @brief getSubjectsWithPartsAndExams gets subject in witch student with given user id is involved with all parts of student and exams of student with given user id.
     * @param _student_id is student id.
     * @return JSON array pointer with subject, student, parts of subject and exams of parts.
     * @see API documentation for more information about JSON array template.
     */
    Poco::JSON::Array::Ptr getSubjectsWithPartsAndExams(std::string _student_id);

    /**
     * @brief getAllSubjects gets from database all subjects.
     * @return JSON array pointer with all subjects
     */
    Poco::JSON::Array::Ptr getAllSubjects();

private:
    MySqlHandler* my_sql_handler; ///< mysql handler with configuration string of Session


};
}

#endif
