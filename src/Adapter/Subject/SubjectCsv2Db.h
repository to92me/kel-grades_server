#ifndef SUBJECTCSV2DB_H
#define SUBJECTCSV2DB_H

#include "Utils/StdString2HttpMethod.h"

#include "Logger/Interface.h"
#include "MySqlHandler/MySqlHandler.h"
#include "Authorization/Authorization.h"

#include "Utils/UriParser.h"
#include "Utils/CsvHandler.h"
#include "Utils/ErrorHandler.h"

#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/JSON/Object.h"

using Poco::Net::HTTPServerRequest;
using Poco::Net::HTTPServerResponse;

using Log::LoggerInterface;
using Credentials::Authorization;
using Poco::Data::Session;
using Utils::UriParser;

namespace Adapter{

/**
 * @brief creates Subject and Subject Parts from CSV file 
 *
 * @ingroup 
 *
 * @author Tomislav Tubas
 *
 * @version 0.1
 *
 * @note not done
 * @date 17.2.2017
 *
 * contact tumbas.tomislav@gmail.com tumbas@uns.ac.rs
 */
class SubjectCsv2Db : public LoggerInterface {
public:

    /**
     * @brief Constructor
     */
    SubjectCsv2Db() : LoggerInterface("SubjectCsv2Db"){
        my_sql_handler = MySqlHandler::getInstance();
    }

    /**
   * @brief createSubject creates new subjects in data base. For more information about CSV file template for this method look at @see API documentation.
   * @param _csv_handler CsvHandler instance with sent CSV file
   * @return true  if access to database is successful 
   */
  bool createSubject(Utils::CsvHandler &_csv_handler);

  /**
   * @brief updateSubject updates subjects in data base. For more information about CSV file template for this method look at @see API documentation.
   * @param _csv_handler CsvHandler instance with sent CSV file
   * @return true  if access to database is successful 
   */
  bool updateSubject(Utils::CsvHandler &_csv_handler);

  /**
   * @brief createSubjectParts not implemented yet
   * @param _csv_handler CsvHandler instance with sent CSV file
   * @todo not implemented, waiting professor with direction
   * @return true  if access to database is successful 
   */
  bool createSubjectParts(Utils::CsvHandler &_csv_hadler);

  /**
   * @brief updateSubjectParts
   * @todo not implemented, waiting professor with directions
   * @param _csv_handler CsvHandler instance with sent CSV file
   * @return true  if access to database is successful 
   */
  bool updateSubjectParts(Utils::CsvHandler &_csv_handler);


  /**
   * @brief updateSubjectStaff updates professors and assistants on subject
   * @param _csv_handler CsvHandler instance with sent CSV file
   * @return true  if access to database is successful
   */
  bool updateSubjectStaff(Utils::CsvHandler &_csv_handler);

  /**
   * @brief createSubjectPartsAndAddStudents creates parts for subject and adds students on subject
   * @param _csv_handler CsvHandler instance with sent CSV file
   * @return true  if access to database is successful
   */
  bool createSubjectPartsAndAddStudents(Utils::CsvHandler &_csv_handler);

  /**
   * @brief updateStudentsGradesAndScores updates student grades and exam scores (editing date with date of update)
   * @param _csv_handler CsvHandler instance with sent CSV file
   * @return true  if access to database is successful
   * @bug not checking if user is student on subject
   * @bug crap code
   */
  bool updateStudentsGradesAndScores(Utils::CsvHandler &_csv_handler);

private:

  MySqlHandler* my_sql_handler; ///< mysql handler with configuration string of Session

  enum Association{
      PROFESSOR,
      ASSISTANT,
      NONE
  };

  typedef std::map<std::string, Association> user_subject_association_map;
  static user_subject_association_map user_subject_association;

};
}

#endif
