#ifndef KELGS_ADAPTER_SUBJECT_SUBJECTDELETE_H
#define KELGS_ADAPTER_SUBJECT_SUBJECTDELETE_H

#include "Logger/Interface.h"
#include "MySqlHandler/MySqlHandler.h"
#include "Authorization/Authorization.h"

#include "Utils/UriParser.h"
#include "Utils/CsvHandler.h"
#include "Utils/ErrorHandler.h"

#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/JSON/Object.h"

using Poco::Net::HTTPServerRequest;
using Poco::Net::HTTPServerResponse;

using Log::LoggerInterface;
using Credentials::Authorization;
using Poco::Data::Session;
using Utils::UriParser;

namespace Adapter{

/**
 * @brief The SubjectDelete class deletes Subject, Subject parts and exams from database
 *
 * @ingroup Adapter
 *
 * @author Tomislav Tubas
 *
 * @version 0.1
 *
 * @date 1.12.2015
 *
 * contact tumbas.tomislav@gmail.com tumbas@uns.ac.rs
 */
class SubjectDelete: public LoggerInterface{
public:

    SubjectDelete(): LoggerInterface("SubjectDelete"){
        my_sql_handler = MySqlHandler::getInstance();
    }

    /**
     * @brief deleteSubject deletes from database subject with give name, all subject parts and exams.
     * @param _subject_name is subject name
     * @return true if access to database is successful 
     */
    bool deleteSubject(std::string _subject_name);

    /**
     * @brief deleteSubject deletes from database subject with given id, all subject parts and exams.
     * @param _subject_id is subject id.
     * @return true if access to database is successful 
     */
    bool deleteSubject(int _subject_id);

private:
    MySqlHandler* my_sql_handler; ///< mysql handler with configuration string of Session
};
}

#endif
