#include "SubjectCsv2Db.h"

namespace Adapter{

SubjectCsv2Db::user_subject_association_map SubjectCsv2Db::user_subject_association = {
    {"professor", SubjectCsv2Db::Association::PROFESSOR},
    {"assistant", SubjectCsv2Db::Association::ASSISTANT},
    {"none", SubjectCsv2Db::Association::NONE}
};

bool SubjectCsv2Db::createSubject(Utils::CsvHandler &_csv_handler){

    Session session(Poco::Data::MySQL::Connector::KEY,my_sql_handler->getConnectorString());

    if(session.isConnected() == true){

        for(int i = 1; i < _csv_handler.size(); i++){
            if(_csv_handler.get(i).size() == 4){
                std::string subject_name = _csv_handler.get(i,0);
                Poco::Nullable<std::string> subject_passed_string;
                Poco::Nullable<int> subject_points_ects;
                Poco::Nullable<std::string> subject_semester;

                session << "SELECT \
                           subject.pass_condition, \
                        subject.points_ects, \
                        subject.semester \
                        FROM \
                        subject \
                        WHERE \
                        subject.name = ?;",
                into(subject_passed_string), into(subject_points_ects), into(subject_semester),
                        use(subject_name),
                        now;
                // if in db is no subject by this name add it
                if(subject_passed_string.isNull() && subject_points_ects.isNull() && subject_semester.isNull()){

                    subject_passed_string = _csv_handler.get(i,1);
                    //ects points must be a integer
                    try{
                        subject_points_ects = std::stoi(_csv_handler.get(i,2));
                    }catch(const std::invalid_argument& ia){
                        _csv_handler.pushElementBack(i,"ects points must be a number, error adding this subject");
                    }

                    //if ects is not number in csv then this nullable will be null
                    if(!subject_points_ects.isNull()){

                        subject_semester = _csv_handler.get(i,3);

                        session << "INSERT INTO subject(name, pass_condition, points_ects, semester) \
                                   VALUES(?,?,?,?);",
                        use(subject_name),
                                use(subject_passed_string),
                                use(subject_points_ects),
                                use(subject_semester),
                                now;
                        _csv_handler.pushElementBack(i,"Subject Created");
                    }
                }else{
                    _csv_handler.pushElementBack(i,"Subject by this name already exists");
                    _csv_handler.pushElementBack(i,(subject_name + " " + subject_passed_string.value() + " " + std::to_string(subject_points_ects.value()) + " " + subject_semester.value() ));
                }

            }else{
                _csv_handler.pushElementBack(i,"Error in elements");
            }
        }
        session.close();
        return true;
    }else{
        error("Database Unreachable");
        return false;
    }
}

bool SubjectCsv2Db::updateSubject(Utils::CsvHandler &_csv_handler){
    Session session(Poco::Data::MySQL::Connector::KEY,my_sql_handler->getConnectorString());

    if(session.isConnected() == true){

        for(int i = 1; i < _csv_handler.size(); i++){
            if(_csv_handler.get(i).size() == 5){
                Poco::Nullable<int> subject_id;
                Poco::Nullable<int> subject_ects_points;
                std::string subject_name = _csv_handler.get(i,1);
                std::string subject_pass = _csv_handler.get(i,2);
                std::string subject_semester = _csv_handler.get(i,4);

                try{
                    subject_ects_points = std::stoi(_csv_handler.get(i,3));
                    subject_id = std::stoi(_csv_handler.get(i,0));
                }catch(const std::invalid_argument& ia){
                    _csv_handler.pushElementBack(i,"fields ects points and subject id have be numbers, error updateing this subject");
                }

                if(subject_ects_points.isNull() != true && subject_id.isNull() != true){

                    Poco::Nullable<std::string> subject_name_tmp;

                    session << "SELECT \
                               subject.name  \
                               FROM  \
                               subject  \
                               WHERE  \
                               subject.subject_id = ?; ",
                    into(subject_name_tmp), use(subject_id.value()), now;

                    if(subject_name_tmp.isNull()){
                        _csv_handler.pushElementBack(i,"Subject by this ID does not exists");
                    }else{
                        session << "UPDATE subject \
                                   SET  \
                                   subject.name = ?, \
                                subject.pass_condition = ?, \
                                subject.points_ects = ?, \
                                subject.semester = ? \
                                    WHERE \
                                    subject.subject_id = ?;",
                        use(subject_name),
                                use(subject_pass),
                                use(subject_ects_points),
                                use(subject_semester),
                                use(subject_id),
                                now;
                        _csv_handler.pushElementBack(i,"Subject updated");
                    }
                }
            }else{
                _csv_handler.pushElementBack(i,"Error in elements");
            }
        }
        session.close();
        return true;
    }else{
        error("Database Unreachable");
        return false;
    }
}

bool SubjectCsv2Db::createSubjectParts(Utils::CsvHandler &_csv_hadler){
    Session session(Poco::Data::MySQL::Connector::KEY,my_sql_handler->getConnectorString());

    if(session.isConnected() == true){


        return false;
    }else{
        error("Database Unreachable");
        return false;
    }
}

bool SubjectCsv2Db::updateSubjectStaff(Utils::CsvHandler &_csv_handler){
    Session session(Poco::Data::MySQL::Connector::KEY,my_sql_handler->getConnectorString());

    if(session.isConnected() == true){

        // for each item (line) in csv file
        // skipping first because first line is info line ( subject name, user id, type of association)
        for(int i = 1; i < _csv_handler.size(); i++){
            if(_csv_handler.get(i).size() == 3){
                Poco::Nullable<int> subject_id;
                std::string user_id;
                std::string association;

                try{
                    subject_id = std::stoi(_csv_handler.get(i,0));
                }catch(const std::invalid_argument& ia){
                    _csv_handler.pushElementBack(i,"field subject id have be numbers, error updateing this subject");
                }

                user_id = _csv_handler.get(i,1);
                association = _csv_handler.get(i,2);

                //check if there is subject by given id and user by this id
                Poco::Nullable<std::string> subject_name_tmp;
                Poco::Nullable<std::string> user_type;

                session << " SELECT \
                           subject.name \
                           FROM \
                           subject \
                           WHERE \
                           subject.subject_id = ?;",
                into(subject_name_tmp), use(subject_id.value()),now;

                session << " SELECT \
                           user.type \
                           FROM \
                           user \
                           WHERE \
                           user.user_id = ?;",
                into(user_type), use(user_id),now;

                user_subject_association_map::iterator it;
                it = user_subject_association.find(association);

                if(subject_name_tmp.isNull()){
                    _csv_handler.pushElementBack(i,"Subject by this ID does not exists");
                }else if(user_type.isNull()){
                    _csv_handler.pushElementBack(i,"User by this ID does not exists");
                }else if(Authorization::getLevel(user_type,false) != Authorization::Level::PROFESSOR){
                    _csv_handler.pushElementBack(i,"User by this ID is not professor");
                }else if(it == user_subject_association.end()){
                    _csv_handler.pushElementBack(i,"Association must be professor, assistant or none");
                }else{

                    Poco::Nullable<std::string> association_tmp;

                    session << "SELECT \
                               professor_subject.type \
                               FROM \
                               professor_subject \
                               WHERE \
                               professor_subject.professor_id = ? \
                                AND professor_subject.subject_id = ?;",
                    into(association_tmp),
                            use(user_id),
                            use(subject_id.value()),
                            now;

                    // if association_tmp is Null then there is no connection between subject and user
                    if(association_tmp.isNull()){

                        // if there is no row with given user and subject check what is new association
                        if(user_subject_association[association] == Association::NONE){
                            // if it is none, there is no work to do :D
                            _csv_handler.pushElementBack(i,"ok");
                        }else{
                            // if it is not none create new row in db

                            session << "insert into professor_subject(professor_id, subject_id, type) \
                                       values(?,?,?);",
                            use(user_id),
                                    use(subject_id),
                                    use(association),
                                    now;
                            _csv_handler.pushElementBack(i,"created");
                            info("assigned user with id: " + user_id + " to subject with id: " + std::to_string(subject_id.value()) + " to be " + association);
                        }

                    }else{
                        if(user_subject_association[association] == user_subject_association[association_tmp.value()]){
                            // if associations are indentical -> no work, done
                            _csv_handler.pushElementBack(i,"ok");

                        }else if(user_subject_association[association] == Association::NONE){

                            session << "DELETE FROM professor_subject \
                                       WHERE \
                                       professor_subject.professor_id = ? \
                                        AND professor_subject.subject_id = ?; ",
                            use(user_id),
                                    use(subject_id.value()),
                                    now;
                            _csv_handler.pushElementBack(i,"deleted");
                            info("removed user with id: " + user_id + " from subject with id: " + std::to_string(subject_id.value()));

                        }else{ // update association type

                            session << "UPDATE professor_subject \
                                       SET \
                                       professor_subject.type = ? \
                                        WHERE \
                                        professor_subject.professor_id = ? \
                                        AND professor_subject.subject_id = ?;",
                            use(association),
                                    use(user_id),
                                    use(subject_id.value()),
                                    now;
                            _csv_handler.pushElementBack(i,"updated");
                            info("updated user with id: " + user_id + " on subject with id: " + std::to_string(subject_id.value()) +
                                 " to be " + association + " (was " + association_tmp.value() + ")");
                        }
                    }
                }
            }else{
                _csv_handler.pushElementBack(i,"Error in elements");
            }
        }
        session.close();
        return true;
    }else{
        error("Database Unreachable");
        return false;
    }
}

bool SubjectCsv2Db::createSubjectPartsAndAddStudents(Utils::CsvHandler &_csv_handler){
    Session session(Poco::Data::MySQL::Connector::KEY,my_sql_handler->getConnectorString());

    if(session.isConnected() == true){

        std::string subject_name = _csv_handler.get(0,0);
        Poco::Nullable<int> subject_id;

        session << "SELECT \
                   subject.subject_id  \
                   FROM \
                   subject \
                   WHERE \
                   subject.name = ?;",
        into(subject_id),
                use(subject_name),
                now;

        if(subject_id.isNull() == false){

            // 1. add parts to subject
            for(int i = 3; i < _csv_handler.rowSize(0)-i; i++){

                std::string part = _csv_handler.get(0,i);

                // get from part (ex. teorija:  deo_jedan)  type of part (teorija) and name of it (deo_jedan)
                StringTokenizer tokenizer(part,":", StringTokenizer::TOK_TRIM | StringTokenizer::TOK_IGNORE_EMPTY);
                StringTokenizer::Iterator s_interator = tokenizer.begin();
                std::string part_type = *s_interator;
                s_interator++;
                std::string part_name = *s_interator;
                s_interator++;
                Poco::Nullable<int> part_max_points;
                try{
                    part_max_points = std::stoi(*s_interator);
                }catch(const std::invalid_argument& ia){
                    _csv_handler.pushElementBack(0,"fields ects points and subject id have be numbers, error updateing this subject");
                }

                if(part_max_points.isNull() == false){

                    //check if that part already exists
                    Poco::Nullable<int> subject_part_id;
                    session << "SELECT \
                               subjectpart.subjectpart_id \
                               FROM \
                               subjectpart \
                               WHERE \
                               subject_id = ? AND name = ? and type = ?;",
                    into(subject_part_id),
                            use(subject_id),
                            use(part_name),
                            use(part_type),
                            now;

                    // if there is no part like it create new
                    if(subject_part_id.isNull() == true){
                        session << "INSERT INTO  subjectpart(subject_id,name,type,points_max) \
                                   VALUES(?,?,?,?);",
                        use(subject_id),
                                use(part_name),
                                use(part_type),
                                use(part_max_points),
                                now;
                        info("Created part of subject: " + subject_name + " with name: " + part_name + ", type: " +
                             part_type + ", max_points: " + std::to_string(part_max_points.value()));
                        _csv_handler.pushElementBack(0,"part: " + part + " created");
                    }else{
                        _csv_handler.pushElementBack(0,"subject part already exists");
                    }
                }
            } //end of for trough parts

            //2. add students to subject
            for(int i = 1; i < _csv_handler.size(); i++){
                std::string user_id = _csv_handler.get(i,0);
                Poco::Nullable<std::string> user_type;

                session << "SELECT \
                           user.type \
                           FROM \
                           user \
                           WHERE \
                           user.user_id = ?",
                                           into(user_type),
                        use(user_id), now;

                if(user_type.isNull()){
                    _csv_handler.pushElementBack(i,"User by this ID does not exists");
                }else if(Authorization::getLevel(user_type.value(),false) != Authorization::Level::STUDENT){
                    _csv_handler.pushElementBack(i,"User by this ID is not student");
                }else{

                    Poco::Nullable<std::string> subject_name_tmp;
                    session << "SELECT \
                               subject.name \
                               FROM \
                               subject \
                               JOIN \
                               student_subject ON student_subject.subject_id = subject.subject_id \
                            WHERE \
                            subject.subject_id = ? \
                                AND student_subject.user_id = ?;",
                    into(subject_name_tmp),
                            use(subject_id),
                            use(user_id),
                            now;

                    if(subject_name_tmp.isNull()){
                        session << "INSERT INTO student_subject(user_id, subject_id) \
                                   VALUES(?,?);",
                        use(user_id),
                                use(subject_id),
                                now;
                        _csv_handler.pushElementBack(i,"Added student to subject");
                        info("Added student by id: " + user_id + " to subject: " + subject_name);
                    }else{
                        _csv_handler.pushElementBack(i,"Student already on subject");
                    }
                }
            }

        }else{
            _csv_handler.pushElementBack(0,"Subject by this name does not exists");
        }
        session.close();
        return true;
    }else{
        error("Database Unreachable");
        return false;
    }
}

bool SubjectCsv2Db::updateStudentsGradesAndScores(Utils::CsvHandler &_csv_handler){
    Session session(Poco::Data::MySQL::Connector::KEY,my_sql_handler->getConnectorString());
    if(session.isConnected() == true){

        //check if subject exists
        std::string subject_name = _csv_handler.get(0,0);
        Poco::Nullable<int> subject_id;

        session << "SELECT \
                   subject.subject_id  \
                   FROM \
                   subject \
                   WHERE \
                   subject.name = ?;",
        into(subject_id),
                use(subject_name),
                now;

        if(subject_id.isNull() == false){

            // for each part
            for(int i = 3; i < _csv_handler.rowSize(0)-3; i++){

                //check if part exists
                std::string part = _csv_handler.get(0,i);

                // get from part (ex. teorija:  deo_jedan)  type of part (teorija) and name of it (deo_jedan)
                StringTokenizer tokenizer(part,":", StringTokenizer::TOK_TRIM | StringTokenizer::TOK_IGNORE_EMPTY);
                StringTokenizer::Iterator s_interator = tokenizer.begin();
                std::string part_type = *s_interator;
                s_interator++;
                std::string part_name = *s_interator;

                //check if that part exists
                Poco::Nullable<int> subject_part_id;
                session << "SELECT \
                           subjectpart.subjectpart_id \
                           FROM \
                           subjectpart \
                           WHERE \
                           subject_id = ? AND name = ? and type = ?;",
                into(subject_part_id),
                        use(subject_id),
                        use(part_name),
                        use(part_type),
                        now;

                if(subject_part_id.isNull() == false){

                    //for each student
                    for(int j = 1; j < _csv_handler.size(); j++){

                        //check if there is enetery for that part
                        if(_csv_handler.rowSize(j) >= i &&
                                _csv_handler.get(j,i).compare(_csv_handler.getEmptyElement()) !=0 ){

                            //check if user is student on subject
                            Poco::Nullable<int> grade;
                            std::string user_id = _csv_handler.get(j,0);

                            session << "SELECT \
                                       student_subject.grade \
                                       FROM \
                                       student_subject \
                                       WHERE \
                                       student_subject.user_id = ? \
                                        AND student_subject.subject_id = ?; ",
                            into(grade),
                                    use(user_id),
                                    use(subject_id),
                                    now;

                            // grade is NotNull filed with default value 5
                            if(grade.isNull() == false){

                                //check if student already has some exams on this part
                                Poco::Nullable<Poco::Data::Date> date;
                                session << "SELECT \
                                           exam.date \
                                           FROM \
                                           exam \
                                           WHERE \
                                           exam.user_id = ? \
                                            AND exam.subjectpart_id = ?;",
                                into(date),
                                        use(user_id),
                                        use(subject_part_id.value()),
                                        now;

                                Poco::Nullable<int> points;
                                try{
                                    points = std::stoi(_csv_handler.get(j,i));
                                }catch(const std::invalid_argument& ia){
                                    _csv_handler.pushElementBack(j,"field point has be number, error");
                                }


                                // if there is no exam with this user_id and subject part id create it
                                Poco::Data::Date today;
                                std::string today_str = Authorization::date2string(today);
                                if(date.isNull() == false && points.isNull() == false){

                                    session << "UPDATE exam \
                                               SET \
                                               exam.date = ?, \
                                            exam.points = ? \
                                                WHERE \
                                                exam.subjectpart_id = ? \
                                                AND exam.user_id = ?; ",
                                    use(today_str),
                                            use(points.value()),
                                            use(subject_part_id.value()),
                                            use(user_id),
                                            now;

                                    // if there is exam update it
                                }else if(date.isNull() == true && points.isNull() == false){
                                    session << "INSERT INTO exam (user_id, subjectpart_id, date, points) \
                                               VALUES (?,?,?,?);",
                                    use(user_id),
                                            use(subject_part_id.value()),
                                            use(today_str),
                                            use(points.value()),
                                            now;
                                }

                            }else{
                                _csv_handler.pushElementBack(j,"This user is not assigned to this subject");
                            }

                        }else{
                            // ako nema ocene tamo --> blah nista nastavi dalje
                        }
                    } // end for each student
                }
            }

            //for sum points, grade and passed field
            for(int i = _csv_handler.rowSize(0)-3; i < _csv_handler.rowSize(0); i++){

                //for each student
                for(int j = 1; j < _csv_handler.size(); j++){

                    //check if user is student on subject
                    Poco::Nullable<int> grade;
                    std::string user_id = _csv_handler.get(j,0);

                    session << "SELECT \
                               student_subject.grade \
                               FROM \
                               student_subject \
                               WHERE \
                               student_subject.user_id = ? \
                                AND student_subject.subject_id = ?; ",
                    into(grade),
                            use(user_id),
                            use(subject_id),
                            now;

                    // grade is NotNull filed with default value 5
                    if(grade.isNull() == false){

                        // if there is value update it
                        if(_csv_handler.rowSize(j) >= i &&
                                _csv_handler.get(j,i).compare(_csv_handler.getEmptyElement()) !=0 ){

                            std::string user_id = _csv_handler.get(j,0);
                            Poco::Nullable<int> number;
                            try{
                                number = std::stoi(_csv_handler.get(j,i));
                            }catch(const std::invalid_argument& ia){
                                _csv_handler.pushElementBack(j,"fields sum points, grade and passed have be numbers, error");
                            }

                            if(number.isNull() == false){

                                if(i == _csv_handler.rowSize(0)-3){
                                    session << "UPDATE student_subject \
                                               SET \
                                               student_subject.points = ? \
                                                WHERE \
                                                student_subject.subject_id = ? \
                                                AND student_subject.user_id = ?;",
                                    use(number.value()),
                                            use(subject_id),
                                            use(user_id),
                                            now;

                                }else if(i == _csv_handler.rowSize(0)-2){
                                    session << "UPDATE student_subject \
                                               SET \
                                               student_subject.grade = ? \
                                                WHERE \
                                                student_subject.subject_id = ? \
                                                AND student_subject.user_id = ?;",
                                    use(number.value()),
                                            use(subject_id),
                                            use(user_id),
                                            now;

                                }else if(i == _csv_handler.rowSize(0)-1){
                                    session << "UPDATE student_subject \
                                               SET \
                                               student_subject.passed = ? \
                                                WHERE \
                                                student_subject.subject_id = ? \
                                                AND student_subject.user_id = ?;",
                                    use(number.value()),
                                            use(subject_id),
                                            use(user_id),
                                            now;
                                }
                            }
                        }
                    }
                }
            }
        }else{
            _csv_handler.pushElementBack(0,"Subject by this name does not exists");
        }
        session.close();
        return true;
    }else{
        error("Database Unreachable");
        return false;
    }
}

}
