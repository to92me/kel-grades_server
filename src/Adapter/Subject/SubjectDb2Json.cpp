#include "SubjectDb2Json.h"

namespace Adapter{

Poco::JSON::Array::Ptr SubjectDb2Json::getSubjectsByUser(Authorization::Level &_level, std::string _user_id){
    Session session(Poco::Data::MySQL::Connector::KEY,my_sql_handler->getConnectorString());
    Poco::JSON::Array::Ptr poco_array_ptr = new Poco::JSON::Array();

    if(session.isConnected() == true){

        std::vector<Subject> subjects;

        if(_level == Authorization::Level::PROFESSOR ||
                _level == Authorization::Level::PROFESSOR_ADMIN){

            session << "SELECT \
                       subject.subject_id, \
                    subject.name, \
                    subject.pass_condition, \
                    subject.semester, \
                    subject.points_ects \
                    FROM subject \
                    JOIN professor_subject ON subject.subject_id = professor_subject.subject_id \
                    WHERE \
                    professor_subject.professor_id = ? ;",
            into(subjects),
                    use(_user_id), now;

            session.close();
        }else if(_level == Authorization::Level::STUDENT){

            session << "SELECT \
                       subject.subject_id, \
                    subject.name, \
                    subject.pass_condition, \
                    subject.semester, \
                    subject.points_ects \
                    FROM subject \
                    JOIN student_subject ON subject.subject_id = student_subject.subject_id \
                    WHERE \
                    student_subject.user_id = ? ;",
            into(subjects),
                    use(_user_id), now;

            session.close();

        }else{
            return poco_array_ptr;
        }

        for(std::vector<Subject>::iterator it = subjects.begin(); it != subjects.end(); ++it){
            Poco::JSON::Object::Ptr subject = new Poco::JSON::Object();

            subject->set("id",it->get<0>());
            subject->set("name",it->get<1>());
            subject->set("semester",it->get<2>());
            subject->set("pass_condition",it->get<3>());
            subject->set("ects_points",it->get<4>());

            poco_array_ptr->add(subject);
        }
    }else{
        error("database unreachable");
    }
    return poco_array_ptr;
}

Poco::JSON::Array::Ptr SubjectDb2Json::getSubjectById(int _subject_id){
    Session session(Poco::Data::MySQL::Connector::KEY,my_sql_handler->getConnectorString());
    Poco::JSON::Array::Ptr poco_array_ptr = new Poco::JSON::Array();

    if(session.isConnected() == true){

        std::vector<Subject> subjects;

        session << "SELECT \
                   subject.subject_id, \
                subject.name, \
                subject.pass_condition, \
                subject.semester, \
                subject.points_ects \
                FROM subject \
                WHERE \
                subject.subject_id = ? ;",
        into(subjects),
                use(_subject_id), now;

        session.close();

        for(std::vector<Subject>::iterator it = subjects.begin(); it != subjects.end(); ++it){
            Poco::JSON::Object::Ptr subject = new Poco::JSON::Object();

            subject->set("id",it->get<0>());
            subject->set("name",it->get<1>());
            subject->set("semester",it->get<2>());
            subject->set("pass_condition",it->get<3>());
            subject->set("ects_points",it->get<4>());

            poco_array_ptr->add(subject);
        }
    }else{
        error("database unreachable");
    }
    return poco_array_ptr;
}

Poco::JSON::Array::Ptr SubjectDb2Json::getAllSubjects(){
    Session session(Poco::Data::MySQL::Connector::KEY,my_sql_handler->getConnectorString());
    Poco::JSON::Array::Ptr poco_array_ptr = new Poco::JSON::Array();

    if(session.isConnected() == true){

        std::vector<Subject> subjects;
        session << "SELECT \
                   subject.subject_id, \
                subject.name, \
                subject.pass_condition, \
                subject.semester, \
                subject.points_ects \
                FROM subject;",
        into(subjects),
                now;

        session.close();

        for(std::vector<Subject>::iterator it = subjects.begin(); it != subjects.end(); ++it){
            Poco::JSON::Object::Ptr subject = new Poco::JSON::Object();

            subject->set("id",it->get<0>());
            subject->set("name",it->get<1>());
            subject->set("semester",it->get<2>());
            subject->set("pass_condition",it->get<3>());
            subject->set("ects_points",it->get<4>());

            poco_array_ptr->add(subject);
        }
    }else{
        error("database unreachable");
    }
    return poco_array_ptr;
}

Poco::JSON::Array::Ptr SubjectDb2Json::getSubjectsWithPartsAndExams(std::string _student_id){
    typedef Poco::Tuple<std::string, std::string, std::string, std::string, Poco::Nullable<std::string>, int, int, int> StudentSubjectGrade;
    Session session(Poco::Data::MySQL::Connector::KEY,my_sql_handler->getConnectorString());
    Poco::JSON::Array::Ptr poco_array_ptr = new Poco::JSON::Array();

    if(session.isConnected() == true){
        std::vector<StudentSubjectGrade> student_subject_grade;
        PartDb2Json part_db_2_json;
        std::vector<Subject> subject;
        std::vector<Professor> professors;
        Poco::JSON::Array::Ptr professors_json = new Poco::JSON::Array();
        Poco::JSON::Array::Ptr student_json = new Poco::JSON::Array();

        session << "SELECT \
                   subject.subject_id, \
                subject.name, \
                subject.pass_condition, \
                subject.semester, \
                subject.points_ects \
                FROM \
                subject \
                join student_subject on student_subject.subject_id = subject.subject_id \
                WHERE \
                student_subject.user_id = ?;",
        into (subject), use(_student_id), now;

        for(std::vector<Subject>::iterator it = subject.begin(); it != subject.end(); ++it){

            Poco::JSON::Object::Ptr subject_json = new Poco::JSON::Object();

            int subject_id = it->get<0>();

            subject_json->set("subject id",it->get<0>());
            subject_json->set("subject name",it->get<1>());
            subject_json->set("pass condition",it->get<2>());
            subject_json->set("semester",it->get<3>());
            subject_json->set("ects points",it->get<4>());



            session << "SELECT \
                       user.user_id, \
                    user.name_first, \
                    user.name_second, \
                    user.email, \
                    user.telephone, \
                    professor_subject.type \
                    FROM \
                    user \
                    JOIN \
                    professor_subject ON user.user_id = professor_subject.professor_id \
                    WHERE \
                    professor_subject.subject_id = ?;",
            into(professors), use(it->get<0>()), now;

            for(std::vector<Professor>::iterator it = professors.begin(); it != professors.end(); ++it){
                Poco::JSON::Object::Ptr professor = new Poco::JSON::Object();

                professor->set("id",it->get<0>());
                professor->set("first name",it->get<1>());
                professor->set("second name",it->get<2>());
                professor->set("email",it->get<3>());
                if((it->get<4>()).isNull()){
                    professor->set("telephone","");
                }else{
                    professor->set("telephone",(it->get<4>()).value());
                }
                professor->set("type",it->get<5>());

                professors_json->add(professor);
            }

            subject_json->set("professor",professors_json);

            session << "SELECT \
                       user.user_id, \
                    user.name_first, \
                    user.name_second, \
                    user.email, \
                    user.telephone, \
                    student_subject.points, \
                    student_subject.grade, \
                    student_subject.passed \
                    FROM \
                    user \
                    JOIN \
                    student_subject ON student_subject.user_id = user.user_id \
                    WHERE \
                    student_subject.subject_id = ? and student_subject.user_id = ?; ",
            into(student_subject_grade),
                    use(subject_id),
                    use(_student_id),
                    now;

            for(std::vector<StudentSubjectGrade>::iterator it = student_subject_grade.begin(); it != student_subject_grade.end(); ++it){
                Poco::JSON::Object::Ptr student = new Poco::JSON::Object();

                //            subject->set("user_id",it->get<0>());
//                student->set("first name",it->get<1>());
//                student->set("second name",it->get<2>());
//                student->set("email",it->get<3>());
//                if((it->get<4>()).isNull()){
//                    student->set("telephone","");
//                }else{
//                    student->set("telephone",(it->get<4>()).value());
//                }
                student->set("points",it->get<5>());
                student->set("grade",it->get<6>());
                student->set("passed",it->get<7>());

                Poco::JSON::Array::Ptr part_ptr = part_db_2_json.getPartsWithExams(subject_id, it->get<0>());

                student->set("part",part_ptr);
                student_json->add(student);
            }
            subject_json->set("student",student_json);
            poco_array_ptr->add(subject_json);
        }

    }else{
        error("database unreachable");
    }

    session.close();
    return poco_array_ptr;
}

Poco::JSON::Array::Ptr SubjectDb2Json::getSubjectWithPartsAndExams(int _subject_id){
    typedef Poco::Tuple<std::string, std::string, std::string, std::string, Poco::Nullable<std::string>, int, int, int> StudentSubjectGrade;
    Session session(Poco::Data::MySQL::Connector::KEY,my_sql_handler->getConnectorString());
    Poco::JSON::Array::Ptr poco_array_ptr = new Poco::JSON::Array();

    if(session.isConnected() == true){
        std::vector<StudentSubjectGrade> student_subject_grade;
        PartDb2Json part_db_2_json;
        Subject subject;
        Poco::JSON::Object::Ptr subject_json = new Poco::JSON::Object();
        std::vector<Professor> professors;
        Poco::JSON::Array::Ptr professors_json = new Poco::JSON::Array();
        Poco::JSON::Array::Ptr student_json = new Poco::JSON::Array();

        session << "SELECT \
                   subject.subject_id, \
                subject.name, \
                subject.pass_condition, \
                subject.semester, \
                subject.points_ects \
                FROM \
                subject \
                where  \
                subject.subject_id = ?; ",
        into (subject), use(_subject_id), now;

        subject_json->set("subject id",subject.get<0>());
        subject_json->set("subject name",subject.get<1>());
        subject_json->set("pass condition",subject.get<2>());
        subject_json->set("semester",subject.get<3>());
        subject_json->set("ects points",subject.get<4>());


        session << "SELECT \
                   user.user_id, \
                user.name_first, \
                user.name_second, \
                user.email, \
                user.telephone, \
                professor_subject.type \
                FROM \
                user \
                JOIN \
                professor_subject ON user.user_id = professor_subject.professor_id \
                WHERE \
                professor_subject.subject_id = ?;",
        into(professors), use(_subject_id), now;

        for(std::vector<Professor>::iterator it = professors.begin(); it != professors.end(); ++it){
            Poco::JSON::Object::Ptr professor = new Poco::JSON::Object();

            professor->set("id",it->get<0>());
            professor->set("first name",it->get<1>());
            professor->set("second name",it->get<2>());
            professor->set("email",it->get<3>());
            if((it->get<4>()).isNull()){
                professor->set("telephone","");
            }else{
                professor->set("telephone",(it->get<4>()).value());
            }
            professor->set("type",it->get<5>());

            professors_json->add(professor);
        }

        subject_json->set("professor",professors_json);

        session << "SELECT \
                   user.user_id, \
                user.name_first, \
                user.name_second, \
                user.email, \
                user.telephone, \
                student_subject.points, \
                student_subject.grade, \
                student_subject.passed \
                FROM \
                user \
                JOIN \
                student_subject ON student_subject.user_id = user.user_id \
                WHERE \
                student_subject.subject_id = ?; ",
        into(student_subject_grade),
                use(_subject_id), now;

        // user.type,

        session.close();

        for(std::vector<StudentSubjectGrade>::iterator it = student_subject_grade.begin(); it != student_subject_grade.end(); ++it){
            Poco::JSON::Object::Ptr student = new Poco::JSON::Object();

            //            subject->set("user_id",it->get<0>());
            student->set("first name",it->get<1>());
            student->set("second name",it->get<2>());
            student->set("email",it->get<3>());
            if((it->get<4>()).isNull()){
                student->set("telephone","");
            }else{
                student->set("telephone",(it->get<4>()).value());
            }
            student->set("points",it->get<5>());
            student->set("grade",it->get<6>());
            student->set("passed",it->get<7>());

            Poco::JSON::Array::Ptr part_ptr = part_db_2_json.getPartsWithExams(_subject_id, it->get<0>());

            student->set("part",part_ptr);
            student_json->add(student);
        }
        subject_json->set("student",student_json);
        poco_array_ptr->add(subject_json);
    }else{
        error("database unreachable");
    }
    return poco_array_ptr;
}

Poco::JSON::Array::Ptr SubjectDb2Json::getSubjectByName(std::string _subject_name){
    Session session(Poco::Data::MySQL::Connector::KEY,my_sql_handler->getConnectorString());
    Poco::JSON::Array::Ptr poco_array_ptr = new Poco::JSON::Array();

    if(session.isConnected() == true){

        std::vector<Subject> subjects;

        session << "SELECT \
                   subject.subject_id, \
                subject.name, \
                subject.pass_condition, \
                subject.semester, \
                subject.points_ects \
                FROM subject \
                WHERE \
                subject.name = ? ;",
        into(subjects),
                use(_subject_name), now;

        session.close();

        for(std::vector<Subject>::iterator it = subjects.begin(); it != subjects.end(); ++it){
            Poco::JSON::Object::Ptr subject = new Poco::JSON::Object();

            subject->set("id",it->get<0>());
            subject->set("name",it->get<1>());
            subject->set("semester",it->get<2>());
            subject->set("pass_condition",it->get<3>());
            subject->set("ects_points",it->get<4>());

            poco_array_ptr->add(subject);
        }
    }else{
        error("database unreachable");
    }
    return poco_array_ptr;
}

Poco::JSON::Array::Ptr SubjectDb2Json::getSubjectWithPartsAndExams(std::string _subject_name){
    typedef Poco::Tuple<std::string, std::string, std::string, std::string, Poco::Nullable<std::string>, int, int, int, int> StudentSubjectGrade;
    Session session(Poco::Data::MySQL::Connector::KEY,my_sql_handler->getConnectorString());
    Poco::JSON::Array::Ptr poco_array_ptr = new Poco::JSON::Array();

    if(session.isConnected() == true){
        std::vector<StudentSubjectGrade> student_subject_grade;
        PartDb2Json part_db_2_json;
        Subject subject;
        Poco::JSON::Object::Ptr subject_json = new Poco::JSON::Object();
        std::vector<Professor> professors;
        Poco::JSON::Array::Ptr professors_json = new Poco::JSON::Array();
        Poco::JSON::Array::Ptr student_json = new Poco::JSON::Array();

        session << "SELECT \
                   subject.subject_id, \
                subject.name, \
                subject.pass_condition, \
                subject.semester, \
                subject.points_ects \
                FROM \
                subject \
                where  \
                subject.name = ?; ",
        into (subject), use(_subject_name), now;

        subject_json->set("subject id",subject.get<0>());
        subject_json->set("subject name",subject.get<1>());
        subject_json->set("pass condition",subject.get<2>());
        subject_json->set("semester",subject.get<3>());
        subject_json->set("ects points",subject.get<4>());


        session << "SELECT \
                   user.user_id, \
                user.name_first, \
                user.name_second, \
                user.email, \
                user.telephone, \
                professor_subject.type \
                FROM \
                user \
                JOIN \
                professor_subject ON user.user_id = professor_subject.professor_id \
                JOIN \
                subject ON subject.subject_id = professor_subject.subject_id \
                WHERE \
                subject.name = ?; ",
        into(professors), use(_subject_name), now;

        for(std::vector<Professor>::iterator it = professors.begin(); it != professors.end(); ++it){
            Poco::JSON::Object::Ptr professor = new Poco::JSON::Object();

            professor->set("id",it->get<0>());
            professor->set("first name",it->get<1>());
            professor->set("second name",it->get<2>());
            professor->set("email",it->get<3>());
            if((it->get<4>()).isNull()){
                professor->set("telephone","");
            }else{
                professor->set("telephone",(it->get<4>()).value());
            }
            professor->set("type",it->get<5>());

            professors_json->add(professor);
        }

        subject_json->set("professor",professors_json);

        session << "SELECT \
                   user.user_id, \
                user.name_first, \
                user.name_second, \
                user.email, \
                user.telephone, \
                student_subject.points, \
                student_subject.grade, \
                student_subject.passed, \
                subject.subject_id \
                FROM \
                user \
                JOIN \
                student_subject ON student_subject.user_id = user.user_id \
                JOIN \
                subject ON subject.subject_id = student_subject.subject_id \
                WHERE \
                subject.name = ?;",
        into(student_subject_grade),
                use(_subject_name), now;

        // user.type,

        session.close();

        for(std::vector<StudentSubjectGrade>::iterator it = student_subject_grade.begin(); it != student_subject_grade.end(); ++it){
            Poco::JSON::Object::Ptr student = new Poco::JSON::Object();

            //            subject->set("user_id",it->get<0>());
            student->set("first name",it->get<1>());
            student->set("second name",it->get<2>());
            student->set("email",it->get<3>());
            if((it->get<4>()).isNull()){
                student->set("telephone","");
            }else{
                student->set("telephone",(it->get<4>()).value());
            }
            student->set("points",it->get<5>());
            student->set("grade",it->get<6>());
            student->set("passed",it->get<7>());

            Poco::JSON::Array::Ptr part_ptr = part_db_2_json.getPartsWithExams(it->get<8>(), it->get<0>());

            student->set("part",part_ptr);
            student_json->add(student);
        }
        subject_json->set("student",student_json);
        poco_array_ptr->add(subject_json);
    }else{
        error("database unreachable");
    }
    return poco_array_ptr;
}

Poco::JSON::Array::Ptr SubjectDb2Json::getSubjectWithPartsAndExams(int _subject_id, std::string _student_id){
    typedef Poco::Tuple<std::string, std::string, std::string, std::string, Poco::Nullable<std::string>, int, int, int> StudentSubjectGrade;
    Session session(Poco::Data::MySQL::Connector::KEY,my_sql_handler->getConnectorString());
    Poco::JSON::Array::Ptr poco_array_ptr = new Poco::JSON::Array();

    if(session.isConnected() == true){
        std::vector<StudentSubjectGrade> student_subject_grade;
        PartDb2Json part_db_2_json;
        Subject subject;
        Poco::JSON::Object::Ptr subject_json = new Poco::JSON::Object();
        std::vector<Professor> professors;
        Poco::JSON::Array::Ptr professors_json = new Poco::JSON::Array();
        Poco::JSON::Array::Ptr student_json = new Poco::JSON::Array();

        session << "SELECT \
                   subject.subject_id, \
                subject.name, \
                subject.pass_condition, \
                subject.semester, \
                subject.points_ects \
                FROM \
                subject \
                where  \
                subject.subject_id = ?; ",
        into (subject), use(_subject_id), now;

        subject_json->set("subject id",subject.get<0>());
        subject_json->set("subject name",subject.get<1>());
        subject_json->set("pass condition",subject.get<2>());
        subject_json->set("semester",subject.get<3>());
        subject_json->set("ects points",subject.get<4>());


        session << "SELECT \
                   user.user_id, \
                user.name_first, \
                user.name_second, \
                user.email, \
                user.telephone, \
                professor_subject.type \
                FROM \
                user \
                JOIN \
                professor_subject ON user.user_id = professor_subject.professor_id \
                WHERE \
                professor_subject.subject_id = ?;",
        into(professors), use(_subject_id), now;

        for(std::vector<Professor>::iterator it = professors.begin(); it != professors.end(); ++it){
            Poco::JSON::Object::Ptr professor = new Poco::JSON::Object();

            professor->set("id",it->get<0>());
            professor->set("first name",it->get<1>());
            professor->set("second name",it->get<2>());
            professor->set("email",it->get<3>());
            if((it->get<4>()).isNull()){
                professor->set("telephone","");
            }else{
                professor->set("telephone",(it->get<4>()).value());
            }
            professor->set("type",it->get<5>());

            professors_json->add(professor);
        }

        subject_json->set("professor",professors_json);


        session << "SELECT \
                   user.user_id, \
                user.name_first, \
                user.name_second, \
                user.email, \
                user.telephone, \
                student_subject.points, \
                student_subject.grade, \
                student_subject.passed \
                FROM \
                user \
                JOIN \
                student_subject ON student_subject.user_id = user.user_id \
                WHERE \
                student_subject.subject_id = ? and user.user_id = ?; ",
        into(student_subject_grade),
                use(_subject_id), use(_student_id), now;

        // user.type,

        session.close();

        for(std::vector<StudentSubjectGrade>::iterator it = student_subject_grade.begin(); it != student_subject_grade.end(); ++it){
            Poco::JSON::Object::Ptr student = new Poco::JSON::Object();

            student->set("user_id",it->get<0>());
            student->set("first name",it->get<1>());
            student->set("second name",it->get<2>());
            student->set("email",it->get<3>());
            if((it->get<4>()).isNull()){
                student->set("telephone","");
            }else{
                student->set("telephone",(it->get<4>()).value());
            }
            student->set("points",it->get<5>());
            student->set("grade",it->get<6>());
            student->set("passed",it->get<7>());

            Poco::JSON::Array::Ptr part_ptr = part_db_2_json.getPartsWithExams(_subject_id, it->get<0>());

            student->set("part",part_ptr);
            student_json->add(student);
        }

        subject_json->set("student",student_json);
        poco_array_ptr->add(subject_json);
    }else{
        error("database unreachable");
    }
    return poco_array_ptr;
}


Poco::JSON::Array::Ptr SubjectDb2Json::getSubjectWithPartsAndExams(std::string _subject_name, std::string _student_id){
    typedef Poco::Tuple<std::string, std::string, std::string, std::string, Poco::Nullable<std::string>, int, int, int, int> StudentSubjectGrade;
    Session session(Poco::Data::MySQL::Connector::KEY,my_sql_handler->getConnectorString());
    Poco::JSON::Array::Ptr poco_array_ptr = new Poco::JSON::Array();

    if(session.isConnected() == true){
        std::vector<StudentSubjectGrade> student_subject_grade;
        PartDb2Json part_db_2_json;
        Subject subject;
        Poco::JSON::Object::Ptr subject_json = new Poco::JSON::Object();
        std::vector<Professor> professors;
        Poco::JSON::Array::Ptr professors_json = new Poco::JSON::Array();
        Poco::JSON::Array::Ptr student_json = new Poco::JSON::Array();

        session << "SELECT \
                   subject.subject_id, \
                subject.name, \
                subject.pass_condition, \
                subject.semester, \
                subject.points_ects \
                FROM \
                subject \
                where  \
                subject.name = ?; ",
        into (subject), use(_subject_name), now;

        subject_json->set("subject id",subject.get<0>());
        subject_json->set("subject name",subject.get<1>());
        subject_json->set("pass condition",subject.get<2>());
        subject_json->set("semester",subject.get<3>());
        subject_json->set("ects points",subject.get<4>());


        session << "SELECT \
                   user.user_id, \
                user.name_first, \
                user.name_second, \
                user.email, \
                user.telephone, \
                professor_subject.type \
                FROM \
                user \
                JOIN \
                professor_subject ON user.user_id = professor_subject.professor_id \
                JOIN \
                subject ON subject.subject_id = professor_subject.subject_id \
                WHERE \
                subject.name = ?; ",
        into(professors), use(_subject_name), now;

        for(std::vector<Professor>::iterator it = professors.begin(); it != professors.end(); ++it){
            Poco::JSON::Object::Ptr professor = new Poco::JSON::Object();

            professor->set("id",it->get<0>());
            professor->set("first name",it->get<1>());
            professor->set("second name",it->get<2>());
            professor->set("email",it->get<3>());
            if((it->get<4>()).isNull()){
                professor->set("telephone","");
            }else{
                professor->set("telephone",(it->get<4>()).value());
            }
            professor->set("type",it->get<5>());

            professors_json->add(professor);
        }

        subject_json->set("professor",professors_json);

        session << "SELECT \
                   user.user_id, \
                user.name_first, \
                user.name_second, \
                user.email, \
                user.telephone, \
                student_subject.points, \
                student_subject.grade, \
                student_subject.passed, \
                subject.subject_id \
                FROM \
                user \
                JOIN \
                student_subject ON student_subject.user_id = user.user_id \
                JOIN \
                subject ON subject.subject_id = student_subject.subject_id \
                WHERE \
                subject.name = ? and user.user_id = ?; ",
        into(student_subject_grade),
                use(_subject_name), use(_student_id), now;

        // user.type,

        session.close();

        for(std::vector<StudentSubjectGrade>::iterator it = student_subject_grade.begin(); it != student_subject_grade.end(); ++it){
            Poco::JSON::Object::Ptr student = new Poco::JSON::Object();

            student->set("user_id",it->get<0>());
            student->set("first name",it->get<1>());
            student->set("second name",it->get<2>());
            student->set("email",it->get<3>());
            if((it->get<4>()).isNull()){
                student->set("telephone","");
            }else{
                student->set("telephone",(it->get<4>()).value());
            }
            student->set("points",it->get<5>());
            student->set("grade",it->get<6>());
            student->set("passed",it->get<7>());

            Poco::JSON::Array::Ptr part_ptr = part_db_2_json.getPartsWithExams(it->get<8>(),it->get<0>());

            student->set("part",part_ptr);
            student_json->add(student);
        }

        subject_json->set("student",student_json);

        poco_array_ptr->add(subject_json);
    }else{
        error("database unreachable");
    }
    return poco_array_ptr;
}
}
