#include "ExamDb2Json.h"

namespace Adapter {

Poco::JSON::Array::Ptr ExamDb2Json::getExam(int _subjectpart_id, std::string _student_id){
    typedef Poco::Tuple<int, Poco::Data::Date> Exam;
    Session session(Poco::Data::MySQL::Connector::KEY,my_sql_handler->getConnectorString());

    if(session.isConnected()){
        Poco::JSON::Array::Ptr json_array = new Poco::JSON::Array();
        std::vector<Exam> exams;

        session << "select \
                   exam.points, \
                exam.date \
                from \
                exam \
                where \
                exam.user_id = ? and exam.subjectpart_id = ?;",
        into(exams), use(_student_id), use(_subjectpart_id), now;

        for(std::vector<Exam>::iterator it = exams.begin(); it != exams.end(); ++it){
            Poco::JSON::Object::Ptr json_exam = new Poco::JSON::Object();

            json_exam->set("points",it->get<0>());
            json_exam->set("date",date_2_string.date2string(it->get<1>()));

            json_array->add(json_exam);
        }
        return json_array;
    }else{
        return NULL;
    }
}

}
