#ifndef KELGS_ADAPTER_EXAM_EXAMDB2JSON_H
#define KELGS_ADAPTER_EXAM_EXAMDB2JSON_H


#include "Logger/Interface.h"
#include "MySqlHandler/MySqlHandler.h"
#include "Authorization/Authorization.h"

#include "Utils/UriParser.h"
#include "Utils/ErrorHandler.h"
#include "Utils/PocoDate2StdString.h"


#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/JSON/Object.h"

using Poco::Net::HTTPServerRequest;
using Poco::Net::HTTPServerResponse;
using Poco::Data::Session;

using Utils::UriParser;
using Log::LoggerInterface;
using Credentials::Authorization;

namespace Adapter{

/**
 * @brief creates Exam Json Object from Data base.
 * @ingroup Adapter
 * @author Tomislav Tubas
 *
 * If Status.user_id is not found in Database UserDb2Json will return empty Poco::JSON::array
 *
 * @version 0.1
 *
 * @note not done
 * @date 1.12.2015
 *
 * contact tumbas.tomislav@gmail.com tumbas@uns.ac.rs
 */
class ExamDb2Json : public LoggerInterface{
public:

    /**
     * @brief Constructor
     */
    ExamDb2Json(): LoggerInterface("ExamDb2Json"){
        my_sql_handler = MySqlHandler::getInstance();
    }

     /**
     * @brief  getExam gets exams with given subject_part_id and user_id (student_id)
     * @param _subjectpart_id is id(primary key) of subject_part 
     * @param _student_id is user id 
     * @return Poco::Json::Array::Ptr with data got from database
     */
    Poco::JSON::Array::Ptr getExam(int _subjectpart_id, std::string _student_id);

private:
    Utils::PocoDate2StdString date_2_string; ///< from Poco::Date to date in string format
    MySqlHandler *my_sql_handler; ///< mysql handler with configuration string of Session

};
}

#endif
