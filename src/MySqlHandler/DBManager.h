#ifndef DBMANAGER_H
#define DBMANAGER_H

#include "MySqlHandler.h"
#include "Logger/Interface.h"
#include "Poco/Data/Session.h"
#include "Poco/Data/SessionPool.h"
#include "Poco/Data/DataException.h"
#include <algorithm>

using Log::LoggerInterface;

namespace MySql {


class DbManager : public LoggerInterface {

public:
    enum ConnectionError_t{
        DBM_ERROR_CONNECTION,
        DBM_ERROR_DATABASE,
        DBM_ERROR_TABLES,
        DBM_SUCCESS
    };

    DbManager();
    ~DbManager();

    ConnectionError_t checkDataBase();

    bool runChecksAndCreation();

private:
    MySqlHandler* handler;
    Poco::Data::SessionPool* session_pool_mysql;
    std::vector<std::string> tables_expected;

    bool createSession();
};


} // end namespace MySql

#endif
