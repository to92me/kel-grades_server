set(MYSQLHANDLER_SRC 
     MySqlHandler.cpp
     DBManager.cpp
)

add_library(MySqlHandler ${MYSQLHANDLER_SRC})

target_link_libraries(MySqlHandler Authorization Logger Server Utils)


