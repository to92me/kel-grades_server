#include "DBManager.h"


namespace MySql {


DbManager::DbManager(){
    this->handler = MySqlHandler::getInstance();
    this->setModule("DbManager");
    this->tables_expected.push_back("credentials");
    this->tables_expected.push_back("exam");
    this->tables_expected.push_back("logs");
    this->tables_expected.push_back("professor_subject");
    this->tables_expected.push_back("student_subject");
    this->tables_expected.push_back("subject");
    this->tables_expected.push_back("subjectpart");
    this->tables_expected.push_back("user");
}

 DbManager::ConnectionError_t DbManager::checkDataBase(){
    try{
        Poco::Data::Session session(Poco::Data::MySQL::Connector::KEY, handler->getConnectorString());

        std::vector<std::string> tables_got;
        session << "SHOW TABLES;", into(tables_got), limit(20), now;
        //        for(std::vector<std::string>::iterator it = tables_got.begin(); it != tables_got.end(); ++it){
        //            std::cout << *it << std::endl;
        //        }

        session.close();

        if(std::is_permutation(this->tables_expected.begin(), this->tables_expected.end(), tables_got.begin())){
            return DBM_SUCCESS;
        }else{
            return DBM_ERROR_TABLES;
        }

    }catch(Poco::Data::ConnectionFailedException &ex){
        error(ex.message());
        return DBM_ERROR_CONNECTION;
    }
}

bool DbManager::runChecksAndCreation(){

//    if(this->checkConnection() == false){
//        error("Can not connect to database");
//        return false;
//    }

//    if(this->createSession() == false){
//        error("Can not connect to database");
//        return false;
//    }

//    info("Connection to databse : SUCESS! ");
    ConnectionError_t result = this->checkDataBase();
    if (result == DBM_SUCCESS){
        info("Databse OK");
        return true;
    }else if(result == DBM_ERROR_TABLES){
        error("Data base Tables are not ok exiting");
        return false;
    }else{
        return false;
    }
}

DbManager::~DbManager(){

    //    delete this->session_pool_mysql;
}


} // end namespace
