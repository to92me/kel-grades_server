#ifndef MYSQLHANDLER_H
#define MYSQLHANDLER_H

//#include "Logger/Interface.h"
#include <string>

#include "Poco/Data/MySQL/MySQLException.h"
#include "Poco/Data/MySQL/Connector.h"
#include "Poco/Data/SessionFactory.h"
#include "Poco/Data/SessionPool.h"
#include "Poco/Mutex.h"

using Poco::Data::SessionFactory;
using Poco::Mutex;
using namespace Poco::Data::Keywords;

namespace MySql{
/**
 * @brief MySQL database congiguration class. Stores data for access to database
 *
 * @ingroup MySql
 *
 * @author Tomislav Tumbas
 *
 * @version 0.1
 *
 * @todo create object pool with connections
 *
 * contact tumbas.tomislav@gmail.com tumbas@uns.ac.rs
 */
class MySqlHandler{
public:
    /**
    * @brief singleton for getting instance of mysql
    * @return instace of MySql object
    */
   static MySqlHandler* getInstance();

   /**
     * @brief destructor
     */
   ~MySqlHandler();

   /**
     * @brief sets address (hostname) of server with database
     * @param _address is address of server
     */
    void setAddress(std::string _address);

    /**
     * @brief sets user name for database login
     * @param _uname is user name
     */
    void setUserName(std::string _uname);

    /**
     * @brief sets password for database login
     * @param _passwd is password
     */
    void setPassword(std::string _passwd);

    /**
     * @brief sets port of database
     * @param _port is port number
     */
    void setPort(std::string _port);

    /**
     * @brief sets defaulth schema of database
     * @param _name is name of schema
     */
    void setDatabaseName(std::string _name);

    /**
     * @brief returns address (hostname) of server with database
     * @return address(hostname)
     */
    std::string getAddress();

    /**
     * @brief returns user name for database login
     * @return user name
     */
    std::string getUserName();

    /**
     * @brief returns password for database login
     * @return password
     */
    std::string getPassword();

    /**
     * @brief returns port of database
     * @return port number
     */
    std::string getPort();

    /**
     * @brief returns defaulth schema of database
     * @return name of schema
     */
    std::string getDabaseName();

    /**
     * @brief returns connection string for poco mysql connector (for more see Poco::Session)
     * @return
     */
    std::string getConnectorString();


private:
    MySqlHandler(){}

    static MySqlHandler* instance;

    std::string db_address;
    std::string db_uname;
    std::string db_passwd;
    std::string db_port;
    std::string db_schema;
    std::string db_connection_chain;

    Mutex mutex;
};

}// end namespace
#endif
