#include "MySqlHandler.h"

namespace MySql {

void MySqlHandler::setAddress(std::string _address){
    Mutex::ScopedLock lock(mutex);

    this->db_address = _address;
}

void MySqlHandler::setPassword(std::string _passwd){
    Mutex::ScopedLock lock(mutex);

    this->db_passwd = _passwd;
}

void MySqlHandler::setUserName(std::string _uname){
    Mutex::ScopedLock lock(mutex);

    this->db_uname = _uname;
}

void MySqlHandler::setPort(std::string _port){
    Mutex::ScopedLock lock(mutex);

    this->db_port = _port;
}

void MySqlHandler::setDatabaseName(std::string _name){
    Mutex::ScopedLock lock(mutex);

    this->db_schema = _name;
}

std::string MySqlHandler::getAddress(){
    Mutex::ScopedLock lock(mutex);

    return this->db_address;
}

std::string MySqlHandler::getPassword(){
    Mutex::ScopedLock lock(mutex);

    return this->db_passwd;
}

std::string MySqlHandler::getUserName(){
    Mutex::ScopedLock lock(mutex);

    return this->db_uname;
}

std::string MySqlHandler::getPort(){
    Mutex::ScopedLock lock(mutex);

    return this->db_port;
}

std::string MySqlHandler::getDabaseName(){
    Mutex::ScopedLock lock(mutex);

    return this->db_schema;
}

MySqlHandler* MySqlHandler::instance = NULL;

MySqlHandler* MySqlHandler::getInstance(){
    if (MySqlHandler::instance == NULL){
        MySqlHandler::instance  = new MySqlHandler();
    }
    return instance;
}

std::string MySqlHandler::getConnectorString(){
    db_connection_chain =
            "host="
            + db_address
            + ";port="
            + db_port
            + ";db="
            + db_schema
            + ";user="
            + db_uname
            + ";password="
            + db_passwd
            + ";auto-reconnect=true";

   return db_connection_chain;
}

MySqlHandler::~MySqlHandler(){
//    std::cout << "exiting handler" << std::endl;
}

}
