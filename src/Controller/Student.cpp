#include "Student.h"

namespace Controller{

void ControllerStudent::getAuthStudent(Authorization::Status &_status, Utils::UriParser &_uri_parser, Poco::Net::HTTPServerRequest &req, Poco::Net::HTTPServerResponse &resp){

    Credentials::Authorization::Level auth_level;
    UserDb2Json user_db_2_json;
    Object::Ptr json_user_ptr;
    Array::Ptr json_array_ptr;
    SubjectDb2Json subject_db_2_json;

    if(checkType(_uri_parser,resp) == false){
        return;
    }

    if(_uri_parser.getFormat() != UriParser::Format::JSON){
        err_handler.genErrMssgNotImplemented(resp,false);
        return;
    }

    // student can access only his data
    if(_uri_parser.getElement(0) == _status.user_id){
        json_array_ptr = user_db_2_json.getUser(auth_level,_uri_parser.getElement(0));

        json_user_ptr = json_array_ptr->getObject(0);

        if(_uri_parser.getElementCount() < 2){

            // get user subjects parts and exams
            json_array_ptr = subject_db_2_json.getSubjectsWithPartsAndExams(_uri_parser.getElement(0));
            // set all data in field subject

        }else{
            if(Controller::ControllerSubject::isNumber(_uri_parser.getElement(1))){
                json_array_ptr = subject_db_2_json.getSubjectWithPartsAndExams(std::stoi(_uri_parser.getElement(1)),_uri_parser.getElement(0));
            }else{
                json_array_ptr = subject_db_2_json.getSubjectWithPartsAndExams(_uri_parser.getElement(1),_uri_parser.getElement(0));
            }
        }
        json_user_ptr->set("subject",json_array_ptr);

        resp.setStatus(Poco::Net::HTTPResponse::HTTPStatus::HTTP_OK);

    }else{
        resp.setStatus(Poco::Net::HTTPResponse::HTTPStatus::HTTP_UNAUTHORIZED);
        json_user_ptr = new Object();
        json_user_ptr->set("ERROR","HTTP_UNAUTHORIZED");
    }

    resp.setContentType("Application/Json");
    std::ostream& out = resp.send();
    Stringifier::stringify(json_user_ptr,out);
    out.flush();

}

void ControllerStudent::getAuthProfAndAdmin(Authorization::Status &_status, Utils::UriParser &_uri_parser, Poco::Net::HTTPServerRequest &req, Poco::Net::HTTPServerResponse &resp){
    Credentials::Authorization::Level auth_level;
    SubjectDb2Json subject_db_2_json;
    UserDb2Json user_db_2_json;
    Object::Ptr json_user_ptr;
    Array::Ptr json_array_ptr;

    if(checkType(_uri_parser,resp) == false){
        return;
    }

    if(_uri_parser.getFormat() != UriParser::Format::JSON){
        err_handler.genErrMssgNotImplemented(resp,false);
        return;
    }

    json_array_ptr = user_db_2_json.getUser(auth_level,_uri_parser.getElement(0));

    if(json_array_ptr->size() == 0){
        // if there is no user by this ID generate ERROR JSON
        json_user_ptr = new Object();
        json_user_ptr->set("ERROR","unknown user ID");
        resp.setStatus(Poco::Net::HTTPResponse::HTTPStatus::HTTP_BAD_REQUEST);
    }else{

        json_user_ptr = json_array_ptr->getObject(0);
        // get user subjects parts and exams
        if(_uri_parser.getElementCount() < 2){

            // get user subjects parts and exams
            json_array_ptr = subject_db_2_json.getSubjectsWithPartsAndExams(_uri_parser.getElement(0));
        }else{
            if(Controller::ControllerSubject::isNumber(_uri_parser.getElement(1))){
                json_array_ptr = subject_db_2_json.getSubjectWithPartsAndExams(std::stoi(_uri_parser.getElement(1)),_uri_parser.getElement(0));
            }else{
                json_array_ptr = subject_db_2_json.getSubjectWithPartsAndExams(_uri_parser.getElement(1),_uri_parser.getElement(0));
            }
        }
        // set all data in field subject
        json_user_ptr->set("subject",json_array_ptr);

        resp.setStatus(Poco::Net::HTTPResponse::HTTPStatus::HTTP_OK);
    }

    resp.setContentType("Application/Json");
    std::ostream& out = resp.send();
    Stringifier::stringify(json_user_ptr,out);
    out.flush();

}

void ControllerStudent::putAuthProfAndAdmin(Authorization::Status &_status, Utils::UriParser &_uri_parser, Poco::Net::HTTPServerRequest &req, Poco::Net::HTTPServerResponse &resp){
    if(_uri_parser.getFormat() != UriParser::Format::CSV){
        err_handler.genErrMssgNotImplemented(resp,true,"ControllerStudent::postAuthProfAndAdmin, !CSV");
        return;
    }

    std::istream& istr = req.stream();
    Utils::CsvHandler csv_h;
    UserCsv2Db user_csv_2_db;

    csv_h.read(istr);
    Authorization::Level level_new_user = Authorization::Level::STUDENT;

    user_csv_2_db.updateUser(req,resp, csv_h,_status.level,level_new_user);

    resp.setContentType("Application/txt");
    std::ostream& out = resp.send();
    csv_h.write(out);
    out.flush();
}

void ControllerStudent::postAuthProfAndAdmin(Authorization::Status &_status, Utils::UriParser &_uri_parser, Poco::Net::HTTPServerRequest &req, Poco::Net::HTTPServerResponse &resp){
    if(_uri_parser.getFormat() != UriParser::Format::CSV){
        err_handler.genErrMssgNotImplemented(resp,true,"ControllerStudent::postAuthProfAndAdmin, !CSV");
        return;
    }

    std::istream& istr = req.stream();
    Utils::CsvHandler csv_h;
    UserCsv2Db user_csv_2_db;

    csv_h.read(istr);
    Authorization::Level level_new_user = Authorization::Level::STUDENT;

    user_csv_2_db.createUser(req,resp, csv_h,_status.level,level_new_user);

    resp.setContentType("Application/txt");
    std::ostream& out = resp.send();
    csv_h.write(out);
    out.flush();
}

void ControllerStudent::deleteAuthProfAndAdmin(Authorization::Status &_status, Utils::UriParser &_uri_parser, Poco::Net::HTTPServerRequest &req, Poco::Net::HTTPServerResponse &resp){
    if(_uri_parser.getElementCount() == 0){
        ErrorHandler err_h;
        err_h.genErrMssgBadRequest(resp);
        return;
    }

    UserDb2Json user_db_2_json;
    Poco::JSON::Array::Ptr tmp_array = user_db_2_json.getUser(_status.level,_uri_parser.getElement(0));

    if(tmp_array->size() == 0){
        ErrorHandler err_h;
        err_h.genErrMssgBadRequest(resp);
        return;
    }

    UserDelete user_del;
    if(user_del.deleteUser(_uri_parser.getElement(0))){
        info("deleted user by id: " +_uri_parser.getElement(0));
        resp.setStatus(Poco::Net::HTTPServerResponse::HTTP_OK);
        std::ostream& out = resp.send();
        out << "<h3>OK</h3>";
        out.flush();
    }else{
        ErrorHandler err_h;
        err_h.genErrMssgDbUnreachable(resp);
    };

}

void ControllerStudent::execute(Authorization::Status &_status, Utils::UriParser &_uri_parser, Poco::Net::HTTPServerRequest &req, Poco::Net::HTTPServerResponse &resp){


    switch (string_2_http.getHttpMethod(req.getMethod())) {
    case StdString2HttpMethod::HttpMethod::GET : {
        if(_status.level == Credentials::Authorization::Level::STUDENT) getAuthStudent(_status,_uri_parser,req,resp);
        else if(_status.level == Credentials::Authorization::Level::PROFESSOR ||
                _status.level == Credentials::Authorization::Level::PROFESSOR_ADMIN ||
                _status.level == Credentials::Authorization::Level::ADMIN) getAuthProfAndAdmin(_status,_uri_parser,req,resp);
        else err_handler.genErrMssgUnauthorized(resp);
    }break;

    case StdString2HttpMethod::HttpMethod::PUT : {
        if(_status.level == Credentials::Authorization::Level::PROFESSOR ||
                _status.level == Credentials::Authorization::Level::PROFESSOR_ADMIN ||
                _status.level == Credentials::Authorization::Level::ADMIN) putAuthProfAndAdmin(_status,_uri_parser,req,resp);
        else err_handler.genErrMssgUnauthorized(resp);
    }break;

    case StdString2HttpMethod::HttpMethod::POST : {
        if(_status.level == Credentials::Authorization::Level::PROFESSOR ||
                _status.level == Credentials::Authorization::Level::PROFESSOR_ADMIN ||
                _status.level == Credentials::Authorization::Level::ADMIN) postAuthProfAndAdmin(_status,_uri_parser,req,resp);
        else err_handler.genErrMssgUnauthorized(resp);

    }break;

    case StdString2HttpMethod::HttpMethod::PATCH : {
        err_handler.genErrMssgMethodNotAllowed(resp,"ControllerStudent method:PATCH");
    }break;

    case StdString2HttpMethod::HttpMethod::DELETE : {
        if(_status.level == Credentials::Authorization::Level::PROFESSOR ||
                _status.level == Credentials::Authorization::Level::PROFESSOR_ADMIN ||
                _status.level == Credentials::Authorization::Level::ADMIN) deleteAuthProfAndAdmin(_status,_uri_parser,req,resp);
        else err_handler.genErrMssgUnauthorized(resp,"ControllerStudent method:DELETE, user not ADMIN or PROFESSOR");
    }break;

    case StdString2HttpMethod::HttpMethod::BAD_REQUEST : {
        err_handler.genErrMssgUnauthorized(resp,"ControllerStudent method BAD REQUEST");
    }break;
    } // end switch
}

bool ControllerStudent::checkType(Utils::UriParser &_uri_parser, Poco::Net::HTTPServerResponse &resp){
    UserDb2Json user_db_2_json;
    Object::Ptr json_user_ptr;
    Array::Ptr json_array_ptr;
    Credentials::Authorization::Level auth_level;

    if(_uri_parser.getElement(0).compare("") == 0){
        err_handler.genErrMssgBadRequest(resp,"Controller student, request with no user id");
        return false;
    }


    json_array_ptr = user_db_2_json.getUser(auth_level,_uri_parser.getElement(0));

    if(json_array_ptr->size() == 0){
        // if there is no user by this ID generate ERROR JSON
        err_handler.genErrMssgBadRequest(resp,"Controller Student, request on non student type");
        return false;
    }

    json_user_ptr = json_array_ptr->getObject(0);
    std::string requested_type = json_user_ptr->get("type");

    if(requested_type.compare("student") != 0){
        err_handler.genErrMssgBadRequest(resp,"Controller Student, request on non student type");
        return false;
    }
    return true;
}



}
