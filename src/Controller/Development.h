#ifndef KELGS_CONTROLLER_DEVELOPMENT_H
#define KELGS_CONTROLLER_DEVELOPMENT_H

#include "Logger/Interface.h"
#include "Authorization/Authorization.h"
#include "Utils/UriParser.h"
#include "View/Development.h"

#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPServerRequest.h"

#include "Poco/JSON/Array.h"
#include "Poco/JSON/Object.h"
#include "Poco/JSON/Stringifier.h"

#include "Utils/ErrorHandler.h"

#include <iostream>

using Log::LoggerInterface;
using Credentials::Authorization;
using Utils::UriParser;


using Poco::Net::HTTPServerRequest;
using Poco::Net::HTTPServerResponse;

namespace Controller {

/**
 * @brief controller of the Development container
 *
 * @ingroup Controller
 *
 * @author Tomislav Tumbas
 *
 * @version 0.1
 *
 * @date 10.12.2016
 *
 * contact tumbas.tomislav@gmail.com tumbas@uns.ac.rs
 */
class ControllerDevelopment : public  LoggerInterface{
public:
    ControllerDevelopment():LoggerInterface("ControllerDevelopment"){}

    /**
     * @brief will generate HTML page with debug text on it
     * @param _status is Authorization Status of logged in user
     * @param _uri_parser is UriParser with request URI
     * @param req is HTTP Server Request
     * @param resp is HTTP Server Response
     */
    void showDevelopment(Authorization::Status &_status, Utils::UriParser &_uri_parser, HTTPServerRequest &req, HTTPServerResponse &resp);


};

}

#endif
