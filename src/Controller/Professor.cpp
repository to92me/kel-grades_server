#include "Professor.h"

namespace Controller{

const std::string ControllerProfessor::professor_name_string_in_db = "professor";

void ControllerProfessor::execute(Authorization::Status &_status, Utils::UriParser &_uri_parser, Poco::Net::HTTPServerRequest &req, Poco::Net::HTTPServerResponse &resp){

    switch (string_2_http.getHttpMethod(req.getMethod())) {
    case StdString2HttpMethod::HttpMethod::GET : {
        if(_status.level == Credentials::Authorization::Level::STUDENT ||
                _status.level == Credentials::Authorization::Level::PROFESSOR ||
                _status.level == Credentials::Authorization::Level::PROFESSOR_ADMIN ||
                _status.level == Credentials::Authorization::Level::ADMIN) get(_status,_uri_parser,req,resp);
        else err_handler.genErrMssgUnauthorized(resp);
    }break;

    case StdString2HttpMethod::HttpMethod::PUT : {
        if(_status.level == Credentials::Authorization::Level::PROFESSOR_ADMIN ||
                _status.level == Credentials::Authorization::Level::ADMIN) putAuthProfAndAdmin(_status,_uri_parser,req,resp);
        else err_handler.genErrMssgUnauthorized(resp,"ControllerStudent method:PUT, user not ADMIN");
    }break;

    case StdString2HttpMethod::HttpMethod::POST : {
        if(_status.level == Credentials::Authorization::Level::PROFESSOR_ADMIN ||
                _status.level == Credentials::Authorization::Level::ADMIN) postAuthProfAndAdmin(_status,_uri_parser,req,resp);
        else err_handler.genErrMssgUnauthorized(resp,"ControllerStudent method:POST, user not ADMIN");

    }break;

    case StdString2HttpMethod::HttpMethod::PATCH : {
        err_handler.genErrMssgMethodNotAllowed(resp,"ControllerStudent method:PATCH");
    }break;

    case StdString2HttpMethod::HttpMethod::DELETE : {
        if(_status.level == Credentials::Authorization::Level::PROFESSOR_ADMIN ||
                _status.level == Credentials::Authorization::Level::ADMIN) deleteAuthProfAndAdmin(_status,_uri_parser,req,resp);
        else err_handler.genErrMssgUnauthorized(resp,"ControllerStudent method:DELETE, user not ADMIN");
    }break;

    case StdString2HttpMethod::HttpMethod::BAD_REQUEST : {
        err_handler.genErrMssgUnauthorized(resp,"ControllerStudent method BAD REQUEST");
    }break;
    } // end switch
}

void ControllerProfessor::get(Authorization::Status &_status, Utils::UriParser &_uri_parser, Poco::Net::HTTPServerRequest &req, Poco::Net::HTTPServerResponse &resp){
    if(_uri_parser.getFormat() != UriParser::Format::JSON){
        err_handler.genErrMssgNotImplemented(resp,false);
        return;
    }

    Adapter::UserDb2Json user_db_2_json;
    Poco::JSON::Array::Ptr json_user_ptr = user_db_2_json.getUsersByType(professor_name_string_in_db);  // @todo

    resp.setContentType("Application/Json");
    std::ostream& out = resp.send();
    Stringifier::stringify(json_user_ptr,out);
    out.flush();
}

void ControllerProfessor::postAuthProfAndAdmin(Authorization::Status &_status, Utils::UriParser &_uri_parser, Poco::Net::HTTPServerRequest &req, Poco::Net::HTTPServerResponse &resp){
    if(_uri_parser.getFormat() != UriParser::Format::CSV){
        err_handler.genErrMssgNotImplemented(resp,true,"ControllerStudent::postAuthProfAndAdmin, !CSV");
        return;
    }

    std::istream& istr = req.stream();
    Utils::CsvHandler csv_h;
    UserCsv2Db user_csv_2_db;

    csv_h.read(istr);
    Authorization::Level level_new_user;
    if(_uri_parser.getElementCount() > 0 && _uri_parser.getElement(0).compare(ControllerAdmin::admin_name_string_in_db)){
        level_new_user = Authorization::Level::PROFESSOR_ADMIN;
    }else{
        level_new_user = Authorization::Level::PROFESSOR;
    }
    user_csv_2_db.createUser(req,resp, csv_h,_status.level,level_new_user);

    resp.setContentType("Application/txt");
    std::ostream& out = resp.send();
    csv_h.write(out);
    out.flush();
}

void ControllerProfessor::putAuthProfAndAdmin(Authorization::Status &_status, Utils::UriParser &_uri_parser, Poco::Net::HTTPServerRequest &req, Poco::Net::HTTPServerResponse &resp){
    if(_uri_parser.getFormat() != UriParser::Format::CSV){
        err_handler.genErrMssgNotImplemented(resp,true,"ControllerStudent::postAuthProfAndAdmin, !CSV");
        return;
    }

    std::istream& istr = req.stream();
    Utils::CsvHandler csv_h;
    UserCsv2Db user_csv_2_db;

    csv_h.read(istr);
    Authorization::Level level_new_user;
    if(_uri_parser.getElementCount() > 0 && _uri_parser.getElement(0).compare(ControllerAdmin::admin_name_string_in_db)){
        level_new_user = Authorization::Level::PROFESSOR_ADMIN;
    }else{
        level_new_user = Authorization::Level::PROFESSOR;
    }

    user_csv_2_db.updateUser(req,resp, csv_h,_status.level,level_new_user);

    resp.setContentType("Application/txt");
    std::ostream& out = resp.send();
    csv_h.write(out);
    out.flush();
}

void ControllerProfessor::deleteAuthProfAndAdmin(Authorization::Status &_status, Utils::UriParser &_uri_parser, Poco::Net::HTTPServerRequest &req, Poco::Net::HTTPServerResponse &resp){
    if(_uri_parser.getElementCount() == 0){
        ErrorHandler err_h;
        err_h.genErrMssgBadRequest(resp);
        return;
    }

    UserDb2Json user_db_2_json;
    Poco::JSON::Array::Ptr tmp_array = user_db_2_json.getUser(_status.level,_uri_parser.getElement(0));

    if(tmp_array->size() == 0){
        ErrorHandler err_h;
        err_h.genErrMssgBadRequest(resp);
        return;
    }

    UserDelete user_del;
    if(user_del.deleteUser(_uri_parser.getElement(0))){
        info("deleted user by id: " +_uri_parser.getElement(0));
        resp.setStatus(Poco::Net::HTTPServerResponse::HTTP_OK);
        std::ostream& out = resp.send();
        out << "<h3>OK</h3>";
        out.flush();
    }else{
        ErrorHandler err_h;
        err_h.genErrMssgDbUnreachable(resp);
    };
}

}
