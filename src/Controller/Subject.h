#ifndef CONTROLLERSUBJECT_H
#define CONTROLLERSUBJECT_H

#include "Logger/Interface.h"
#include "Authorization/Authorization.h"
#include "Utils/UriParser.h"
#include "Utils/StdString2HttpMethod.h"
#include "Utils/ErrorHandler.h"

#include "Adapter/User/UserDb2Json.h"
#include "Adapter/Subject/SubjectCsv2Db.h"
#include "Adapter/Subject/SubjectDb2Json.h"
#include "Adapter/Subject/SubjectDelete.h"

#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/JSON/Object.h"
#include "Poco/JSON/Array.h"

#include "iostream"

using Log::LoggerInterface;
using Credentials::Authorization;
using Utils::UriParser;
using Utils::StdString2HttpMethod;
using Utils::ErrorHandler;

using Adapter::UserDb2Json;
using Adapter::SubjectCsv2Db;
using Adapter::SubjectDb2Json;
using Adapter::SubjectDelete;

using Poco::Net::HTTPServerRequest;
using Poco::Net::HTTPServerResponse;
using Poco::JSON::Array;
using Poco::JSON::Object;
using Poco::JSON::Stringifier;

namespace Controller{

/**
 * @brief controller of the Subject container
 *
 * @ingroup Controller
 *
 * @author Tomislav Tumbas
 *
 * @version 0.1
 *
 * @date 10.12.2016
 *
 * contact tumbas.tomislav@gmail.com tumbas@uns.ac.rs
 */
class ControllerSubject : public LoggerInterface{
public:

    ControllerSubject() : LoggerInterface("ControllerSubject"){}

    /**
     * @brief main method witch will check logged in user authorization and HTTP Method and call corresponding method for each combination or generate error.
     * @param _status is Authorization Status of logged in user
     * @param _uri_parser is UriParser with request URI
     * @param req is HTTP Server Request
     * @param resp is HTTP Server Response
     */
    void execute(Authorization::Status &_status, Utils::UriParser &_uri_parser, Poco::Net::HTTPServerRequest &req, HTTPServerResponse &resp);


    static bool isNumber(const std::string& s);


private:
    StdString2HttpMethod string_2_http; ///< util for parsing Http method from string
    ErrorHandler err_handler; ///< error handler, generates error json and creates log

    /**
     * @brief method GET on container Subject with authorization Student
     * @param _status is Authorization Status of logged in user
     * @param _uri_parser is UriParser with request URI
     * @param req is HTTP Server Request
     * @param resp is HTTP Server Response
     */
    inline void getAuthStudent(Authorization::Status &_status, Utils::UriParser &_uri_parser, HTTPServerRequest &req, HTTPServerResponse &resp);

    /**
     * @brief method GET on container Subject with authorization Professor, ProfessorAdmin or ADMIN
     * @param _status is Authorization Status of logged in user
     * @param _uri_parser is UriParser with request URI
     * @param req is HTTP Server Request
     * @param resp is HTTP Server Response
     */
    inline void getAuthProfAndAdmin(Authorization::Status &_status, Utils::UriParser &_uri_parser, HTTPServerRequest &req, HTTPServerResponse &resp);

    /**
     * @brief method PUT on container Subject with authorization Professor, ProfessorAdmin or ADMIN
     * @param _status is Authorization Status of logged in user
     * @param _uri_parser is UriParser with request URI
     * @param req is HTTP Server Request
     * @param resp is HTTP Server Response
     */
    inline void putAuthProfAdminAndAdmin(Authorization::Status &_status, Utils::UriParser &_uri_parser, HTTPServerRequest &req, HTTPServerResponse &resp);

    /**
     * @brief method POST on container Subject with authorization Professor, ProfessorAdmin or ADMIN
     * @param _status is Authorization Status of logged in user
     * @param _uri_parser is UriParser with request URI
     * @param req is HTTP Server Request
     * @param resp is HTTP Server Response
     */
    inline void postAuthProfAdminAndAdmin(Authorization::Status &_status, Utils::UriParser &_uri_parser, Poco::Net::HTTPServerRequest &req, HTTPServerResponse &resp);

    /**
     * @brief method DELETE on container Subject with authorization Professor, ProfessorAdmin or ADMIN
     * @param _status is Authorization Status of logged in user
     * @param _uri_parser is UriParser with request URI
     * @param req is HTTP Server Request
     * @param resp is HTTP Server Response
     */
    inline void deleteAuthProfAdminAndAdmin(Authorization::Status &_status, Utils::UriParser &_uri_parser, HTTPServerRequest &req, HTTPServerResponse &resp);

    /**
     * @brief method PATCH on container Subject with authorization Professor, ProfessorADMIN or ADMIN
     * @param _status is Authorization Status of logged in user
     * @param _uri_parser is UriParser with request URI
     * @param req is HTTP Server Request
     * @param resp is HTTP Server Response
     */
    inline void patchAuthProfAndAdmin(Authorization::Status &_status, Utils::UriParser &_uri_parser, Poco::Net::HTTPServerRequest &req, HTTPServerResponse &resp);

    /**
     * @brief check if user of operation (get, put .. ) is student. If not return error and generate Error message.
     * @param _uri_parser is UriParser with request URI
     * @param resp is Poco::HTTPResponse
     * @return true if user is student otherwise false
     */
    bool checkType(Utils::UriParser &_uri_parser, HTTPServerResponse &resp);


    inline Poco::JSON::Array::Ptr getAllSubjects();


};

}

#endif
