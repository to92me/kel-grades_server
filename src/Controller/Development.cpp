#include "Development.h"

namespace Controller {

void ControllerDevelopment::showDevelopment(Authorization::Status &_status, Utils::UriParser &_uri_parser, Poco::Net::HTTPServerRequest &req, Poco::Net::HTTPServerResponse &resp){
    Utils::ErrorHandler err_h;

    if(req.getMethod().compare("GET") != 0){
        err_h.genErrMssgBadRequest(resp);
    }else{

        if(_status.level != Credentials::Authorization::Level::PROFESSOR_ADMIN &&
                _status.level != Credentials::Authorization::Level::ADMIN){

            //            if(_uri_parser.getFormat() == UriParser::Format::HTML){
            //                std::ostream& out = resp.send();
            //                out << "<h3>KEL GRADES SERVER!</h3>"
            //                    << "<p>unauthorized !! </p>"
            //                    << "";
            //                out.flush();
            //            }else if(_uri_parser.getFormat() == UriParser::Format::JSON){
            //                Poco::JSON::Object::Ptr json_obj = new Poco::JSON::Object();

            //                json_obj->set("ERROR","unauthorized");

            //                Poco::JSON::Stringifier::stringify(json_obj,true,resp.send());
            //            }else{
            //                std::ostream& out = resp.send();
            //                out << "unauthorized";
            //                out.flush();
            //            }

            //            resp.setStatus(Poco::Net::HTTPResponse::HTTPStatus::HTTP_UNAUTHORIZED);
            err_h.genErrMssgUnauthorized(resp);

        }else{

            if(_uri_parser.getFormat() == UriParser::Format::HTML){

                View::Development dev;
                dev.generateHtml(resp.send());
                resp.setContentType("text/html");
                resp.setStatus(Poco::Net::HTTPResponse::HTTPStatus::HTTP_OK);

            }else if(_uri_parser.getFormat() == UriParser::Format::JSON){
                Poco::JSON::Object::Ptr json_obj = new Poco::JSON::Object();

                json_obj = this->getLoggerInstance()->jsonLog();

                Poco::JSON::Stringifier::stringify(json_obj,resp.send());

                resp.setContentType("application/json");
                resp.setStatus(Poco::Net::HTTPResponse::HTTPStatus::HTTP_OK);
            }else{
                err_h.genErrMssgBadRequest(resp);
            }
        }
    }

    //    std::ostream& out = resp.send();

    //        std::cout << "<h3>KEL GRADES SERVER!</h3>"
    //            << "<p>Host: "   << req.getHost()   << "</p>"
    //            << "<p>Method: " << req.getMethod() << "</p>"
    //            << "<p>URI: "    << req.getURI()    << "</p>"
    //            << "<p>Status.message" << _status.message << "<p>"
    //            << "<p>Status.token " << _status.token << "<p>"
    //            << "<p>Status.id " << _status.user_id << "<p>"
    //            << "<p>status.year " << _status.token_exipre_date.year() << "<p>"
    //            << "<p>status.month " << _status.token_exipre_date.month() << "<p>"
    //            << "<p>status.day " << _status.token_exipre_date.day() << "<p>"
    //            << "";
    //    out.flush();

}

}
