#include "Subject.h"

namespace Controller{

void ControllerSubject::execute(Authorization::Status &_status, Utils::UriParser &_uri_parser, HTTPServerRequest &req, HTTPServerResponse &resp){
    switch (string_2_http.getHttpMethod(req.getMethod())) {
    case StdString2HttpMethod::HttpMethod::GET : {
        if(_status.level == Credentials::Authorization::Level::STUDENT) getAuthStudent(_status,_uri_parser,req,resp);
        else if(_status.level == Credentials::Authorization::Level::PROFESSOR ||
                _status.level == Credentials::Authorization::Level::PROFESSOR_ADMIN ||
                _status.level == Credentials::Authorization::Level::ADMIN) getAuthProfAndAdmin(_status,_uri_parser,req,resp);
        else err_handler.genErrMssgUnauthorized(resp);
    }break;

    case StdString2HttpMethod::HttpMethod::PUT : {
        if(_status.level == Credentials::Authorization::Level::PROFESSOR_ADMIN ||
                _status.level == Credentials::Authorization::Level::ADMIN) putAuthProfAdminAndAdmin(_status,_uri_parser,req,resp);
        else err_handler.genErrMssgUnauthorized(resp);
    }break;

    case StdString2HttpMethod::HttpMethod::POST : {
        if(_status.level == Credentials::Authorization::Level::PROFESSOR_ADMIN ||
                _status.level == Credentials::Authorization::Level::ADMIN) postAuthProfAdminAndAdmin(_status,_uri_parser,req,resp);
        else err_handler.genErrMssgUnauthorized(resp);

    }break;

    case StdString2HttpMethod::HttpMethod::PATCH : {
        if(_status.level == Credentials::Authorization::Level::PROFESSOR_ADMIN ||
                _status.level == Credentials::Authorization::Level::PROFESSOR ||
                _status.level == Credentials::Authorization::Level::ADMIN ) patchAuthProfAndAdmin(_status,_uri_parser,req,resp);
        else err_handler.genErrMssgUnauthorized(resp);
    }break;

    case StdString2HttpMethod::HttpMethod::DELETE : {
        if(_status.level == Credentials::Authorization::Level::PROFESSOR_ADMIN ||
                _status.level == Credentials::Authorization::Level::ADMIN) deleteAuthProfAdminAndAdmin(_status,_uri_parser,req,resp);
        else err_handler.genErrMssgUnauthorized(resp,"ControllerStudent method:DELETE, user not ADMIN or PROFESSOR");
    }break;

    case StdString2HttpMethod::HttpMethod::BAD_REQUEST : {
        err_handler.genErrMssgUnauthorized(resp,"ControllerStudent method BAD REQUEST");
    }break;
    } // end switch
}

void ControllerSubject::getAuthStudent(Authorization::Status &_status, Utils::UriParser &_uri_parser,  Poco::Net::HTTPServerRequest &req, Poco::Net::HTTPServerResponse &resp){
    // if request is on example /subject.js return json with all subjects in database.

    if(_uri_parser.getFormat() != UriParser::Format::JSON){
        err_handler.genErrMssgNotImplemented(resp,true,"ControllerStudent::getAuthStudent, !JSON");
        return;
    }

    SubjectDb2Json subject_db_2_json;
    Poco::JSON::Array::Ptr subject_array_ptr;

    if(_uri_parser.getElementCount() == 0){
        subject_array_ptr = subject_db_2_json.getAllSubjects();

    }else{
        //if subject is requested by id argument is number, and if it is requested by name argument is string
        if(isNumber(_uri_parser.getElement(0))){
            subject_array_ptr = subject_db_2_json.getSubjectWithPartsAndExams(std::stoi(_uri_parser.getElement(0)),_status.user_id);
        }else{
            subject_array_ptr = subject_db_2_json.getSubjectWithPartsAndExams(_uri_parser.getElement(0),_status.user_id);
        }
    }

    resp.setContentType("Application/Json");
    std::ostream& out = resp.send();
    Stringifier::stringify(subject_array_ptr,out);
    out.flush();
}

void ControllerSubject::getAuthProfAndAdmin(Authorization::Status &_status, Utils::UriParser &_uri_parser,  Poco::Net::HTTPServerRequest &req, Poco::Net::HTTPServerResponse &resp){

    if(_uri_parser.getFormat() != UriParser::Format::JSON){
        err_handler.genErrMssgNotImplemented(resp,true,"ControllerStudent::getAuthProfAndAdmin, !JSON");
        return;
    }

    SubjectDb2Json subject_db_2_json;
    Poco::JSON::Array::Ptr subject_array_ptr;

    if(_uri_parser.getElementCount() == 0){
        subject_array_ptr = subject_db_2_json.getAllSubjects();

    }else{
        //if subject is requested by id argument is number, and if it is requested by name argument is string
        if(isNumber(_uri_parser.getElement(0))){
            subject_array_ptr = subject_db_2_json.getSubjectWithPartsAndExams(std::stoi(_uri_parser.getElement(0)));
        }else{
            subject_array_ptr = subject_db_2_json.getSubjectWithPartsAndExams(_uri_parser.getElement(0));
        }
    }

    resp.setContentType("Application/Json");
    std::ostream& out = resp.send();
    Stringifier::stringify(subject_array_ptr,out);
    out.flush();
}

void ControllerSubject::postAuthProfAdminAndAdmin(Authorization::Status &_status, Utils::UriParser &_uri_parser,  Poco::Net::HTTPServerRequest &req, Poco::Net::HTTPServerResponse &resp){
    if(_uri_parser.getFormat() != UriParser::Format::CSV){
        err_handler.genErrMssgNotImplemented(resp,true,"ControllerStudent::postAuthProfAndAdmin, !CSV");
        return;
    }

    SubjectCsv2Db sub_csv_2_db;
    std::istream& istr = req.stream();
    Utils::CsvHandler csv_h;

    csv_h.read(istr);

    if(_uri_parser.getElementCount() == 0){
        sub_csv_2_db.createSubject(csv_h);
    }else{
        if(csv_h.get(0,0).compare(_uri_parser.getElement(0)) == 0){
            sub_csv_2_db.createSubjectPartsAndAddStudents(csv_h);
        }else{
            csv_h.pushElementBack(0,"Url and Subject name in CSV mistmach - error");
        }
    }
    resp.setContentType("Application/txt");
    std::ostream& out = resp.send();
    csv_h.write(out);
    out.flush();
}

void ControllerSubject::putAuthProfAdminAndAdmin(Authorization::Status &_status, Utils::UriParser &_uri_parser,  Poco::Net::HTTPServerRequest &req, Poco::Net::HTTPServerResponse &resp){
    if(_uri_parser.getFormat() != UriParser::Format::CSV){
        err_handler.genErrMssgNotImplemented(resp,true,"ControllerStudent::postAuthProfAndAdmin, !CSV");
        return;
    }
    SubjectCsv2Db sub_csv_2_db;
    std::istream& istr = req.stream();
    Utils::CsvHandler csv_h;

    csv_h.read(istr);

    if(_uri_parser.getElementCount() == 0){
         sub_csv_2_db.updateSubject(csv_h);
    }else{
        if(csv_h.get(0,0).compare(_uri_parser.getElement(0)) == 0){
            sub_csv_2_db.updateStudentsGradesAndScores(csv_h);
        }else{
            csv_h.pushElementBack(0,"Url and Subject name in CSV mistmach - error");
        }
    }

    resp.setContentType("Application/txt");
    std::ostream& out = resp.send();
    csv_h.write(out);
    out.flush();
}

void ControllerSubject::deleteAuthProfAdminAndAdmin(Authorization::Status &_status, Utils::UriParser &_uri_parser,  Poco::Net::HTTPServerRequest &req, Poco::Net::HTTPServerResponse &resp){

    SubjectDb2Json subject_db_2_json;
    Poco::JSON::Array::Ptr subject_array_ptr;

    bool is_number = isNumber(_uri_parser.getElement(0));

    if(is_number){
        subject_array_ptr = subject_db_2_json.getSubjectById((std::stoi(_uri_parser.getElement(0))));
    }else{
        subject_array_ptr = subject_db_2_json.getSubjectByName(_uri_parser.getElement(0));
    }

    if(subject_array_ptr->size() == 0){
        // there is no Subject by this id
        err_handler.genErrMssgBadRequest(resp);
        return;
    }

    SubjectDelete subject_delete;
    bool success;
    if(is_number){
        success = subject_delete.deleteSubject(std::stoi(_uri_parser.getElement(0)));
    }else{
        success = subject_delete.deleteSubject(_uri_parser.getElement(0));
    }

    if(success){
        info("deleted subject by id: " +_uri_parser.getElement(0));
        resp.setStatus(Poco::Net::HTTPServerResponse::HTTP_OK);
        std::ostream& out = resp.send();
        out << "<h3>OK</h3>";
        out.flush();
    }else{
        err_handler.genErrMssgDbUnreachable(resp);
    }

}

void ControllerSubject::patchAuthProfAndAdmin(Authorization::Status &_status, Utils::UriParser &_uri_parser, Poco::Net::HTTPServerRequest &req, Poco::Net::HTTPServerResponse &resp){
    if(_uri_parser.getFormat() != UriParser::Format::CSV){
        err_handler.genErrMssgNotImplemented(resp,true,"ControllerStudent::postAuthProfAndAdmin, !CSV");
        return;
    }

    SubjectCsv2Db subject_csv_2_db;

    std::istream& istr = req.stream();
    Utils::CsvHandler csv_h;

    csv_h.read(istr);

    subject_csv_2_db.updateSubjectStaff(csv_h);

    resp.setContentType("Application/txt");
    std::ostream& out = resp.send();
    csv_h.write(out);
    out.flush();
}

bool ControllerSubject::checkType(Utils::UriParser &_uri_parser, Poco::Net::HTTPServerResponse &resp){
    UserDb2Json user_db_2_json;
    Object::Ptr json_user_ptr;
    Array::Ptr json_array_ptr;
    Credentials::Authorization::Level auth_level;

    if(_uri_parser.getElement(0).compare("") == 0){
        err_handler.genErrMssgBadRequest(resp,"Controller student, request with no user id");
        return false;
    }

    json_array_ptr = user_db_2_json.getUser(auth_level,_uri_parser.getElement(0));

    if(json_array_ptr->size() == 0){
        // if there is no user by this ID generate ERROR JSON
        err_handler.genErrMssgBadRequest(resp,"Controller Student, request on non student type");
        return false;
    }

    json_user_ptr = json_array_ptr->getObject(0);
    std::string requested_type = json_user_ptr->get("type");

    if(requested_type.compare("student") != 0){
        err_handler.genErrMssgBadRequest(resp,"Controller Student, request on non student type");
        return false;
    }
    return true;
}

bool ControllerSubject::isNumber(const std::string& s)
{
    return !s.empty() && std::find_if(s.begin(),
                                      s.end(), [](char c) { return !std::isdigit(c); }) == s.end();
}
}
