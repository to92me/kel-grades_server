#ifndef KELGS_CONTROLLER_ADMIN_H
#define KELGS_CONTROLLER_ADMIN_H

#include "Logger/Interface.h"
#include "Authorization/Authorization.h"
#include "Utils/UriParser.h"
#include "Utils/ErrorHandler.h"
#include "Utils/StdString2HttpMethod.h"

#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/JSON/Object.h"
#include "Poco/JSON/Array.h"

#include "Adapter/User/UserDb2Json.h"
#include "Adapter/User/UserDelete.h"
#include "Adapter/User/UserCsv2Db.h"

#include "Controller/Professor.h"

#include "iostream"

using Log::LoggerInterface;
using Credentials::Authorization;
using Utils::UriParser;
using Utils::ErrorHandler;
using Utils::StdString2HttpMethod;
using Utils::CsvHandler;

using Poco::Net::HTTPServerRequest;
using Poco::Net::HTTPServerResponse;
using Poco::JSON::Array;
using Poco::JSON::Object;
using Poco::JSON::Stringifier;

using Adapter::UserDb2Json;
using Adapter::UserDelete;
using Adapter::UserCsv2Db;

namespace Controller {

/**
 * @brief  Controller of the Admin container
 *
 * @ingroup Controller
 *
 * @author Tomislav Tumbas
 *
 * @version 0.1
 *
 * @date 23.12.2016
 *
 * contact tumbas.tomislav@gmail.com tumbas@uns.ac.rs
 */
class ControllerAdmin: public LoggerInterface{
public:
    /**
     * @brief default constructor
     */
    ControllerAdmin(): LoggerInterface("ControllerAdmin"){}

    /**
     * @brief main method witch will check logged in user authorization and HTTP Method and call corresponding method for each combination or generate error.
     * @param _status is Authorization Status of logged in user
     * @param _uri_parser is UriParser with request URI
     * @param req is HTTP Server Request
     * @param resp is HTTP Server Response
     */
    void execute(Authorization::Status &_status, Utils::UriParser &_uri_parser, Poco::Net::HTTPServerRequest &req, HTTPServerResponse &resp);

    const static std::string admin_name_string_in_db;

private:

    StdString2HttpMethod string_2_http; ///<  for parsing HTTP method from string 
    ErrorHandler err_handler; ///< error handler, generates error JSON and creates log

    /**
     * @brief method GET on container ADMIN  for all authorizations 
     * @param _status is Authorization Status of logged in user
     * @param _uri_parser is UriParser with request URI
     * @param req is HTTP Server Request
     * @param resp is HTTP Server Response
     */
    inline void get(Authorization::Status &_status, Utils::UriParser &_uri_parser, HTTPServerRequest &req, HTTPServerResponse &resp);

    /**
     * @brief method PUT on container ADMIN with authorization Professor, ProfessorAdmin or ADMIN
     * @param _status is Authorization Status of logged in user
     * @param _uri_parser is UriParser with request URI
     * @param req is HTTP Server Request
     * @param resp is HTTP Server Response
     */
    inline void putAuthProfAndAdmin(Authorization::Status &_status, Utils::UriParser &_uri_parser, HTTPServerRequest &req, HTTPServerResponse &resp);

    /**
     * @brief method POST on container ADMIN with authorization Professor, ProfessorAdmin or ADMIN
     * @param _status is Authorization Status of logged in user
     * @param _uri_parser is UriParser with request URI
     * @param req is HTTP Server Request
     * @param resp is HTTP Server Response
     */
    inline void postAuthProfAndAdmin(Authorization::Status &_status, Utils::UriParser &_uri_parser, Poco::Net::HTTPServerRequest &req, HTTPServerResponse &resp);

    /**
     * @brief method DELETE on container ADMIN with authorization Professor, ProfessorAdmin or ADMIN
     * @param _status is Authorization Status
     * @param _uri_parser is UriParser with request URI
     * @param req is HTTP Server Request
     * @param resp is HTTP Server Response
     */
    inline void deleteAuthProfAndAdmin(Authorization::Status &_status, Utils::UriParser &_uri_parser, HTTPServerRequest &req, HTTPServerResponse &resp);

};

}
#endif
