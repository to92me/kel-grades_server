set(AUTHORIZATION_SRC 
    Authorization.cpp
    
)

add_library(Authorization ${AUTHORIZATION_SRC})

target_link_libraries(Authorization Logger MySqlHandler Server Utils)


