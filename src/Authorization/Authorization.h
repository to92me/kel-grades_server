#ifndef AUTHORIZATION_H
#define AUTHORIZATION_H

#include "Logger/Interface.h"
#include "Poco/Data/Session.h"
#include "MySqlHandler/MySqlHandler.h"
#include "Poco/DateTime.h"
#include "Poco/StringTokenizer.h"
#include <string>
#include "Poco/SHA1Engine.h"
#include "Poco/Random.h"

using Log::LoggerInterface;
using Poco::DateTime;
using Poco::StringTokenizer;
using Poco::SHA1Engine;

namespace Credentials {

/**
 * @brief The Authorization class
 * @ingroup Credentials
 * @author Tomislav Tubas
 *
 * @version 0.1
 * @todo cache some authorizatinos data. If User is currently using application cache his token
 *
 * @note not done
 * @date 26.11.2015
 *
 * contact tumbas.tomislav@gmail.com tumbas@uns.ac.rs
 */
class Authorization : public LoggerInterface{
public:

    /**
     * @brief authorization level of every user
     *
     */
    enum Level{
        NONE, /*!< has no authorization, */
        STUDENT, /*!< student can see his data and his subjects. Student authorization level can not modifie anything */
        PROFESSOR, /*!< professor can see his subjects and students on his subjects. Professor can modifie parts of his subject and information of subject.
                    also can add students to subject*/
        PROFESSOR_ADMIN, /*!< professor admin can everything as professor + modifiing subjects ( adding deleteing ... ), adding professor to subject
                    and adding professor to professor admin */
        ADMIN,  /*!< admin can edit anything */
    };

    /**
     * @brief The AuthorizationStatus from authorization
     */
    enum Login{
        SUCCESS, ///< sucessfully loged in
        ERROR_DB, ///< error in connection to database
        ERROR_CREDENTIALS, ///< no user with those credentials
        ERROR_TOKEN_EXPIRED, ///< token has expired
        ERROR_TOKEN_UNKNOWN  ///< no user with given token
    };

    /**
     * @brief The Status struct contains information about login process
     */
    struct Status{
        Level level = Level::NONE;  ///< which level of authorization user has
        std::string message = "not used"; ///< in case of error this is error message
        Login login = Login::ERROR_DB; ///< login process status, see struct Login
        std::string token; ///< token for this user
        Poco::Data::Date token_exipre_date; ///< after this date token won't be valid anymore. After this date user has to genereate new token with login(uname, passwd) method.
        std::string user_id = "not set";
    };

    /**
     * @brief Defaulth constructor
     */
    Authorization():LoggerInterface("Autorization"){
        this->mysql_handler = MySql::MySqlHandler::getInstance();
    }

    /**
      * @brief Defaulth destructor
      */
    ~Authorization(){
        if (createdSession == true && this->session_mysql){
            session_mysql->close();
        }
    }

    /**
     * @brief try logiu to applicaton with token
     * @param _token is std::string token got from app before
     * @return struct Status @see struct Status for more inormation
     */
    Status login(std::string _token);

    /**
     * @brief try login with user name and password. This method will return user token.
     * @param _uname is user name for login (for student index and for professor jmbg)
     * @param _passwd is password for login
     * @return struct Status @see struct Status for more information
     */
    Status login(std::string _uname, std::string _passwd);

    /**
     * @brief if user has authorization level of Level::ADMIN or Level::PROFESSOR_ADMIN admin he can modifie authorization level of other users.
     * @param _userid is ID from user table from database (for student index and for professor jmbg)
     * @param _level is new level of authorization for given user ID
     * @return true if modification is success
     * @note this method can use only user with level of Level::ADMIN or Level::PROFESSOR_ADMIN, otherwise method will do nothing and return false.
     * @todo modifieAauthLevel is not implemented jet
     */
    bool modifieAauthLevel(std::string _userid, Level _level);

    /**
     * @brief returns string of Authorization string. Example level is Authorization::Level::STUDENT then return std::string "student"
     * @param _level is Authorization::Level that is converted to std::string
     * @return Authorization is std::string
     */
    static std::string getAuthorizationLevelString(Authorization::Level &_level);

    /**
     * @brief return true if _level has admin status, otherwise false
     * @param _level is Authorization::Level
     * @return bool Admin status
     */
    static bool getAuthorizationAdminBool(Authorization::Level &_level);

    /**
     * @brief method will dethermine which Level of authorization user has, from database user.type and database user.admin
     * @param _type is type from databse user.type
     * @param _isadmin is bit from databse user.admin
     * @return Level @see Lever for more information
     */
   static Authorization::Level getLevel(std::string _type, bool _isadmin);

   /**
    * @brief generates string from date (for purposes of generate token _date paramtere @see generateToken)
    * @param _date is date stored in Poco::Data::Date
    * @return string of date if format yearmonthday
    */
   static std::string date2string(Poco::Data::Date _date);

private:
    /**
     * @brief convert date stored in string to Poco::DateTime
     * @param _date is date stored in string got from mysql
     * @return DateTime is parsed date
     */
    DateTime parseStringDate(std::string _date);

    /**
     * @brief mehod will generate session token using SHA1 algorirhm
     * @param _uname is username from user
     * @param _passwd is password from user
     * @param _date is date when token will expire
     * @param _key is random key
     * @return session token
     */
    std::string generateToken(std::string _uname, std::string _passwd, std::string _date);

    Poco::Data::Session* session_mysql;
    MySql::MySqlHandler* mysql_handler;

    /**
     * @brief connect to data base and generate Session
     * @return true if connection is established
     */
    bool connectDb();
    bool createdSession = false;

    DateTime date;

};

}

#endif
