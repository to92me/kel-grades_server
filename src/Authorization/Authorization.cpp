#include "Authorization.h"

namespace Credentials {

Authorization::Status Authorization::login(std::string _token){
    Status status;
    if(this->connectDb() == true){
        Poco::Nullable<std::string> db_type;
        Poco::Nullable<std::string> db_user_id;
        Poco::Nullable<bool> db_admin;
        Poco::Nullable<Poco::Data::Date> db_token_expire_date;
        DateTime now_time;
        DateTime token_expire_date;

        std::vector<std::string> tables_got;

        *session_mysql << " SELECT \
                          user.user_id,\
                user.type,\
                user.admin,\
                credentials.token_expire_date\
                FROM \
                credentials \
                JOIN \
                user ON \
                credentials.user_id = user.user_id \
                WHERE \
                credentials.token = ?;",
        into(db_user_id),into(db_type), into(db_admin), into(db_token_expire_date) ,use(_token), now;

        //close session -don't need it anymore
        session_mysql->close();

        // check if all data is correct ( not NULL)
        if(db_token_expire_date.isNull() == false &&
                db_admin.isNull() == false &&
                db_type.isNull() == false &&
                db_type.isNull() == false){

            token_expire_date.assign(db_token_expire_date.value().year(), db_token_expire_date.value().month(), db_token_expire_date.value().day());

            // check if token is still OK
            if(token_expire_date > now_time){
                Level level;

                //get authorization level of user
                level = getLevel(db_type.value(),db_admin.value());

                //if level is NULL type of user is incorrect in database
                if(level == Level::NONE){
                    status.message = "Unknown type of user";
                    status.login = Login::ERROR_TOKEN_UNKNOWN;
                }else{
                    status.level = level;
                    status.login = Login::SUCCESS;
                    status.token = _token;
                    status.token_exipre_date = db_token_expire_date.value();
                    status.user_id = db_user_id.value();
                    status.message = "All OK";
                }

                // token has expired
            }else{
                status.login = Login::ERROR_TOKEN_EXPIRED;
                status.message = "token has exipred";
            }

            // all data is null
        }else{
            status.login = Login::ERROR_TOKEN_UNKNOWN;
            status.message = "Unknown token";
        }
    }else{
        status.login = Login::ERROR_DB;
        status.message = "Error in connection to data base";
    }
    return status;
}

Authorization::Status Authorization::login(std::string _uname, std::string _passwd){
    Status status;

    if(this->connectDb() == true){

        Poco::Nullable<std::string> db_type;
        Poco::Nullable<bool> db_admin;
        Poco::Nullable<std::string> db_token;
        Poco::Nullable<std::string> db_user_id;
        Poco::Nullable<Poco::Data::Date> db_token_expire_date;

        *session_mysql << "SELECT \
                          user.user_id, \
                user.type, \
                user.admin, \
                credentials.token, \
                credentials.token_expire_date \
                FROM \
                credentials \
                join user on credentials.user_id = user.user_id \
                WHERE \
                credentials.user_id = ? and credentials.passwd = ?; ",
        into(db_user_id), into(db_type), into(db_admin), into(db_token), into(db_token_expire_date), use(_uname), use(_passwd), now;


        // if type and admin are NULL then there is no user in database registed by credentials _uname and _passwd
        if(db_type.isNull() == true || db_admin.isNull() == true || db_user_id.isNull() == true){
            status.login = Login::ERROR_CREDENTIALS;
            status.message = "unknown username or bad password";


        }else{

            Poco::Data::Date date,date1;
            Level level;

            date.assign((date1.year()+1),date1.month(),date1.day());

            std::string date_str = date2string(date);
            std::string token = generateToken(_uname, _passwd, date_str);

            //get authorization level of user
            level = getLevel(db_type.value(),db_admin.value());

            //if level is NULL type of user is incorrect in database
            if(level == Level::NONE){

                status.message = "Unknown type of user";
                status.login = Login::ERROR_TOKEN_UNKNOWN;

            }else{

                *session_mysql << " UPDATE credentials \
                                  SET \
                                  token = ?, \
                        token_expire_date = ? \
                            WHERE \
                            credentials.user_id = ?;",
                use(token),use(date_str),use(db_user_id.value()),now;

                session_mysql->close();

                status.token = token;
                status.token_exipre_date = date;
                status.level = level;
                status.login = Login::SUCCESS;
                status.message = "All OK";
                status.user_id = db_user_id.value();
            } // end if level
        }// ende else
    }// end if connected
    return status;
}

bool Authorization::modifieAauthLevel(std::__cxx11::string _userid, Level _level){
    //TODO
    bool success = false;
    return success;
}

bool Authorization::connectDb(){
    bool success = true;

    if(this->createdSession == false){
        createdSession = true;

        try{
            this->session_mysql = new Poco::Data::Session(Poco::Data::MySQL::Connector::KEY, this->mysql_handler->getConnectorString());
        }catch(Poco::Data::UnknownDataBaseException & ex){
            success = false;
            error(ex.message());
        }
    }

    if (success == true ){
        if(this->session_mysql->isConnected() == false){
            try{
                this->session_mysql->reconnect();
            }catch(Poco::Data::UnknownDataBaseException & ex){
                success = false;
                error(ex.message());
            }
        }
    }
    return success;
}

DateTime Authorization::parseStringDate(std::string _date){
    StringTokenizer s_tokenizer(_date,"-",StringTokenizer::TOK_TRIM | StringTokenizer::TOK_IGNORE_EMPTY);
    StringTokenizer::Iterator s_iterator = s_tokenizer.begin();
    DateTime date;

    int year = stoi(*s_iterator);
    s_iterator++;
    int month = stoi(*s_iterator);
    s_iterator++;
    int day = stoi(*s_iterator);

    date.assign(year,month,day);

    return date;

}

Authorization::Level Authorization::getLevel(std::__cxx11::string _type, bool _isadmin){
    if(_type.compare("student") == 0) return Level::STUDENT;
    else if(_type.compare("professor") == 0){
        if(_isadmin == false) return Level::PROFESSOR;
        else return Level::PROFESSOR_ADMIN;
    }
    else if(_type.compare("admin") == 0) return Level::ADMIN;
    else{
//        error("got type is non of expected! got:" + _type);
        return Level::NONE;
    }
}

std::string Authorization::generateToken(std::string _uname, std::string _passwd, std::string _date){
    SHA1Engine sha1_engine;
    std::string key;

    //    if(_key.isNull()){
    Poco::Random random;
    random.seed();

    for(unsigned int i = 0; i < random.next(20); i++){
        key = key + random.nextChar();
    }

    //    }else{
    //        key = _key.value();
    //    }

    sha1_engine.update(_uname);
    sha1_engine.update(_passwd);
    sha1_engine.update(_date);
    sha1_engine.update(key);

    const Poco::DigestEngine::Digest& digest = sha1_engine.digest();

    std::string token(Poco::DigestEngine::digestToHex(digest));

    return token;
}

std::string Authorization::date2string(Poco::Data::Date _date){
    std::string date;
    date    = std::to_string(_date.year())
            + "-"
            + std::to_string(_date.month())
            + "-"
            + std::to_string(_date.day());
    return date;
}

std::string Authorization::getAuthorizationLevelString(Authorization::Level &_level){
    switch (_level) {
    case Authorization::Level::STUDENT: return "student";
    case Authorization::Level::PROFESSOR: return "professor";
    case Authorization::Level::PROFESSOR_ADMIN: return "professor";
    case Authorization::Level::ADMIN: return "admin";
    default:
        throw std::out_of_range("row out of range");
    }
}

bool Authorization::getAuthorizationAdminBool(Authorization::Level &_level){
    if(_level == Authorization::Level::ADMIN ||
            _level == Authorization::Level::PROFESSOR_ADMIN){
        return true;
    }else{
        return false;
    }
}

}
