/** @defgroup Adapter Adapter
* is group of adapters, they convert formats like json csv and xml to data base transactions
*/

/** @defgroup Credentials Credentials
* is set of classes that provide login, authorization levels and statuses 
*/

/** @defgroup Log Log 
* is loggin group. It contains Logger singleton and Logger interface. 
*/ 

/** @defgroup MySql MySql 
* is grop of classes that manage connections (sessions) to database 
* @note MySqlHandler is broken :/ for now it just creates connection string chain 
* @todo create new implementatino of one thread that will create sessions in pool, 
* offer them to other and store them while they are in use
*/ 

/** @defgroup Server Server
* are classes that create Http server and TLS soceket. 
* @note this module requires CA folder (Certificate Authoritiy)
*/ 

/** @defgroup Utils Utils
* those are common utils for this application
*/ 

/** @defgroup View View
* this group represents Html web pages
*/ 

/** @defgroup HttpServer HttpServer
 *this group creates SSL socket, Http stack on top of it, and request thread provider.
 */
