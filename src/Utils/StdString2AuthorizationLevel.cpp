#include "StdString2AuthorizationLevel.h"

namespace Utils{

Credentials::Authorization::Level StdString2AuthorizationLevel::convert(std::__cxx11::string _type, bool _isadmin){
    if(_type.compare("student") == 0) return Credentials::Authorization::Level::STUDENT;
    else if(_type.compare("professor") == 0){
        if(_isadmin == false) return Credentials::Authorization::Level::PROFESSOR;
        else return Credentials::Authorization::Level::PROFESSOR_ADMIN;
    }
    else if(_type.compare("admin") == 0) return Credentials::Authorization::Level::ADMIN;
    else{
        //!< @todo
//        error("got type is non of expected! got:" + _type);
        return Credentials::Authorization::Level::NONE;
    }
}

}
