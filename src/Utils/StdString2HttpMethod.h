#ifndef KELGS_UTILS_STDSTRING2HTTPMETHOD_H
#define KELGS_UTILS_STDSTRING2HTTPMETHOD_H

#include "iostream"

namespace Utils {

class StdString2HttpMethod{
public:
    enum HttpMethod{
        BAD_REQUEST,
        GET,
        PUT,
        POST,
        DELETE,
        PATCH
    };

   static HttpMethod getHttpMethod(std::string _string_method);

};

}

#endif
