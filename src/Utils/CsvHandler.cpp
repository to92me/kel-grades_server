#include "CsvHandler.h"

namespace Utils {

void CsvHandler::read(std::istream &_istr){
    while(_istr){
        std::string line;
        csv_row_t row;

        std::getline(_istr,line);

        //# HOTFIX @bug @todo TODO
        // because in exam points can be blank and string tokenizer will eat that empty spaces. empty spaces ex. ,, will
        // be replaces with -
        line.erase(remove_if(line.begin(), line.end(), isspace), line.end());
        line = Poco::replace(line,",,",",-,");

        StringTokenizer tokenizer(line,",",StringTokenizer::TOK_TRIM | StringTokenizer::TOK_IGNORE_EMPTY);

        for( StringTokenizer::Iterator t_iterator = tokenizer.begin(); t_iterator != tokenizer.end(); t_iterator++){
            row.push_back(*t_iterator);
        }

        if(row.size() == 0){
//            delete row;
        }else{
            csv_deque.push_back(row);
        }
    }
}

void CsvHandler::write(std::ostream &_ostr){
    while(!csv_deque.empty()){
        csv_row_t row = csv_deque.front();
        csv_deque.pop_front();

        for(csv_row_t::iterator r_iterator  = row.begin(); r_iterator != row.end() ; ++r_iterator){
            _ostr << *r_iterator;
            _ostr << ",";
//            debug(*r_iterator);
        }
        _ostr << "\n";
    }
}

CsvHandler::csv_row_t& CsvHandler::get(unsigned int _row){
    if(_row >= csv_deque.size()){
        throw std::out_of_range("row out of range");
    }
    return csv_deque[_row];
}

std::string CsvHandler::get(unsigned int _row, unsigned int _element){
    if(_row >= csv_deque.size()){
        throw std::out_of_range("row out of range");
    }

    if(_element >= csv_deque[_row].size()){
        throw std::out_of_range("row out of range");
    }

    return csv_deque[_row][_element];
}

CsvHandler::csv_deque_t& CsvHandler::get(){
    return csv_deque;
}

void CsvHandler::pushFront(const csv_row_t _row){
    csv_deque.push_front(_row);
}

void CsvHandler::pushBack(const csv_row_t _row){
    csv_deque.push_back(_row);
}

void CsvHandler::pushElementFront(const unsigned int _row, const std::__cxx11::string _element){
    if(_row >= csv_deque.size()){
        throw std::out_of_range("row out of range");
    }
    csv_deque[_row].insert( csv_deque[_row].begin(),_element);

}

void CsvHandler::pushElementBack(unsigned int _row, std::__cxx11::string _element){
    if(_row >= csv_deque.size()){
        throw std::out_of_range("row out of range");
    }
    csv_deque[_row].push_back(_element);
}

int CsvHandler::size() const{
    return csv_deque.size();
}

int CsvHandler::rowSize(unsigned int _row) const{
    if(_row >= csv_deque.size()){
        throw std::out_of_range("row out of range");
    }
    return csv_deque[_row].size();
}

CsvHandler::csv_row_t& CsvHandler::front(){
    if(csv_deque.empty()){
        throw std::out_of_range("csv queue is empty");
    }
    return csv_deque.front();
}

CsvHandler::csv_row_t& CsvHandler::back(){
    if(csv_deque.empty()){
        throw std::out_of_range("csv queue is empty");
    }
    return csv_deque.back();
}

void CsvHandler::popFront(){
    csv_deque.pop_front();
}

void CsvHandler::popBack(){
    csv_deque.pop_back();
}

}
