#ifndef KELGS_UTILS_ERRORHANDER_H
#define KELGS_UTILS_ERRORHANDER_H

#include "Logger/Interface.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Authorization/Authorization.h"

using Log::LoggerInterface;
using Poco::Net::HTTPServerResponse;
using Poco::Net::HTTPResponse;
using Credentials::Authorization;

namespace Utils{

/**
 * @brief The ErrorHandler class
 */
class ErrorHandler : public LoggerInterface{
public:
    /**
     * @brief Constructor
     */
    ErrorHandler():LoggerInterface("ErrorHandler"){}

    /**
      * @brief Desctructor
      */
    ~ErrorHandler(){}

    /**
     * @brief genErrMssgUnauthorized
     * @param _resp
     * @param _module_print
     */
    void genErrMssgUnauthorized(HTTPServerResponse &_resp, std::string _module_print = "");

    /**
     * @brief genErrMssgMethodNotAllowed
     * @param _resp
     * @param _module_print
     */
    void genErrMssgMethodNotAllowed(HTTPServerResponse &_resp, std::string _module_print = "");

    /**
     * @brief genErrMssgBadRequest
     * @param _resp
     * @param _module_print
     */
    void genErrMssgBadRequest(HTTPServerResponse &_resp, std::string _module_print = "");

    /**
     * @brief genErrMssgBadAuthorization
     * @param _resp
     * @param _login
     */
    void genErrMssgBadAuthorization(HTTPServerResponse &_resp, Authorization::Login _login);

    /**
     * @brief genErrMssgNotImplemented
     * @param _resp
     * @param _log_warning
     * @param _module_print
     */
    void genErrMssgNotImplemented(HTTPServerResponse &_resp, bool _log_warning = false, std::string _module_print = "");

    /**
     * @brief genErrMssgDbUnreachable
     * @param _resp
     * @param _log_error
     * @param _module_print
     */
    void genErrMssgDbUnreachable(Poco::Net::HTTPServerResponse &_resp, bool _log_error = true, std::string _module_print = "");


private:


};

}

#endif
