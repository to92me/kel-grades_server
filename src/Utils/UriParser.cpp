#include "UriParser.h"

namespace Utils{

void UriParser::setRawUri(std::string _raw_uri){

    this->parsed = false;
    this->raw_uri = _raw_uri;
    this->error = NO_ERR;
    while(elements.empty() == false) elements.pop_front();
    while(query.empty() == false) query.pop();
}


bool UriParser::parse(){
    //1. strip ?data = ...
    StringTokenizer string_tokenizer1(this->raw_uri,"?",StringTokenizer::TOK_TRIM | StringTokenizer::TOK_IGNORE_EMPTY);

    //TODO @todo test if this is necessary ( I could not test it because web browser is always sending /)
    if(string_tokenizer1.count() == 0){
        this->error = ERR_FORMAT_CONTAINER;
        this->formant = OTHER;
        return false;
    }

    StringTokenizer::Iterator s_iterator;

    if(string_tokenizer1.count() > 2){
        for(s_iterator = (string_tokenizer1.begin()+1); s_iterator != (string_tokenizer1.end()); s_iterator++){
            query.push(*s_iterator);
            debug("query: " + *s_iterator);
        }
    }

    //Parse format
    s_iterator = string_tokenizer1.begin();
    StringTokenizer string_tokenizer2(*s_iterator,".",StringTokenizer::TOK_TRIM | StringTokenizer::TOK_IGNORE_EMPTY);
    if(string_tokenizer2.count() < 2){
        //this is JSON because default format is JSON
        this->formant = JSON;
    }else{
        s_iterator = string_tokenizer2.begin();
        s_iterator++;
        debug(*s_iterator);
        if(format_map.find(*s_iterator) != format_map.end()){
            this->formant = format_map[*s_iterator];
            debug(("format: "+*s_iterator));
        }else{
            this->error = ErrorType::ERR_FORMAT;
            this->formant = OTHER;
            warning(("incorrect UIR, unrecognized format: "+*s_iterator));
        }
    }


    // Parse container and sub "path"
    s_iterator = string_tokenizer2.begin();
    StringTokenizer string_tokenizer(*s_iterator,"/",StringTokenizer::TOK_TRIM | StringTokenizer::TOK_IGNORE_EMPTY);

    if(string_tokenizer.count() == 0){
        this->error = ERR_FORMAT_CONTAINER;
        this->formant = OTHER;
        return false;
    }

    s_iterator = string_tokenizer.begin();

    if(container_map.find(*s_iterator) != container_map.end()){
        debug(("container: " + *s_iterator));
        this->container = container_map[*s_iterator];
    }else{
        warning(("incorrect UIR, unrecognized container: "+*s_iterator));
        this->error = ErrorType::ERR_CONTAINER;
        this->formant = OTHER;
        return false;
    }

    if(string_tokenizer.count() > 1){
        for(s_iterator = (string_tokenizer.begin()+1); s_iterator != (string_tokenizer.end()); s_iterator++ ){
            debug(("elements: " + *s_iterator));
            this->elements.push_back(*s_iterator);
            debug("element" + *s_iterator);
        }
    }


//    s_iterator = string_tokenizer.end();
//    s_iterator--;

//    if(format_map.find(*s_iterator) != format_map.end()){
//        this->formant = format_map[*s_iterator];
//        debug(("format: "+*s_iterator));
//    }else{
//        this->error = ErrorType::ERR_FORMAT;
//        this->formant = OTHER;
//        warning(("incorrect UIR, unrecognized format: "+*s_iterator));
//        return false;
//    }
    this->parsed = true;
    return true;
}

std::queue<std::string> UriParser::getAllElements(){
    StringTokenizer string_tokenizer(this->raw_uri,"./?",StringTokenizer::TOK_TRIM | StringTokenizer::TOK_IGNORE_EMPTY);
    StringTokenizer::Iterator s_iterator;
    for(s_iterator = (string_tokenizer.begin()); s_iterator != (string_tokenizer.end()); s_iterator++){
        all_elements.push(*s_iterator);
        debug("all_elements: " + *s_iterator);
    }
    return all_elements;
}

UriParser::ContainerMap UriParser::container_map = {
    {"student", UriParser::UriContainer::STUDENT},
    {"professor", UriParser::UriContainer::PROFESSOR},
    {"subject", UriParser::UriContainer::SUBJECT},
    {"admin", UriParser::UriContainer::ADMIN},
    {"debug", UriParser::UriContainer::DEVELOPMENT},
    {"login", UriParser::LOGIN},
    {"pattern",UriParser::FAVICON},
    {"favicon",UriParser::FAVICON}
};

UriParser::FormatMap UriParser::format_map = {
    {"json",UriParser::Format::JSON},
    {"csv",UriParser::Format::CSV},
    {"xml", UriParser::Format::XML},
    {"ico", UriParser::Format::ICO},
    {"html", UriParser::Format::HTML},
    {"png", UriParser::Format::ICO}
};

std::string UriParser::getElement(unsigned int _element_number){
    if(this->elements.size() < (_element_number)
            || elements.size() == 0){
        return "";
    }else{
        return elements[_element_number];
    }

}

} //end namespace
