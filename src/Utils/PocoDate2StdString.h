#ifndef UTILS_POCODATE2STDSTRING_H
#define UTILS_POCODATE2STDSTRING_H

#include "iostream"
#include "Poco/Data/Date.h"

namespace Utils{

/**
 * @brief converts Poco Date and DateTime objects to std::string
 *
 * @ingroup Utils
 *
 * @author Tomislav Tumbas
 *
 * @todo test all fetures
 *
 * @version 0.1
 *
 * contact tumbas.tomislav@gmail.com tumbas@uns.ac.rs
 */
class PocoDate2StdString{
public:
    /**
     * @brief converts Poco::Data::Date to std::string
     * @param _date is Poco::Data::Date instance
     * @return date in std::string ("year-month-day")
     */
    static std::string date2string(Poco::Data::Date _date);

    /**
     * @brief converts Poco::DateTime to std::string
     * @param _date is Poco::DateTime instance
     * @return date in std::string ("year-month-day hh:mm")
     */
    static std::string dateTime2String(Poco::DateTime _date);

};

} // end namespace utils
#endif
