#ifndef URIPARSER_H
#define URIPARSER_H

#include <string>
#include <queue>

#include "Poco/StringTokenizer.h"
#include <iostream>
#include <map>
#include "Logger/Interface.h"
#include "Poco/URI.h"

using Poco::StringTokenizer;
using Log::LoggerInterface;

namespace Utils{

/**
 * @brief The UriParser class
 */
class UriParser : public LoggerInterface{
public:

    UriParser(): LoggerInterface("UriParser"){}
    ~UriParser(){}

    enum ErrorType{
        NO_ERR = 0,
        ERR_FORMAT = 1,
        ERR_CONTAINER = 2,
        ERR_FORMAT_CONTAINER = 3
    };

    enum UriContainer {
        STUDENT,
        PROFESSOR,
        SUBJECT,
        ADMIN,
        DEVELOPMENT,
        LOGIN,
        FAVICON
    };

    enum Format {
        JSON,
        CSV,
        XML,
        ICO,
        HTML,
        OTHER
    };

    /**
     * @brief setURI
     * @param input_uri
     */
    void setURI(std::string input_uri);

    /**
     * @brief setRawUri
     * @param _raw_uri
     */
    void setRawUri(std::string _raw_uri);

    /**
     * @brief getContainer
     * @return
     */
    UriContainer getContainer(){
            return this->container;
    }

    /**
     * @brief getElements
     * @return
     */
    std::deque<std::string> getElements() const{
        return this->elements;
    }

    /**
     * @brief getQuery
     * @return
     */
    std::queue<std::string> getQuery() const{
        return this->query;
    }

    /**
     * @brief getFormat
     * @return
     */
    inline Format getFormat() const{
        return this->formant;
    }

    /**
     * @brief isParsed
     * @return
     */
    inline bool isParsed() const{
        return this->uri_parsed;
    }

    /**
     * @brief getError
     * @return
     */
    inline ErrorType getError() const{
        return this->error;
    }

    /**
     * @brief getAllElements
     * @return
     */
    std::queue<std::string> getAllElements();


    /**
     * @brief parse
     * @return
     */
    bool parse();

    /**
     * @brief getElement
     * @param _element_number
     * @return
     */
    std::string getElement(unsigned int _element_number);

    /**
     * @brief getElementCount
     * @return
     */
    inline int getElementCount() const{
        return this->elements.size();
    }

private:
    bool parsed;
    Format formant;
    std::string raw_uri;
    UriContainer container;
    std::deque<std::string> elements;
    std::queue<std::string> query;
    std::queue<std::string> all_elements;
    bool uri_parsed;
    ErrorType error = ErrorType::NO_ERR;

    typedef std::map<std::string, UriContainer> ContainerMap;
    static ContainerMap container_map;
    typedef std::map<std::string, Format> FormatMap;
    static FormatMap format_map;





};

} // end namespace

#endif
