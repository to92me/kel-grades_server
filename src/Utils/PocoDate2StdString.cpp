#include "PocoDate2StdString.h"

namespace Utils{

std::string PocoDate2StdString::date2string(Poco::Data::Date _date){
    std::string date;
    date    = std::to_string(_date.year())
            + "-"
            + std::to_string(_date.month())
            + "-"
            + std::to_string(_date.day());
    return date;
}

std::string PocoDate2StdString::dateTime2String(Poco::DateTime _date){
    std::string date_time;

    date_time = std::to_string(_date.year())
            + "-"
            + std::to_string(_date.month())
            + "-"
            + std::to_string(_date.day())
            + " "
            + std::to_string(_date.hour())
            + ":"
            + std::to_string(_date.minute());

    return date_time;
}

}
