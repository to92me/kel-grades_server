#include "StdString2HttpMethod.h"

namespace Utils{

StdString2HttpMethod::HttpMethod StdString2HttpMethod::getHttpMethod(std::string _string_method){

    if(_string_method.compare("GET") == 0){
        return HttpMethod::GET;
    }else if(_string_method.compare("POST") == 0){
        return HttpMethod::POST;
    }else if(_string_method.compare("PUT") == 0){
        return HttpMethod::PUT;
    }else if(_string_method.compare("PATCH") == 0){
        return HttpMethod::PATCH;
    }else if(_string_method.compare("DELETE") == 0){
        return HttpMethod::DELETE;
    }else{
        return HttpMethod::BAD_REQUEST;
    }

}


}
