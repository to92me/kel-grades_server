#ifndef KELGS_UTILS_STDSTRING2AUTHORIZATINOLEVEL_H
#define KELGS_UTILS_STDSTRING2AUTHORIZATINOLEVEL_H

#include "Authorization/Authorization.h"

namespace Utils {

class StdString2AuthorizationLevel{
public:
    /**
     * @brief convert string _type and bool admin to corresponding Authorization Level
     * @param _type is string type from databse table User field type
     * @param _isadmin is bool from databse table user field is_admin
     * @return Authorizatino Level
     * @todo error handleing
     */
   static  Credentials::Authorization::Level convert(std::__cxx11::string _type, bool _isadmin);


};

}

#endif
