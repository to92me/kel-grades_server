#include "ErrorHandler.h"

namespace Utils {

void ErrorHandler::genErrMssgUnauthorized(Poco::Net::HTTPServerResponse &_resp,std::string _module_print){
    debug("Unauthorized " + _module_print);

    _resp.setStatus(HTTPResponse::HTTPStatus::HTTP_UNAUTHORIZED);
    _resp.setContentType("text/html");
    _resp.setReason(Poco::Net::HTTPResponse::HTTP_REASON_FORBIDDEN);
    std::ostream& out = _resp.send();
    out << "<h3>HTTP_UNAUTHORIZED</h3>";
    out.flush();

}

void ErrorHandler::genErrMssgMethodNotAllowed(Poco::Net::HTTPServerResponse &_resp,std::string _module_print){
    debug("Method not allowed " + _module_print);

    _resp.setStatus(HTTPResponse::HTTPStatus::HTTP_METHOD_NOT_ALLOWED);
    _resp.setContentType("text/html");
    _resp.setReason(Poco::Net::HTTPResponse::HTTP_REASON_METHOD_NOT_ALLOWED);
    std::ostream& out = _resp.send();
    out << "<h3>HTTP_METHOD_NOT_ALLOWED</h3>";
    out.flush();
}

void ErrorHandler::genErrMssgBadRequest(Poco::Net::HTTPServerResponse &_resp,std::string _module_print){
    debug("Bad Request " + _module_print);

    _resp.setStatus(HTTPResponse::HTTPStatus::HTTP_BAD_REQUEST);
    _resp.setContentType("text/html");
    _resp.setReason(Poco::Net::HTTPResponse::HTTP_REASON_BAD_REQUEST);
    std::ostream& out = _resp.send();
    out << "<h3>HTTP_BAD_REQUEST</h3>";
    out.flush();
}

void ErrorHandler::genErrMssgBadAuthorization(Poco::Net::HTTPServerResponse &_resp, Authorization::Login _login){

    switch (_login) {

    case Authorization::Login::ERROR_DB : {
        _resp.setStatus(HTTPResponse::HTTPStatus::HTTP_SERVICE_UNAVAILABLE);
        _resp.setReason(Poco::Net::HTTPResponse::HTTP_REASON_SERVICE_UNAVAILABLE);
        _resp.setContentType("text/html");
        std::ostream& out = _resp.send();
        out << "<h3>SERVICE UNAVAILABLE </h3>";
        out.flush();
    }

    default:{
        _resp.setStatus(HTTPResponse::HTTPStatus::HTTP_UNAUTHORIZED);
        _resp.setStatus(HTTPResponse::HTTPStatus::HTTP_UNAUTHORIZED);
        _resp.setContentType("text/html");
        std::ostream& out = _resp.send();
        out << "<h3>HTTP_UNAUTHORIZED</h3>";
        out.flush();
    }break;
    }
}

void ErrorHandler::genErrMssgNotImplemented(HTTPServerResponse &_resp, bool _log_warning, std::string _module_print){

    if(_log_warning == true){
        warning("Method Not Implemented!! " + _module_print);
    }

    _resp.setStatus(HTTPResponse::HTTPStatus::HTTP_NOT_IMPLEMENTED);
    _resp.setContentType("text/html");
    _resp.setReason(Poco::Net::HTTPResponse::HTTP_REASON_NOT_IMPLEMENTED);
    std::ostream& out = _resp.send();
    out << "<h3>HTTP_NOT_IMPLEMENTED</h3>";
    out.flush();
}

void ErrorHandler::genErrMssgDbUnreachable(Poco::Net::HTTPServerResponse &_resp, bool _log_error, std::__cxx11::string _module_print){
    if(_log_error == true){
        error("Database unreachable!!" + _module_print);
    }

    _resp.setStatus(HTTPResponse::HTTPStatus::HTTP_INTERNAL_SERVER_ERROR);
    _resp.setContentType("text/html");
    _resp.setReason(Poco::Net::HTTPResponse::HTTP_REASON_INTERNAL_SERVER_ERROR);
    std::ostream& out = _resp.send();
    out << "<h3>HTTP_INTERNAL_SERVER_ERROR</h3>";
    out.flush();
}

}
