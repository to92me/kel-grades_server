#ifndef KELGS_UTILS_CSVHANDLER_H
#define KELGS_UTILS_CSVHANDLER_H

#include "Logger/Interface.h"
#include "Poco/StringTokenizer.h"

#include <queue>
#include <map>
#include <set>

using Log::LoggerInterface;
using Poco::StringTokenizer;

namespace Utils{

/**
 * @brief parses and generates csv files.
 *
 *
 * @ingroup Utils
 *
 * @author Tomislav Tumbas
 *
 * @todo testing
 *
 * @version 0.1
 *
 * contact tumbas.tomislav@gmail.com tumbas@uns.ac.rs
 */
class CsvHandler : LoggerInterface{
public:

    /**
     * @brief one csv row type
     */
    typedef std::vector<std::string> csv_row_t;

    /**
     * @brief queue of all csv rows type
     */
    typedef std::deque<csv_row_t> csv_deque_t;

    /**
     * @brief Constructor
     */
    CsvHandler():LoggerInterface("CsvParser"){}

    /**
     * @brief writes csv string stream to std::ostream.
     * (csv string stream is generated from csv_deque
     * @param _ostr is std::ostream where csv file ( string stream ) is written
     */
    void write(std::ostream& _ostr);

    /**
     * @brief reads data from string stream and updates csv_deque
     * @param _istr is std::istream from which is scv read
     */
    void read(std::istream& _istr);

    /**
     * @brief returns one row from csv
     * @param _row is row number
     * @return row
     */
    csv_row_t& get(unsigned int _row);

    /**
     * @brief returns all csv rows in queue
     * @return all csv rows in queue
     */
    csv_deque_t& get();

    /**
     * @brief return _element element in _row row
     * @return string element
     */
    std::string get(unsigned int _row, unsigned int _element);

    /**
     * @brief adds row in front of queue
     * @param _row csv row to be added
     */
    void pushFront(const csv_row_t _row);

    /**
     * @brief adds row in back of queue
     * @param _row csv row to be added
     */
    void pushBack(const csv_row_t _row);

    /**
     * @brief adds one element inside one csv row. Adds it at back of row set
     * @param _row is number of csv row
     * @param _element is element to be added
     */
    void pushElementBack(unsigned int _row, std::string _element);

    /**
     * @brief adds one element inside one csv row. Adds it at front of row set
     * @param _row is number of csv row
     * @param _element is element to be added
     */
    void pushElementFront(const unsigned int _row, const std::__cxx11::string _element);

    /**
     * @brief retuns size ( number of rows ) in csv
     * @return size
     */
    int size()const;

    /**
     * @brief returns size of one row
     * @param _row is number of row
     * @return size of row ( number of elements in row )
     */
    int rowSize(unsigned int _row)const;

    /**
     * @brief returns row from front of csv
     * @return csv row
     */
    csv_row_t &front();

    /**
     * @brief returns row from back of csv
     * @return csv row
     */
    csv_row_t &back();

    /**
     * @brief deletes row from front of csv
     */
    void popFront();

    /**
     * @brief deletes row from back of csv
     */
    void popBack();

    /**
     * @brief getEmptyElement @hotfix @bug @todo @see CsvHandler::read()
     * @return
     */
    std::string getEmptyElement()const{
        return empty_element;
    }

private:

    /**
     * @brief csv_deque queue with all rows in csv
     */
    csv_deque_t csv_deque;
    std::string empty_element = "-";

};

}

#endif
