#include "Server/HttpServer.h"
#include "MySqlHandler/DBManager.h"
#include "View/Development.h"

using Server::HttpServer;
using MySql::DbManager;
using MySql::MySqlHandler;
using Log::Logger;

int main(int argc, char** argv){
    Logger::setDevMode(true);
    HttpServer http_server;
    MySqlHandler* mysql_handler;
    DbManager* db_manager;
    Logger* logger;

    //configure database
    mysql_handler = MySqlHandler::getInstance();

    mysql_handler->setAddress("192.168.100.90");
    mysql_handler->setPort("3306");
    mysql_handler->setPassword("kelTheBoss");
    mysql_handler->setUserName("kelgs");
    mysql_handler->setDatabaseName("kelg");


    //configure port for server
    http_server.setSocketPort(9090);
    http_server.setCaFolderPath("../CA3/");
    http_server.setCrtFileName("kelgs.bundle.crt");
    http_server.setKeyFileName("kelgs.key");
//    http_server.setCaFolderPath("../CA_gist/");
//    http_server.setCrtFileName("192.168.100.100.bundle.crt");
//    http_server.setKeyFileName("192.168.100.100.key");

    http_server.setUseSSL(false);
    Server::RequestHandler::setUseSLLConf(false);


    db_manager = new DbManager();
    if(!db_manager->runChecksAndCreation())return -1;

    delete db_manager;

    // configure footer and header files for debug html
    View::Development::setBottomHeaderFile("../debugtheme/footer.html");
    View::Development::setTopHeaderFile("../debugtheme/header.html");

    //configure logger
    logger = Logger::getInstance();
    logger->setWriteToHtmlStream(true);

    logger->blacklistModule("UriParser");

    logger->logInfo("Main","Starting server");
    http_server.run(argc, argv);
    logger->logInfo("Main","HTTPS server stopped, exiting");

    delete mysql_handler;
    delete logger;

    return 0;
}
