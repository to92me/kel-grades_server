#ifndef REQUESTHANDLER_H
#define REQUESTHANDLER_H

#include "iostream"
#include "string"
#include "vector"

#include "Poco/Net/HTTPRequestHandler.h"
#include "Poco/Net/HTTPServerResponse.h"
#include "Poco/Net/HTTPServerRequest.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/Net/HTTPBasicCredentials.h"

#include "Utils/UriParser.h"
#include "Utils/ErrorHandler.h"
#include "Authorization/Authorization.h"
#include "Controller/Development.h"
#include "Controller/Subject.h"
#include "Controller/Student.h"
#include "Controller/Professor.h"
#include "Controller/Admin.h"
#include "Logger/Logger.h"

#include "Utils/CsvHandler.h"

using Poco::Net::HTTPRequestHandler;
using Poco::Net::HTTPServerRequest;
using Poco::Net::HTTPServerResponse;
using Poco::Net::HTTPResponse;
using Poco::Net::NameValueCollection;

using Credentials::Authorization;
using Controller::ControllerDevelopment;
using Controller::ControllerSubject;
using Controller::ControllerStudent;
using Controller::ControllerProfessor;
using Controller::ControllerAdmin;
using Log::Logger;

using namespace std;

namespace Server{

/**
 * @brief is hadler to http request got from client. Method void handleRequest is
 * running in new thread.
 *
 * @ingroup HttpServer
 *
 * @author Tomislav Tumbas
 *
 * @version 0.3
 *
 * @todo create respnse with .json and html in debug mode
 * @todo create "index.html" if someone load host from browser
 * @todo create login by web page and reset passwd
 *
 *
 * contact tumbas.tomislav@gmail.com tumbas@uns.ac.rs
 */
class RequestHandler: public HTTPRequestHandler
{
public:

    /**
     * @brief represents method called in new thread. It gets request from client and should create corresponding response
     * this method culd be called main method of whole application
     * @param req is Http request got from client
     * @param resp is Http response for returning data to client
     */
    virtual void handleRequest(HTTPServerRequest &req, HTTPServerResponse &resp);

    /**
     * @brief set ssl configuration. If ssl config is set to true sessionToken will be secure, Otherwise not.
     * (if sessionToken is secure then it will not be sent with Http request. Only with Https
     * @param _use ssl configuration
     */
    static void setUseSLLConf(const bool _use){
        use_ssl_config = _use;
    }

    /**
     * @brief returns true if class is using ssl configuration @see setUseSLLConf
     */
    static bool getUsingSLLConf(){
        return use_ssl_config;
    }

private:
    Utils::UriParser uri_parser;
    Authorization auth;
    Logger* logger;
    static bool use_ssl_config;

    /**
     * @brief prints debug to terminal
     * @param req is HTTPServerRequest got from client
     * @param resp is HTTPServerResponse for returning to client
     */
    void printDebug(HTTPServerRequest &req, Poco::Net::HTTPServerResponse &resp, const Authorization::Status &status);

    /**
     * @brief login this method checks if there is user name and passwd in http header first, if not it
     * checks if there is sessionToken for login. With information of user name and passwdor or sesstionToken
     * Auhorization::Status will be generated for that user @see Status Authorization::login(std::string _token) and
     * @see Authorization::login(std::string _uname, std::string _passwd);
     * @param req is HTTPServerRequest got from client
     * @return Auhorization::Status with information about authoizations and login status.
     */
    Authorization::Status login(const HTTPServerRequest &req);

    /**
     * @brief method sets Http Herader (cookies and http status) acording to Authorization::Status
     * @see Authorization::Status
     * @param login is status of authorization
     * @param resp is HTTPServerResponse for returning to client
     */
    void setHttpCookie(const Authorization::Status &status, HTTPServerResponse &resp);

};




} // end namespace HttpServer

#endif
