#ifndef HTTPSERVER_H
#define HTTPSERVER_H

#define SERVER_PORT 9090

#include "Poco/Net/HTTPServer.h"
#include "Poco/Util/ServerApplication.h"
#include "Poco/Net/HTTPServerParams.h"
#include "Poco/Net/ServerSocket.h"
#include "Poco/Types.h"

//include fo SLL
#include "Poco/Net/SecureServerSocket.h"

#include "RequestHandlerFactory.h"

#include "Poco/StreamCopier.h"
#include "Poco/URI.h"
#include "Poco/Exception.h"
#include "Poco/SharedPtr.h"
#include "Poco/Net/SSLManager.h"
#include "Poco/Net/KeyConsoleHandler.h"
#include "Poco/Net/ConsoleCertificateHandler.h"
#include "Poco/Net/HTTPSClientSession.h"
#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTTPResponse.h"
#include <memory>
#include <iostream>

#include "Poco/Net/NetException.h"

using namespace Poco::Net;
using namespace Poco::Util;

using Poco::UInt16;

using namespace std;

namespace Server{

/**
 * @brief The HttpServer class
 *
 * @ingroup HttpServer
 *
 * @author Tomislav Tumbas
 *
 * @version 0.1
 *
 * @date march 2017
 *
 * contact tumbas.tomislav@gmail.com tumbas@uns.ac.rs
 */
class HttpServer: public ServerApplication {
public:
    //    HttpServer(){}
    //    ~HttpServer(){}

    /**
     * @brief setSocketPort sets port on witch socket will be opened
     * @param _port is socket port
     */
    void setSocketPort(const int _port);

    /**
     * @brief setCaFolderPath sets path to folder with certificate
     * @param _path is path to certificate folder
     */
    void setCaFolderPath(const std::string _path);

    /**
     * @brief setCrtFileName sets name of certificate file ( boundle certificate of web site and certificate
     * of certificate authority
     * @param _name is name of certificate file
     */
    void setCrtFileName(const std::string _name){
        this->crt_file_name = _name;
    }

    /**
     * @brief setKeyFileName sets name of private key of certificate
     * @param _name is name of private key file
     */
    void setKeyFileName(const std::string _name){
        this->key_file_name = _name;
    }

    /**
     * @brief setUseSSL sets server to use or not TLS1.2
     * @param _use is bool to use or not TLS1.2
     */
    void setUseSSL(const bool _use){
        this->use_ssl = _use;
    }

    /**
     * @brief getUsingSLL returns if server is using currently TLS
     * @return returns true if server is using TLS
     */
    bool getUsingSLL()const{
        return this->use_ssl;
    }

protected:
    /**
     * @brief main thread main method @see Poco::ServerApplication for more
     * @param args
     * @return
     */
    int main(const std::vector<string> &args);

    //private:
    Poco::Net::HTTPServer* http_server;
    HTTPServerParams* http_server_params;
    ServerSocket server_socket;

    RequestHandlerFactory* request_handle_factory;

    UInt16 server_socket_bind_address = 9090;
    std::string ca_path = "../CA/";

    /**
     * @brief init configures socket and http/s stack for server
     */
    void init();

    std::string crt_file_name = "servercert.pem";
    std::string key_file_name = "serverkey.pem";

    bool use_ssl = true;

};

} // end namespace http_server

#endif
