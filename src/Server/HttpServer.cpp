#include "HttpServer.h"

//#define KEL_SSL

namespace Server{

void HttpServer::init(){
    http_server_params = new HTTPServerParams();

    http_server_params->setMaxQueued(100);
    http_server_params->setMaxThreads(16);

    request_handle_factory = new RequestHandlerFactory();

    //#ifdef KEL_SSL

    if(use_ssl){

        initializeSSL();

        Poco::SharedPtr<InvalidCertificateHandler> pCert = new Poco::Net::ConsoleCertificateHandler(false);

        Poco::Net::Context::Ptr pContext = new Poco::Net::Context(
                    Poco::Net::Context::TLSV1_2_SERVER_USE,
                    //                "../CA/serverkey.pem",  // private key
                    //                "../CA/servercert.pem",  // complete certificate file wiht private key
                    //                "../CA/",                // folder with all certificates

                    //                ca_path + "serverkey.pem",
                    //                ca_path + "servercert.pem",
                    //                ca_path + "newcerts/01.pem",
                    ca_path + key_file_name,
                    ca_path + crt_file_name,
                    ca_path,
                    Poco::Net::Context::VERIFY_NONE,
                    9,
                    false,
                    "ALL:!ADH:!LOW:!EXP:!MD5:@STRENGTH");
        SSLManager::instance().initializeClient(0, pCert, pContext);

        SecureServerSocket svs(server_socket_bind_address, 64, pContext);



        http_server = new HTTPServer(request_handle_factory,svs,http_server_params);


        //#else
    }else{

        http_server = new HTTPServer(request_handle_factory,ServerSocket(server_socket_bind_address),http_server_params);
    }
    //#endif
}

int HttpServer::main(const std::vector<string> &args){
    this->init();

    try{

        http_server->start();

    }catch(Poco::Net::HTTPException http_ex ){
        //TODO logger
        std::cout << http_ex.what();
    }


    waitForTerminationRequest();



    http_server->stop();

    return Application::EXIT_OK;
}

void HttpServer::setSocketPort(const int _port){
    this->server_socket_bind_address = _port;
}

void HttpServer::setCaFolderPath(const string _path){
    this->ca_path = _path;
}


}
