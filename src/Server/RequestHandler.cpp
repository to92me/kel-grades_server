#include "RequestHandler.h"

namespace Server{

bool RequestHandler::use_ssl_config = true;

void RequestHandler::handleRequest(Poco::Net::HTTPServerRequest &req, Poco::Net::HTTPServerResponse &resp){
    Authorization::Status status_user;

    logger = Logger::getInstance();

    // development purpose - for instant printing of request data;
//    if(Log::Logger::getDevMode() == true) this->printDebug(req,resp,status_user);

    //login with http request
    status_user = this->login(req);

    // if client is not logged in
    if(status_user.login != Authorization::Login::SUCCESS){
        Utils::ErrorHandler err_hander;
        err_hander.genErrMssgBadAuthorization(resp,status_user.login);
        return;
    }

    //set parser and parse URI for data
    uri_parser.setRawUri(req.getURI());
    uri_parser.parse();

    // set status of logging to response
    this->setHttpCookie(status_user,resp);

    if(uri_parser.getError() != UriParser::ErrorType::NO_ERR && uri_parser.getContainer() != UriParser::LOGIN && uri_parser.getError() != UriParser::ErrorType::ERR_FORMAT){
        Utils::ErrorHandler err_hander;
        err_hander.genErrMssgBadRequest(resp,"RequestHanlder: error in uri_parser");
        return;
    }


    //call controller for corresponding container
    switch (uri_parser.getContainer()) {
    case UriParser::UriContainer::STUDENT : {
        ControllerStudent controller_student;
        controller_student.execute(status_user,uri_parser,req,resp);
    }break;

    case UriParser::UriContainer::PROFESSOR : {
         ControllerProfessor controller_professor;
         controller_professor.execute(status_user,uri_parser,req,resp);
    }break;

    case UriParser::UriContainer::ADMIN : {
        ControllerAdmin controller_admin;
        controller_admin.execute(status_user,uri_parser,req,resp);
    }break;

    case UriParser::UriContainer::SUBJECT : {
        ControllerSubject controller_subject;
        controller_subject.execute(status_user, uri_parser, req, resp);
    }break;

    case UriParser::UriContainer::DEVELOPMENT : {
        ControllerDevelopment controller_dev;
        controller_dev.showDevelopment(status_user,uri_parser,req,resp);
    }break;

    case UriParser::UriContainer::LOGIN : {
        resp.setStatus(HTTPResponse::HTTPStatus::HTTP_OK);
        resp.setContentType("text/html");
        std::ostream& out = resp.send();
        out << "<h3>Credentials OK!</h3>";
        out.flush();
    }break;

    case UriParser::FAVICON : {
        resp.setStatus(HTTPResponse::HTTPStatus::HTTP_OK);
        resp.setContentType("image/x-icon");
        std::ostream& out = resp.send();
//        out << "<h3>Credentials OK!</h3>";
        out.flush();
    }break;
    }// end switch
}


void RequestHandler::printDebug(Poco::Net::HTTPServerRequest &req, Poco::Net::HTTPServerResponse &resp,const Authorization::Status &status) {


    //    ostream& out = resp.send();
    std::stringstream ss;
    //        ss << "<h3>KEL GRADES SERVER!</h3>"
    //            << "<p>Count: "  << ++count         << "</p>"
    //            << "<p>Host: "   << req.getHost()   << "</p>"
    //            << "<p>Method: " << req.getMethod() << "</p>"
    //            << "<p>URI: "    << req.getURI()    << "</p>"
    //            << "<p>Status.message" << status.message << "<p>"
    //            << "<p>Status.token " << status.token << "<p>"
    //            << "<p>Status.id " << status.user_id << "<p>"
    //            << "<p>status.year " << status.token_exipre_date.year() << "<p>"
    //            << "<p>status.month " << status.token_exipre_date.month() << "<p>"
    //            << "<p>status.day " << status.token_exipre_date.day() << "<p>"
    //            << "";

    //    ss << "Host: "   << req.getHost()   << std::endl
    //       << "Method: " << req.getMethod() << std::endl
    //       << "URI: "    << req.getURI()    << std::endl
    //       << "Status.message" << status.message << std::endl
    //       << "Status.token " << status.token << std::endl
    //       << "Status.id " << status.user_id << std::endl
    //       << "status.year " << status.token_exipre_date.year() << std::endl
    //       << "status.month " << status.token_exipre_date.month() << std::endl
    //       << "status.day " << status.token_exipre_date.day() << std::endl;
    //    debug(ss.chr());


    //    cout << endl
    //         << "Response sent for count=" << count
    //         << " and URI=" << req.getURI() << endl;

        req.write(std::cout);
        std::istream& istr = req.stream();
            std::cout << istr.gcount();
            Utils::CsvHandler csv_h;
            csv_h.read(istr);

        std::cout << "\n\n";
    return;
}


Authorization::Status RequestHandler::login(const Poco::Net::HTTPServerRequest &req){
    NameValueCollection cookie_collection;
    Authorization::Status status;

    req.getCookies(cookie_collection);

    if(req.hasCredentials()){
        Poco::Net::HTTPBasicCredentials http_basic_base64_credentials(req);
        status = auth.login(http_basic_base64_credentials.getUsername(), http_basic_base64_credentials.getPassword());
        if(status.login == Authorization::Login::SUCCESS){
            logger->logDebug("HTTPRequestHandler","login id: " + status.user_id);
        }else{
            logger->logDebug("HTTPRequestHandler","unseccessful login");
        }

    }else if(cookie_collection.has("sessionToken")){
        status = auth.login(cookie_collection.get("sessionToken"));

    }else{
        status.login = Authorization::Login::ERROR_CREDENTIALS;
    }

    return status;
}

void RequestHandler::setHttpCookie(const Authorization::Status &status, HTTPServerResponse &resp){
    Poco::Net::HTTPCookie http_cookie;

    http_cookie.setName("sessionToken");
    http_cookie.setValue(status.token);
    http_cookie.setSecure(use_ssl_config);
    http_cookie.setMaxAge(31536000);
    http_cookie.setPath("/");

    resp.addCookie(http_cookie);
}
} // end namespace HttpServer
