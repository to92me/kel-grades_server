#ifndef REQUESTHANDLERFACTORY_H
#define REQUESTHANDLERFACTORY_H

#include "Poco/Net/HTTPRequestHandlerFactory.h"

#include "RequestHandler.h"

using Poco::Net::HTTPRequestHandlerFactory;
using Poco::Net::HTTPServerRequest;

namespace Server{

/**
 * @brief The RequestHandlerFactory class
 */
class RequestHandlerFactory : public HTTPRequestHandlerFactory {
public:

    /**
     * @brief createRequestHandler returns new RequestHandler for every server request
     * @return RequestHandler
     */
    virtual HTTPRequestHandler* createRequestHandler(const HTTPServerRequest &)
    {
        return new RequestHandler;
    }
};


}

#endif


