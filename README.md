# KELGS (KEL GRADES SERVER)      ![alt text](https://cdn3.iconfinder.com/data/icons/education-1-7/48/22-128.png "Logo")

---
> KEL Grades Server is serves side application of grades management system on KEL ( Katedra za Elektroniku )
> FTN, Univerzitet u Novom Sadu    

## Getting started

---
KELGS is ReST server application written in C++ using Poco library and CMake.

## Prerequisites:
### 1. Download source
To download application source clone git repository:

```bash
    $git clone -b master --depth 1 https://to92me@bitbucket.org/to92me/kel-grades_server.git
```

### 2. Poco library
> Poco is: ''Modern, powerful open source C++ class libraries for building network- and internet-based applications that run on desktop, server, mobile and embedded systems.''
Poco lib has also dependencies so first run following:

* Ubuntu

1. install mysql dependencies:     
```
$sudo apt-get install libmysqlclient-dev
```
2. install OpsenSSL dependence:       
```
$sudo apt-get install openssl libssl-dev
```

* Arch
1. install mysql dependencies:      
```
$sudo pacman -S openssl
```

2. install OpsenSSL dependence:        
```
$sudo pacman -S libmariadbclient
```

Download Poco Complete Edition [download](https://pocoproject.org/releases/poco-1.7.6/poco-1.7.6-all.tar.gz)
Extract package, go in poco folder and run:
```
$./configure --omit=Data/ODBC
```
build Poco:
```
$make -j<number_of_your_CPU_cores+1>
```

after building poco link poco folder to project_folder/libs/poco
(example: ``` ln -s /path/to/poco/poco.version /path/to/project/folder/libs/poco ``` )  

### 3. Cmake
> 'CMake is an open-source, cross-platform family of tools designed to build, test and package software.'

install poco by running:
```
$sudo apt-get install cmake
```

### 4. Doxygen
> 'Doxygen is the de facto standard tool for generating documentation from annotated C++ sources'
Doxygen is not required to build application but it is required to get documentation about application.

install doxygen
```
$sudo apt-get install doxygen
$sudo apt-get install graphviz
```

## Building and running KELGS

---
in project folder run configuration for cmake:
```
$cmake -G "Unix Makefiles"
```
$build application:
```
$make -j<number_od_your_CPU_cores+1>
```
executable will be stored in bin/ folder.

copy CA from project folder to bin folder
```
$cp CA bin/
```

run application:
```
$./KELGServer
```

## REST API

---

 [API](docfiles/kelgs_API.md)

## For KEL students

---

 [Developers intro](docfiles/kelgs_dev.md)

## Documentation generation

---
to generate documentation with doxygen run doxygen in project folder:
```
doxygen DoxyFile
```
documentation will be generated in project sub-folder doc/.
You see docs in browser by running:
```
$firefox docs/html/index.html
```

## Questions and issues

---
a [bitbucket issue and bug tracker](https://bitbucket.org/to92me/kel-grades_server/issues?status=new&status=open)  is only for bug reports and feature requests.
